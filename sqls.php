ALTER TABLE `edicoes`
	ADD COLUMN `titulo_it` LONGTEXT NULL AFTER `titulo_en`,
	ADD COLUMN `titulo_es` LONGTEXT NULL AFTER `titulo_it`,
    ADD COLUMN `texto_it` LONGTEXT NULL AFTER `palavra_chave_en`,
	ADD COLUMN `texto_it` LONGTEXT NULL AFTER `palavra_chave_en`,
	ADD COLUMN `palavra_chave_it` LONGTEXT NULL AFTER `texto_it`,
	ADD COLUMN `texto_es` LONGTEXT NULL AFTER `palavra_chave_it`,
	ADD COLUMN `palavra_chave_es` LONGTEXT NULL AFTER `texto_es`,
	ADD COLUMN `resumo_it` VARCHAR(20) NULL DEFAULT NULL AFTER `resumo_en`,
	ADD COLUMN `resumo_es` VARCHAR(20) NULL DEFAULT NULL AFTER `resumo_it`,
	ADD COLUMN `chave_it` VARCHAR(20) NULL DEFAULT NULL AFTER `chave_en`,
	ADD COLUMN `chave_es` VARCHAR(20) NULL DEFAULT NULL AFTER `chave_it`,
	ADD COLUMN `titulo_principal_it` TINYINT(4) NOT NULL AFTER `titulo_principal_en`,
	ADD COLUMN `titulo_principal_es` TINYINT(4) NOT NULL AFTER `titulo_principal_it`,
	ADD COLUMN `idioma_original` ENUM('pt','en','es', 'it') NOT NULL;

ALTER TABLE `revistas`
	ADD COLUMN `diretrizes_pt_file` VARCHAR(255) NULL AFTER `diretrizes_pt`,
	ADD COLUMN `diretrizes_en_file` VARCHAR(255) NULL AFTER `diretrizes_en`,
	ADD COLUMN `regras_pt_file` VARCHAR(255) NULL AFTER `regras_pt`,
	ADD COLUMN `regras_en_file` VARCHAR(255) NULL AFTER `regras_en`;


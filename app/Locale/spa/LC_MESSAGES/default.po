# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2014-09-15 22:00+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/ContatoController.php:30
msgid "Mensagem enviada com sucesso"
msgstr "Mensaje enviado con éxito"

#: Controller/ContatoController.php:33
msgid "Não foi possível enviar a sua mensagem, por favor, tente novamente"
msgstr "No se puede enviar el mensaje, vuelve a intentarlo"

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:80
msgid "Plugin to process"
msgstr ""

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:83
msgid "Better manage, and easily synchronize you application's ACO tree"
msgstr ""

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:88
msgid "Add new ACOs for new controllers and actions. Does not remove nodes from the ACO table."
msgstr ""

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:93
msgid "Perform a full sync on the ACO table.Will create new ACOs or missing controllers and actions.Will also remove orphaned entries that no longer have a matching controller/action"
msgstr ""

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:97
msgid "Verify the tree structure of either your Aco or Aro Trees"
msgstr ""

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:102
msgid "The type of tree to verify"
msgstr ""

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:108
msgid "Recover a corrupted Tree"
msgstr ""

#: Plugin/AclExtras/Console/Command/AclExtrasShell.php:113
msgid "The type of tree to recover"
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:123
msgid "<error>Plugin %s not found or not activated</error>"
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:136
msgid "<success>Aco Update Complete</success>"
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:181
msgid "Deleted %s and all children"
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:222
msgid "Created Aco node: <success>%s</success>"
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:275
msgid "Unable to get methods for \"%s\""
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:296
msgid "Deleted Aco node: <warning>%s</warning>"
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:315
msgid "<success>Tree is valid and strong</success>"
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:333
msgid "Tree has been recovered, or tree did not need recovery."
msgstr ""

#: Plugin/AclExtras/Lib/AclExtras.php:335
msgid "<error>Tree recovery failed.</error>"
msgstr ""

#: Plugin/FdRotas/Controller/FdRotasController.php:71;108;172
msgid "Registro Inválido."
msgstr ""

#: Plugin/FdRotas/Controller/FdRotasController.php:91;129
msgid "Registro salvo com sucesso."
msgstr ""

#: Plugin/FdRotas/Controller/FdRotasController.php:94;138
msgid "O registro não pode ser salvo. Verifique os campos em destaque."
msgstr ""

#: Plugin/FdRotas/Controller/FdRotasController.php:177
msgid "Registro deletado."
msgstr ""

#: Plugin/FdRotas/Controller/FdRotasController.php:180
msgid "Registro não pode ser deletado."
msgstr ""

#: Plugin/FilterResults/Controller/Component/FilterResultsComponent.php:416
msgid "$filters type must be a array or string"
msgstr ""

#: Plugin/Upload/Model/Behavior/UploadBehavior.php:1078
msgid "No filename after parsing. Function %s returned an invalid filename"
msgstr ""

#: View/Areas/detalhe.ctp:1
msgid "Áreas de Atuação "
msgstr "Áreas de Especialización "

#: View/Areas/detalhe.ctp:10
#: View/Areas/index.ctp:10
#: View/Home/index.ctp:119
msgid "Áreas de"
msgstr "Áreas"

#: View/Areas/detalhe.ctp:11
#: View/Areas/index.ctp:11
msgid "Atuação"
msgstr "Experiencia"

#: View/Areas/detalhe.ctp:25
#: View/Areas/index.ctp:1
msgid "Áreas de Atuação"
msgstr "Áreas de Especialización"

#: View/Areas/detalhe.ctp:36
#: View/Noticias/detalhe.ctp:40
#: View/Publicacoes/detalhe.ctp:40
msgid "Voltar"
msgstr "Volver"

#: View/Contato/index.ctp:1;7
#: View/Elements/site/footer.ctp:10
#: View/Elements/site/header.ctp:33;66
msgid "Contato"
msgstr "Contacto"

#: View/Contato/index.ctp:18
msgid "Nossos"
msgstr "Nuestra"

#: View/Contato/index.ctp:19
#: View/Home/index.ctp:105
msgid "Escritórios"
msgstr Oficinas"

#: View/Contato/index.ctp:31;41
msgid "Ver mapa"
msgstr "Ver mapa"

#: View/Contato/index.ctp:47
msgid "Fale"
msgstr "Contacto"

#: View/Contato/index.ctp:48
msgid "Conosco"
msgstr "Nosotros"

#: View/Contato/index.ctp:51
msgid "Preencha o formulário abaixo e envie sua mensagem"
msgstr "Rellene el siguiente formulario y enviar tu mensaje"

#: View/Contato/index.ctp:54
#: View/Elements/site/form-newsletter-interna.ctp:5
#: View/Home/index.ctp:71;78;87;94;259
msgid "Nome"
msgstr "Nombre"

#: View/Contato/index.ctp:55
#: View/Elements/site/form-newsletter-interna.ctp:6
#: View/Home/index.ctp:260
msgid "E-mail"
msgstr "Email"

#: View/Contato/index.ctp:56
msgid "Telefone"
msgstr "Teléfono"

#: View/Contato/index.ctp:57
msgid "Mensagem"
msgstr "Mensaje"

#: View/Contato/index.ctp:59
msgid "Enviar mensagem"
msgstr "Enviar mensaje"

#: View/Elements/site/footer.ctp:5
#: View/Elements/site/header.ctp:15;48
msgid "Home"
msgstr ""

#: View/Elements/site/footer.ctp:6
#: View/Elements/site/header.ctp:18;51
#: View/Empresa/index.ctp:1;11
msgid "Empresa"
msgstr Empresa"

#: View/Elements/site/footer.ctp:7
#: View/Elements/site/header.ctp:21;54
msgid "Áreas de atuação"
msgstr "Áreas de especialización"

#: View/Elements/site/footer.ctp:8
#: View/Elements/site/header.ctp:24;57
#: View/Home/index.ctp:199
#: View/Publicacoes/categoria.ctp:1;7;21
#: View/Publicacoes/detalhe.ctp:1;7;21
#: View/Publicacoes/index.ctp:1;7;21
#: View/Publicacoes/pesquisa.ctp:3;9;23
msgid "Publicações"
msgstr "Publicaciones"

#: View/Elements/site/footer.ctp:9
#: View/Elements/site/header.ctp:27;60
#: View/Home/index.ctp:212
#: View/Noticias/detalhe.ctp:7;21
#: View/Noticias/index.ctp:1;7;21
#: View/Noticias/pesquisa.ctp:9;23
#: View/Noticias/todas.ctp:1;7;21
msgid "Notícias"
msgstr "Noticias"

#: View/Elements/site/form-newsletter-interna.ctp:7
#: View/Home/index.ctp:263
msgid "Área um"
msgstr "Una de las áreas"

#: View/Elements/site/form-newsletter-interna.ctp:7
#: View/Home/index.ctp:263
msgid "Área dois"
msgstr "Dos Área"

#: View/Elements/site/form-newsletter-interna.ctp:7
#: View/Home/index.ctp:263
msgid "Área três"
msgstr "Área de tres"

#: View/Elements/site/form-newsletter-interna.ctp:7
#: View/Home/index.ctp:263
msgid "Área de Interesse"
msgstr "Área de Interés"

#: View/Elements/site/form-newsletter-interna.ctp:9
#: View/Home/index.ctp:265
msgid "ASSINAR"
msgstr SIGN"

#: View/Elements/site/header.ctp:30;63
msgid "Consulte seu processo"
msgstr "Consulte a su proceso"

#: View/Elements/site/sidebar-areas-de-atuacao.ctp:5
msgid "TODAS AS"
msgstr "TODO"

#: View/Elements/site/sidebar-areas-de-atuacao.ctp:5
msgid "ÁREAS DE ATUAÇÃO"
msgstr "ÁREAS"

#: View/Elements/site/sidebar-noticias.ctp:5
#: View/Noticias/detalhe.ctp:31
#: View/Noticias/index.ctp:31
msgid "Últimas notícias"
msgstr "Últimas noticias"

#: View/Elements/site/sidebar-noticias.ctp:5
#: View/Noticias/detalhe.ctp:21
#: View/Noticias/index.ctp:21
msgid "Últimas Notícias"
msgstr "Últimas Noticias"

#: View/Elements/site/sidebar-noticias.ctp:8
#: View/Noticias/todas.ctp:31
msgid "Todas as notícias"
msgstr "Todas las noticias"

#: View/Elements/site/sidebar-noticias.ctp:5
#: View/Noticias/detalhe.ctp:31
#: View/Noticias/index.ctp:31
msgid "Últimas notícias"
msgstr "Lo último en los medios de comunicación"

#: View/Elements/site/sidebar-noticias.ctp:5
#: View/Noticias/detalhe.ctp:21
#: View/Noticias/index.ctp:21
msgid "Últimas Notícias"
msgstr "Lo último en los medios de comunicación"

#: View/Elements/site/sidebar-noticias.ctp:8
#: View/Noticias/todas.ctp:31
msgid "Todas as notícias"
msgstr "Todos los medios de comunicación"

#: View/Elements/site/sidebar-publicacoes.ctp:5
#: View/Publicacoes/index.ctp:21;31
msgid "Últimas publicações"
msgstr "Últimas publicaciones"


#: View/Elements/site/sidebar-publicacoes.ctp:5
msgid "Últimos artigos"
msgstr "Últimos artículos"

#: View/Elements/site/sidebar-publicacoes.ctp:13
msgid "Todas as publicações"
msgstr "Todas las publicaciones"

#: View/Elements/site/sidebar-publicacoes.ctp:13
msgid "Todos os artigos"
msgstr "Todos los artículos"

#: View/Empresa/index.ctp:10
msgid "Institucional"
msgstr "Institucional"

#: View/Empresa/index.ctp:27
msgid "Saiba mais sobre as nossas Áreas de de Atuação"
msgstr "Más información sobre nuestras Áreas de Práctica"

#: View/Empresa/index.ctp:28
msgid "e nossos serviços em nosso"
msgstr "y nuestros servicios en nuestro"

#: View/Empresa/index.ctp:28
msgid "Perfil Institucional"
msgstr "Perfil Institucional"

#: View/Empresa/index.ctp:29
msgid "PERFIL INSTITUCIONAL"
msgstr "PERFIL INSTITUCIONAL"

#: View/Empresa/index.ctp:55
msgid "Missão"
msgstr "Misión"

#: View/Empresa/index.ctp:122
#: View/Home/index.ctp:186
msgid "Rede Scalzilli Brasil - Corporate"
msgstr "Red Scalzilli Brasil - Corporate"

#: View/Empresa/index.ctp:126
#: View/Home/index.ctp:192
msgid "Rede Scalzilli no Brasil"
msgstr "Red Scalzilli en Brasil"

#: View/Empresa/index.ctp:147
msgid "Consulte-nos através dos e-mails:"
msgstr "Póngase en contacto con nosotros por correo electrónico:"

#: View/Home/index.ctp:30
msgid "Quem Somos"
msgstr "Sobre Nosotros"

#: View/Home/index.ctp:34
msgid "A Scalzilli.fmv Advogados & Associados, com 40 anos de atuação, consolidou sua tradição e credibilidade em todo mercado jurídico corporativo com o compromisso de estar sempre próxima ao cliente. Com esta abordagem, a Scalzilli.fmv Advogados & Associados expandiu sua atuação para todo o país, buscando proporcionar um atendimento Estratégico, Eficaz e Dinâmico, focado sempre no Resultado e respeitando as peculiaridades e a cultura de cada região."
msgstr "El Scalzilli.fmv Advogados y Associados, con 40 años de operaciones, consolidando su tradición y credibilidad a través del mercado legal corporativa con el compromiso de estar siempre cerca del cliente. Con este enfoque, la Scalzilli.fmv Abogados & Associates expandieron sus operaciones en todo el país, buscando brindar un servicio estratégico, eficaz y dinámico, siempre centrado en los ingresos y respetando las peculiaridades y la cultura de cada región."

#: View/Home/index.ctp:35;312
msgid "Saiba mais"
msgstr "Aprender más"

#: View/Home/index.ctp:63
msgid "Profissionais"
msgstr "Profesional"

#: View/Home/index.ctp:69;76;85;92
msgid "Nome do profissional"
msgstr "Nombre del Estado Mayor"

#: View/Home/index.ctp:72;79;88;95
msgid "Função na empresa"
msgstr "Posición en la compañía"

#: View/Home/index.ctp:120
msgid "atuação"
msgstr "rendimiento"

#: View/Home/index.ctp:127;151
msgid "Gestão de RH"
msgstr "Gestión de RH"

#: View/Home/index.ctp:129;153
msgid "Direito Trabalhista"
msgstr "Derecho del Trabajo"

#: View/Home/index.ctp:130;154
msgid "e Gestão de RH"
msgstr "y Gestión de RH"

#: View/Home/index.ctp:132;140;148;156;164;172
msgid "Praesent fringilla tortor tempus, lacinia nunc a, suscipit ante. Duis eleifend dictum arcu. Vivamus consectetur sodales."
msgstr "Praesent fringilla tortor tempus, lacinia nunc a, suscipit ante. Duis eleifend dictum arcu. Vivamus consectetur sodales."

#: View/Home/index.ctp:135;159
msgid "Gestão Tributária"
msgstr "Gestión Tributaria"

#: View/Home/index.ctp:137;161
msgid "Gestão"
msgstr "Gestión"

#: View/Home/index.ctp:138;162
msgid "Tributária"
msgstr "Tributaria"

#: View/Home/index.ctp:143;167
msgid "Fusões & Aquisições"
msgstr "Fusiones y Adquisiciones"

#: View/Home/index.ctp:145;169
msgid "Fusões &"
msgstr "Fusiones &"

#: View/Home/index.ctp:146;170
msgid "Aquisições"
msgstr "Adquisiciones"

#: View/Home/index.ctp:178
msgid "Veja todas as áreas de atuação"
msgstr "Ver todas las áreas de práctica"

#: View/Home/index.ctp:187
msgid "A Rede Scalzilli/Brasil é formada por Parceiros Operacionais Credenciados em todo Brasil. Foi criada com o objetivo de otimizar, com dinamismo e qualidade, o apoio jurídico aos clientes da Scalzilli.fmv Advogados & Associados e da própria Rede Scalzilli/Brasil, em qualquer lugar do País, onde houver necessidade."
msgstr "El Scalzilli Red / Brasil está formada por socios acreditados que operan en todo Brasil. Fue creado con el objetivo de optimizar con el dinamismo y el apoyo legal de calidad a los clientes de Scalzilli.fmv Abogados & Associates y la propia Red Scalzilli / Brasil, en cualquier parte del país, donde hay necesidad."

#: View/Home/index.ctp:189
msgid "Clique e conheça"
msgstr "Haz clic aquí para saber"

#: View/Home/index.ctp:216
msgid "Acesso o site"
msgstr "Acceso al sitio"

#: View/Home/index.ctp:209;222
#: View/Noticias/index.ctp:46
#: View/Noticias/pesquisa.ctp:41
#: View/Noticias/todas.ctp:38
#: View/Publicacoes/categoria.ctp:38
#: View/Publicacoes/index.ctp:46
#: View/Publicacoes/pesquisa.ctp:41
msgid "Leia mais"
msgstr "Leer más"

#: View/Home/index.ctp:227
msgid "na mídia"
msgstr "en los medios de comunicación"

#: View/Home/index.ctp:236;245
msgid "FONTE: Valor Econômico"
msgstr "FUENTE: Valor Económico"

#: View/Home/index.ctp:238;247
msgid "Lorem ipsum dolor sit amet, consectetuer adipiscing"
msgstr "Lorem ipsum dolor sit amet, consectetuer adipiscing"

#: View/Home/index.ctp:252
msgid "Veja todas"
msgstr "Ver todo"

#: View/Home/index.ctp:273;276
msgid "Newsletter"
msgstr "Newsletter"

#: View/Home/index.ctp:277
msgid "Assine nossa newsletter e receba as novidades do portal da Scalzilli.fmv Advogados & Associados"
msgstr "Suscríbete a nuestro newsletter y reciba el portal de noticias de Scalzilli.fmv Advogados & Associados"

#: View/Home/index.ctp:279
msgid "Assinar"
msgstr "Suscribirse"

#: View/Home/index.ctp:290
msgid "Consulta de falências e"
msgstr "Consulta de Quiebras y"

#: View/Home/index.ctp:291
msgid "recuperações judiciais"
msgstr "recuperaciones judiciales"

#: View/Home/index.ctp:296
msgid "Falências"
msgstr "Quiebra"

#: View/Home/index.ctp:299
msgid "Recuperações judiciais"
msgstr "Recuperaciones judiciales"

#: View/Home/index.ctp:299
msgid "Recuperações"
msgstr "Recuperaciones"

#: View/Home/index.ctp:299
msgid "judiciais"
msgstr "judicial"

#: View/Home/index.ctp:303;304
msgid "Enquete"
msgstr "Encuesta"

#: View/Home/index.ctp:305;311;317
msgid "Lorem Ipsum dolor sit amet sent fringilla tortor tempus, lacinia nunc a, suscipit ante."
msgstr "Lorem Ipsum dolor sit amet sent fringilla tortor tempus, lacinia nunc a, suscipit ante."

#: View/Home/index.ctp:306
msgid "Responder"
msgstr "Responder"

#: View/Home/index.ctp:309
msgid "Links úteis"
msgstr "Enlaces útiles"

#: View/Home/index.ctp:310
msgid "Links Úteis"
msgstr "Enlaces Útiles"

#: View/Home/index.ctp:315;316
msgid "Trabalhe Conosco"
msgstr "Trabaja con Nosotros"

#: View/Home/index.ctp:318
msgid "Enviar currículo"
msgstr "Enviar hoja de vida"

#: View/Noticias/detalhe.ctp:1
#: View/Noticias/pesquisa.ctp:3
msgid "Noticias"
msgstr "Noticias"

#: View/Noticias/detalhe.ctp:26
#: View/Noticias/index.ctp:26
#: View/Noticias/pesquisa.ctp:28
#: View/Noticias/todas.ctp:26
#: View/Publicacoes/categoria.ctp:26
#: View/Publicacoes/detalhe.ctp:26
#: View/Publicacoes/index.ctp:26
#: View/Publicacoes/pesquisa.ctp:28
msgid "pesquisar"
msgstr búsqueda"

#: View/Noticias/index.ctp:38
msgid "Leia a notícia completa"
msgstr "Lea la historia completa"

#: View/Noticias/index.ctp:46
#: View/Noticias/pesquisa.ctp:41
#: View/Noticias/todas.ctp:38
#: View/Publicacoes/categoria.ctp:38
#: View/Publicacoes/index.ctp:46
#: View/Publicacoes/pesquisa.ctp:41
msgid "Leia mais >>"
msgstr "Leer más >>"

#: View/Noticias/pesquisa.ctp:23
#: View/Publicacoes/pesquisa.ctp:23
msgid "Pesquisa"
msgstr "Búsqueda"

#: View/Noticias/pesquisa.ctp:33
#: View/Publicacoes/pesquisa.ctp:33
msgid "Resultado da pesquisa"
msgstr "Resultado de la búsqueda"

#: View/Noticias/pesquisa.ctp:34
#: View/Publicacoes/pesquisa.ctp:34
msgid "Foram encontrados"
msgstr "Encontrado"

#: View/Noticias/pesquisa.ctp:34
#: View/Publicacoes/pesquisa.ctp:34
msgid "relacionados ao termo"
msgstr "término relacionado"

#: View/Noticias/todas.ctp:21
msgid "Todas as Notícias"
msgstr "Todas las Noticias"

#: Model/Contato.php:validation for field nome
msgid "Informe o seu nome"
msgstr ""

#: Model/Contato.php:validation for field email
msgid "Informe o seu e-mail"
msgstr ""

#: Model/Contato.php:validation for field mensagem
msgid "Informe a sua mensagem"
msgstr ""

#: Plugin/FdConfiguracoes/Model/Configuracao.php:validation for field key
msgid "Informe a chave da configuração"
msgstr ""

#: Plugin/FdConfiguracoes/Model/Configuracao.php:validation for field value
msgid "Informe o valor da configuração"
msgstr ""

#: Plugin/FdIdiomas/Model/Idioma.php:validation for field nome
msgid "Informe o nome do idioma"
msgstr ""

#: Plugin/FdIdiomas/Model/Idioma.php:validation for field slug
msgid "Informe a sigla do idioma"
msgstr ""

#: Plugin/FdNoticias/Model/Noticia.php:validation for field url_seo
#: Plugin/FdNoticias/Model/NoticiaCategoria.php:validation for field url_seo
#: Plugin/FdPaginas/Model/Pagina.php:validation for field url_seo
msgid "Informe a url de acesso desta página"
msgstr ""

#: Plugin/FdNoticias/Model/Noticia.php:validation for field url_seo
#: Plugin/FdNoticias/Model/NoticiaCategoria.php:validation for field url_seo
msgid "isUnique"
msgstr ""

#: Plugin/FdRotas/Model/Rota.php:validation for field seo_url;validation for field controller;validation for field model;validation for field action
msgid "Campo de preenchimento obrigatório."
msgstr ""

#: Plugin/FdUsuarios/Model/Grupo.php:validation for field nome
msgid "O nome do grupo é obrigatório"
msgstr ""


<?php $this->start('title') ?><?php echo $galeria['Galeria']['titulo'] ?> - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<?php $this->start('script') ?>
<?php echo $this->Html->script('site/galerias/main'); ?>
<?php $this->end() ?>

<?php
$videos = $galeria['Video'];
$fotos = $galeria['Foto'];
?>

<section class="galeria-interna">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3">
				<h1>Galeria</h1>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9">
				<h2><?php echo $galeria['Galeria']['titulo'] ?></h2>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12" id="slideshow-1">
				<div class="cycle-slideshow" id="cycle-1" data-cycle-slides=".wrapper" data-cycle-timeout=0>
					<?php if(count($videos) > 0): ?>
						<?php foreach($videos as $video): ?>
							<div class="wrapper">
								<?php echo $this->Youtube->video($video['video'], array('theme' => 'Dark'), array('width' => '100%', 'height' => '100%')) ?>
								<p><?php echo $video['legenda'] ?></p>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if(count($fotos) > 0): ?>
						<?php foreach($fotos as $foto): ?>
							<div class="wrapper">
								<div class="img">
									<?php echo $this->Html->image('../files/foto/foto/'.$foto['id'].'/xvga_'.$foto['foto'], array('alt' => $foto['titulo'], 'class' => 'img-responsive')) ?>
								</div>
								<p><?php echo $foto['legenda'] ?></p>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="thumbnails">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12" id="slideshow-2">
				<div class="cycle-slideshow" id="cycle-2" data-cycle-timeout=0 data-cycle-fx="carousel" data-cycle-prev="#slideshow-2 .cycle-prev" data-cycle-next="#slideshow-2 .cycle-next" data-cycle-slides=".thumb" data-cycle-carousel-visible="5" data-cycle-carousel-fluid=true data-allow-wrap="false">
					<?php if(count($videos) > 0): ?>
						<?php foreach($videos as $video): ?>
							<div class="col-lg-2 col-md-2 thumb">
								<?php echo $this->Youtube->thumbnail($video['video'], 'large', array('alt' => $video['titulo'], 'class' => 'img-responsive')) ?>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if(count($fotos) > 0): ?>
						<?php foreach($fotos as $foto): ?>
							<div class="col-lg-2 col-md-2 thumb">
								<?php echo $this->Html->image('../files/foto/foto/'.$foto['id'].'/vga_'.$foto['foto'], array('alt' => $foto['titulo'], 'class' => 'img-responsive')) ?>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<a href="#" class="cycle-prev">&laquo; prev</a> <a href="#" class="cycle-next">next &raquo;</a>
			</div>
		</div>
	</div>
</section>
<?php $this->start('title') ?>Galerias - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>


<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="galerias">
					<h1>Galerias</h1>
					<div class="migalhas-de-pao hidden-xs">
						<?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('Galerias', '#'); ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="galerias">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<h2><strong>Fotos</strong><br>e Videos</h2>
				<hr>
				<?php foreach($galerias as $i => $galeria): ?>
				<?php $i = $i + 1; ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<?php if(count($galeria['Foto']) == 0): ?>
						<?php echo $this->Html->link($this->Youtube->thumbnail($galeria['Video'][0]['video'], 'large', array('class' => 'img-responsive')), '/'.$galeria['Galeria']['url_seo'], array('alt' => $galeria['Galeria']['titulo'], 'escape' => false))  ?>
					<?php else: ?>
						<?php echo $this->Html->link($this->Html->image('../files/foto/foto/'.$galeria['Foto'][0]['id'].'/vga_'.$galeria['Foto'][0]['foto'], array('class' => 'img-responsive')), '/'.$galeria['Galeria']['url_seo'], array('alt' => $galeria['Galeria']['titulo'], 'escape' => false))  ?>
					<?php endif; ?>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="row wrapper">
							<h3><?php echo $this->Html->link($galeria['Galeria']['titulo'], '/'.$galeria['Galeria']['url_seo'])  ?></h3>
						</div>
					</div>
				</div>
				<?php if($i > 0 && $i%3 == 0): ?>
				<div class="clearfix"></div>
				<?php endif; ?>
				<?php endforeach; ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php echo $this->element('site/paginacao') ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php

class TextoHelper extends AppHelper {

	public function titulo($titulo, $numero = 1, $tag = 'h2', $br = 'sim')
	{
		$separa = explode(' ', $titulo);

		$numero = ($numero == 1 ? 0 : $numero-1);

		$parte1 = '';
		$parte2 = '';
		for($i = 0; $i < count($separa); $i++) {
			if($i <= $numero) {
				$parte1 .= ' ' . $separa[$i];
			} else {
				$parte2 .= ' ' . $separa[$i];
			}
		}

		$parte1 = substr($parte1, 1);
		$parte2 = substr($parte2, 1);

		$retorna = '<'.$tag.'>';
		$retorna .= '<span>';
		$retorna .= $parte1;
		$retorna .= '</span>';
		$retorna .= $br == 'sim' ? '<br>' : '';
		$retorna .= $parte2;
		$retorna .= '</'.$tag.'>';

		return $retorna;
	}

	public function conteudo($contendo, $completo = true, $tag = '<hr>')
	{
		if($completo == false)
		{
			$string = explode($tag, $contendo);
			return $string[0];
		} else {
			return str_replace($tag, '', $contendo);
		}
	}

	public function primeiroParagrafo($str)
	{
		$str = substr($str, 0, strpos($str, '</p>') + 4);
		$str = str_replace('<p>', '', str_replace('<p/>', '', $str));

		return $str;
	}

    public function array_group_by(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }
        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;
        // Load the new array, splitting by the target key
        $grouped = array();
        foreach ($array as $value) {
            $key = null;
            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && isset($value->{$_key})) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }
            if ($key === null) {
                continue;
            }
            $grouped[$key][] = $value;
        }
        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();
            foreach ($grouped as $key => $value) {
                $params = array_merge(array($value), array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
            }
        }

//        echo '<pre>';
//        die(print_r($grouped));
//

        return $grouped;
    }

}
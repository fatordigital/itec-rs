<?php

class LanguageHelper extends AppHelper {

	public $helpers = array('Html','Session');

	public function link($title, $url = null, $options = array(), $confirmMessage = false)
	{
		if (!isset($url['language']) || !$url['language'])
		{
			if ((Configure::read('Config.language') != 'pt') || ($this->Session->read('LANGUAGE') != 'pt'))
			{
			    $url['language'] = Configure::read('Config.language') ? Configure::read('Config.language') : $this->Session->read('LANGUAGE');
			}
		}
		return $this->Html->link($title, $url, $options, $confirmMessage);
	}

}
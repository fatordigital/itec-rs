<?php

class TituloHelper extends AppHelper {

	public function init($titulo, $numero = 1)
	{
		$separa = explode(' ', $titulo);

		$numero = ($numero == 1 ? 0 : $numero-1);

		$parte1 = '';
		$parte2 = '';
		for($i = 0; $i < count($separa); $i++) {
			if($i <= $numero) {
				$parte1 .= ' ' . $separa[$i];
			} else {
				$parte2 .= ' ' . $separa[$i];
			}
		}

		$parte1 = substr($parte1, 1);
		$parte2 = substr($parte2, 1);

		$retorna = '<h2>';
		$retorna .= '<span>';
		$retorna .= $parte1;
		$retorna .= '</span><br>';
		$retorna .= $parte2;
		$retorna .= '</h2>';

		return $retorna;
	}

	public function limpa($titulo)
	{
		return str_replace('|', ' ', $titulo);
	}

}
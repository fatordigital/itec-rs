<?php

App::uses('HtmlHelper', 'View/Helper');
class MyHtmlHelper extends HtmlHelper {

    public $helpers = array(
        'Session'
    );

    public function link ( $title , $url = null , $options = array() , $confirmMessage = false ) {

        $session = $this->Session->read('LANGUAGE');


        if(is_array($url)) {
            if( !isset($url['language']) ){
                if(isset($params['language']) && $this->params->params['language'] != 'pt'){
                    $url['language'] = $this->params->params['language'];
                }elseif($session != false){
                    if($session != 'pt'){
                        $url['language'] = $session;
                    } 
                }
            }
        }else{
            if($session != 'pt'){
                if(!is_array($url)){
                    if($session != null && $url != ""){
                        if(strpos($url, $session.'/') === false){
                            $url = '/'.$session.'/'.$url;
                        }
                    }
                }else{
                    $url['language'] = $session;
                }
            }
        }
        return parent::link($title, $url, $options, $confirmMessage);
    }

    public function activeLink($title, $url = null, $options = array(), $confirmMessage = false){
        if(is_array($url)){
            $this->params->params['controller'] = strtolower($this->params->params['controller']);
            if($this->params->params['controller'] == $url['controller']){
                if(isset($options['class'])) {
                  $options['class'] .= ' ativo';
                } else {
                  $options['class'] = 'ativo';
                }
            }
        }else{
            if ($this->request->here == $url) {
                if(isset($options['class'])) {
                  $options['class'] .= ' ativo';
                } else {
                  $options['class'] = 'ativo';
                }
            }
        }
        return parent::link($title, $url, $options, $confirmMessage);
    }


}
sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <!-- link início -->
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-dashboard"></i><span>Início</span>',
                        array('plugin' => 'fd_dashboard',
                            'controller' => 'fd_dashboard',
                            'action' => 'index',
                            'prefix' => 'fatorcms'),
                        array('class' => (($this->params['controller'] == 'fd_dashboard') && ($this->params['action'] == 'fatorcms_index')) ? 'active' : '',
                            'escape' => false)) ?>
                </li>

                <li>
                    <?php echo $this->Html->link(
                        '</i><span>Revista</span>',
                        array('plugin' => 'fd_edicoes',
                            'controller' => 'fd_revistas',
                            'action' => 'index',
                            'prefix' => 'fatorcms'),
                        array('class' => (($this->params['controller'] == 'fd_revistas') && ($this->params['action'] == 'fatorcms_index')) ? 'active' : '',
                            'escape' => false)) ?>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<span>Edições</span>',
                        array('plugin' => 'fd_edicoes',
                            'controller' => 'fd_edicoes',
                            'action' => 'index',
                            'prefix' => 'fatorcms'),
                        array('class' => (($this->params['controller'] == 'fd_edicoes') && ($this->params['action'] == 'fatorcms_index')) ? 'active' : '',
                            'escape' => false)) ?>
                </li>
                <!-- link início -->
                <?php if($this->Session->read('Auth.User.grupo_id') == 9): ?>
                <!-- categorias & notícias -->
                <li class="sub-menu">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-rss"></i><span>Notícias & Categorias de Notícias</span>',
                        '#',
                        array('class' => ((($this->params['controller'] == 'fd_noticias') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_noticias') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_noticias') && ($this->params['action'] == 'fatorcms_editar'))
                            || (($this->params['controller'] == 'fd_noticias_categorias') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_noticias_categorias') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_noticias_categorias') && ($this->params['action'] == 'fatorcms_editar'))) ? 'active' : '',
                            'escape' => false)) ?>
                    <ul class="sub">
                        <li><?php echo $this->Html->link(
                            'Categorias',
                            array('plugin' => 'fd_noticias',
                                'controller' => 'fd_noticias_categorias',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                        <li><?php echo $this->Html->link(
                            'Notícias',
                            array('plugin' => 'fd_noticias',
                                'controller' => 'fd_noticias',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                    </ul>
                </li>
                <!-- categorias & notícias -->
                <!-- galerias, vídeos e fotos -->
                <li class="sub-menu">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-camera"></i><span>Galerias, vídeos e fotos</span>',
                        '#',
                        array('class' => ((($this->params['controller'] == 'fd_galerias') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_galerias') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_galerias') && ($this->params['action'] == 'fatorcms_editar'))
                            || (($this->params['controller'] == 'fd_videos') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_videos') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_videos') && ($this->params['action'] == 'fatorcms_editar'))
                            || (($this->params['controller'] == 'fd_fotos') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_fotos') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_fotos') && ($this->params['action'] == 'fatorcms_editar'))) ? 'active' : '', 'escape' => false)
                    ); ?>
                    <ul class="sub">
                        <li><?php echo $this->Html->link(
                            'Galerias',
                            array('plugin' => 'fd_galerias',
                                'controller' => 'fd_galerias',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                        <li><?php echo $this->Html->link(
                            'Vídeos',
                            array('plugin' => 'fd_galerias',
                                'controller' => 'fd_videos',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                        <li><?php echo $this->Html->link(
                            'Fotos',
                            array('plugin' => 'fd_galerias',
                                'controller' => 'fd_fotos',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                    </ul>
                </li>
                <!-- galerias -->
                <!-- livros -->
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-book"></i><span>Livros</span>',
                        array('plugin' => 'fd_livros',
                                'controller' => 'fd_livros',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                        array('class' => ((($this->params['controller'] == 'fd_livros') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_livros') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_livros') && ($this->params['action'] == 'fatorcms_editar'))) ? 'active' : '', 'escape' => false)
                    ); ?>
                </li>
                <!-- livros -->
                <!-- usuários, grupos e acl -->

                <li class="sub-menu">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-users"></i><span>Grupos, Usuários e ACL</span>',
                        '#',
                        array('class' => ((($this->params['controller'] == 'fd_usuarios') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_usuarios') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_usuarios') && ($this->params['action'] == 'fatorcms_editar'))
                            || (($this->params['controller'] == 'fd_grupos') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_grupos') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_grupos') && ($this->params['action'] == 'fatorcms_editar'))
                            || (($this->params['controller'] == 'acl') && ($this->params['action'] == 'index'))
                            || (($this->params['controller'] == 'acl') && ($this->params['action'] == 'permissions'))) ? 'active' : '',
                            'escape' => false)) ?>
                    <ul class="sub">
                        <li><?php echo $this->Html->link(
                            'Grupos',
                            array('plugin' => 'fd_usuarios',
                                'controller' => 'fd_grupos',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                        <li><?php echo $this->Html->link(
                            'Usuários',
                            array('plugin' => 'fd_usuarios',
                                'controller' => 'fd_usuarios',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                        <li><?php echo $this->Html->link(
                            'ACL',
                            array('plugin' => 'acl_manager',
                                'controller' => 'acl',
                                'action' => 'index'),
                                array('escape' => false)) ?></li>
                    </ul>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-users"></i><span>Usuários</span>',
                        array('plugin' => 'fd_usuarios',
                                'controller' => 'fd_usuarios',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                        array('class' => ((($this->params['controller'] == 'fd_usuarios') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_usuarios') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_usuarios') && ($this->params['action'] == 'fatorcms_editar'))) ? 'active' : '', 'escape' => false)
                    ); ?>
                </li>
                <li class="sub-menu">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-cogs"></i><span>Configurações</span>',
                        '#',
                        array('class' => ((($this->params['controller'] == 'fd_configuracoes') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_configuracoes') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_configuracoes') && ($this->params['action'] == 'fatorcms_editar'))
                            || (($this->params['controller'] == 'fd_idiomas') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_idiomas') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_idiomas') && ($this->params['action'] == 'fatorcms_editar'))
                            || (($this->params['controller'] == 'fd_rotas') && ($this->params['action'] == 'fatorcms_index'))
                            || (($this->params['controller'] == 'fd_rotas') && ($this->params['action'] == 'fatorcms_criar'))
                            || (($this->params['controller'] == 'fd_rotas') && ($this->params['action'] == 'fatorcms_editar'))) ? 'active' : '',
                            'escape' => false)) ?>
                    <ul class="sub">
                        <li><?php echo $this->Html->link(
                            'Configurações Gerais',
                            array('plugin' => 'fd_configuracoes',
                                'controller' => 'fd_configuracoes',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                        <!-- <li><?php echo $this->Html->link(
                            'Idiomas',
                            array('plugin' => 'fd_idiomas',
                                'controller' => 'fd_idiomas',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li> -->
                        <li><?php echo $this->Html->link(
                            'Rotas',
                            array('plugin' => 'fd_rotas',
                                'controller' => 'fd_rotas',
                                'action' => 'index',
                                'prefix' => 'fatorcms'),
                                array('escape' => false)) ?></li>
                    </ul>
                </li>
                <?php endif; ?>
                <!-- configurações -->
            </ul>
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end
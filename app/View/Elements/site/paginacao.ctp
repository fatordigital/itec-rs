<div class="text-center">
	<?php if ($this->Paginator->hasPage(2)): ?>
	<ul class="paginacao list-inline">
		<?php echo $this->Paginator->prev(' << ', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a')) ?>
		<?php echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)); ?>
		<?php echo $this->Paginator->next(' >> ', array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a')) ?>
	</ul>
	<?php endif; ?>
</div>
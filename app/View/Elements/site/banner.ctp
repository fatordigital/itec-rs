<?php echo $this->Html->script('site/banner'); ?>
<section class="banner" <?php echo preg_match('/MSIE 8/', $_SERVER['HTTP_USER_AGENT']) ? 'style="top: -15px;"' : '' ?>>
	<div class="row">
		<?php if(!preg_match('/MSIE 8/', $_SERVER['HTTP_USER_AGENT'])): ?>
		<div class="hidden-xs hidden-sm col-md-12 col-lg-12" style="padding-bottom: 50%; margin-top: -22px">
			<div id="swiffycontainer" style="position: absolute; top: -85px; left: 0; right: 0; bottom: 0;"></div>
		</div>
		<?php else: ?>
		<div class="hidden-xs hidden-sm col-md-12 col-lg-12">
			<?php echo $this->Html->image('site/1024-e-768.jpg', array('width' => '100%')) ?>
		</div>
		<?php endif; ?>
		<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
			<?php echo $this->Html->image('site/444.jpg') ?>
		</div>
		<div class="col-sm-12 hidden-xs hidden-md hidden-lg">
			<?php echo $this->Html->image('site/1024-e-768.jpg') ?>
		</div>
	</div>
	<?php if(!preg_match('/MSIE 8/', $_SERVER['HTTP_USER_AGENT'])): ?>
    <?php echo $this->Html->script('site/banner.html5') ?>
	<?php endif; ?>

</section>
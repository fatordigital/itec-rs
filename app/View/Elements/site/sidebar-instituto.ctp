<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
	<ul class="menu-institucional">
		<li>
			<?php echo $this->Html->link('O Instituto', '/o-instituto') ?>
			<ul class="hidden-sm hidden-xs">
				<li><?php echo $this->Html->link('POR QUE TRANSDISCIPLINAR?','/o-instituto#' . Inflector::slug(strtolower('POR QUE TRANSDISCIPLINAR?'), '-')) ?></li>
				<li><?php echo $this->Html->link('CARTA DE TRANSDISCIPLINARIDADE', '/o-instituto#' . Inflector::slug(strtolower('CARTA DE TRANSDISCIPLINARIDADE'), '-')) ?></li>
			</ul>
		</li>
		<?php /*<li><?php echo $this->Html->link('Histórico', '/historico') ?></li>*/ ?>
		<li><?php echo $this->Html->link('Membros', '/membros') ?></li>
		<li><?php echo $this->Html->link('Pensadores', '/pensadores') ?></li>
	</ul>
</div>
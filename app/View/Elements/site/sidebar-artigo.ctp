<div class="col-md-4 col-sm-6 col-xs-12 class1">
    <figure>
        <?php if (is_file(WWW_ROOT . 'files' . DS . 'revista' . DS . 'imagem' . DS . $revista['Revista']['id'] . DS . $revista['Revista']['imagem'])) { ?>
            <img src="<?php echo $this->Html->Url('/files/revista/imagem/' . $revista['Revista']['id'] . '/custom_' . $revista['Revista']['imagem'], true) ?>" width="309" alt="<?php echo $revista['Revista']['nome'] ?>" />
        <?php } ?>
    </figure>

    <div id="accordion">
        <div class="item-sel">
            <span class="icon"></span>

            <?php
                $url = 'rec';
                if($idioma != 'pt'){
                    $url  = $idioma .'/'. $url;
                }
            ?>
            <a href="<?php echo $this->Html->Url('/' . $url, true) ?>">
                <span class="text-sel">
                    <?php echo $idioma == 'pt' ? 'Apresentação' : 'Presentation' ?>
                </span>
            </a>
            <div class="clear"></div>
        </div>

        <div class="item-sel accordion-toggle">
            <span class="icon"></span>
            <span class="text-sel"><?php echo $idioma == 'pt' ? 'Número atual' : 'Current Issue' ?></span>
            <div class="clear"></div>
        </div>

        <div class="accordion-content"<?php echo isset($this->params['named']['numero-atual']) ? ' style="display: block;"' : '' ?>>
            <div class="accordion-int">
                <div class="item-sel-int">
                    <span class="icon"></span>
                    <?php
                    $url = 'edicoes/numero-atual:'.$ultimo_numero['Edicao']['numero'];
                    if($idioma != 'pt'){
                        $url  = $idioma .'/'. $url;
                    }
                    ?>
                    <a href="<?php echo $this->Html->Url('/'.$url, true) ?>">
                        <span class="text-sel">
                            <strong><?php echo $ultimo_numero['Edicao']['numero'] ?></strong>
                        </span>
                    </a>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div class="item-sel accordion-toggle">
            <span class="icon"></span>
            <span class="text-sel"><?php echo $idioma == 'pt' ? 'Números anteriores' : 'Archive' ?></span>
            <div class="clear"></div>
        </div>

        <div class="accordion-content"<?php echo isset($this->params['named']['numero-anterior']) ? ' style="display: block;"' : '' ?>>
            <div class="accordion-int">
                <?php foreach ($atributos as $ano => $attr) { ?>
                    <div class="item-sel-int accordion-toggle-int">
                        <span class="icon"></span>
                        <span class="text-sel"><?php echo isset($numero_anterior[0]['Edicao']['ano']) && $numero_anterior[0]['Edicao']['ano'] == $ano ? '<strong>'.$ano.'</strong>' : $ano; ?></span>
                        <div class="clear"></div>
                    </div>
                    <div class="accordion-content-int"<?php echo isset($numero_anterior[0]['Edicao']['ano']) && $numero_anterior[0]['Edicao']['ano'] == $ano ? ' style="display: block;"' : '' ?>>
                        <ul>
                            <?php $numeros_ja_chamados = array() ?>
                            <?php foreach (Set::sort($atributos[$ano], '{n}.numero', 'desc') as $t) { ?>
                                <?php if (!in_array($t['numero'], $numeros_ja_chamados)) { ?>
                                    <li>
                                        <?php
                                        $url = 'edicoes/numero-anterior:'.$t['numero'];
                                        if($idioma != 'pt'){
                                            $url  = $idioma .'/'. $url;
                                        }
                                        ?>
                                        <a href="<?php echo $this->Html->Url('/' . $url, true) ?>">
                                            <?php echo $t['numero'] ?>
                                        </a>
                                    </li>
                                    <?php $numeros_ja_chamados[] = $t['numero'] ?>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="item-sel">
            <span class="icon"></span>
            <span class="text-sel">
                <?php
                $url = 'edicoes/escopo:ler';
                if($idioma != 'pt'){
                    $url  = $idioma .'/'. $url;
                }
                ?>

                <a href="<?php echo $this->Html->Url('/' . $url, true) ?>">
                    <?php if (isset($this->params['named']['escopo'])) { ?>
                        <strong><?php echo $idioma == 'pt' ? 'Escopo e periodicidade' : 'Scope' ?></strong>
                    <?php } else { ?>
                        <?php echo $idioma == 'pt' ? 'Escopo e periodicidade' : 'Scope' ?>
                    <?php } ?>
                </a>
            </span>
            <div class="clear"></div>
        </div>

        <div class="item-sel">
            <span class="icon"></span>
            <span class="text-sel">
                <?php
                $url = 'edicoes/equipe:ler';
                if($idioma != 'pt'){
                    $url  = $idioma .'/'. $url;
                }
                ?>

                <a href="<?php echo $this->Html->Url('/' . $url, true) ?>">
                    <?php if (isset($this->params['named']['equipe'])) { ?>
                        <strong><?php echo $idioma == 'pt' ? 'Equipe' : 'Editorial Team' ?></strong>
                    <?php } else { ?>
                        <?php echo $idioma == 'pt' ? 'Equipe' : 'Editorial Team' ?>
                    <?php } ?>
                </a>
            </span>
            <div class="clear"></div>
        </div>

        <div class="item-sel">
            <span class="icon"></span>
            <span class="text-sel" style="max-width: 260px;">
                <?php
                $url = 'edicoes/diretrizes:ler';
                if($idioma != 'pt'){
                    $url  = $idioma .'/'. $url;
                }
                ?>

                <a href="<?php echo $this->Html->Url('/' . $url, true) ?>">
                    <?php if (isset($this->params['named']['diretrizes'])) { ?>
                        <strong><?php echo $idioma == 'pt' ? 'Diretrizes éticas' : 'Publication Ethics and Publication Malpractice Statement' ?></strong>
                    <?php } else { ?>
                        <?php echo $idioma == 'pt' ? 'Diretrizes éticas' : 'Publication Ethics and Publication Malpractice statement' ?>
                    <?php } ?>
                </a>
            </span>
            <div class="clear"></div>
        </div>

        <div class="item-sel">
            <span class="icon"></span>
            <span class="text-sel">
                <?php
                $url = 'edicoes/regras:ler';
                if($idioma != 'pt'){
                    $url  = $idioma .'/'. $url;
                }
                ?>

                <a href="<?php echo $this->Html->Url('/' . $url, true) ?>">
                    <?php if (isset($this->params['named']['regras'])) { ?>
                        <strong><?php echo $idioma == 'pt' ? 'Regras para publicação' : 'Instructions to Authors' ?></strong>
                    <?php } else { ?>
                        <?php echo $idioma == 'pt' ? 'Regras para publicação' : 'Instructions to Authors' ?>
                    <?php } ?>
                </a>
            </span>
            <div class="clear"></div>
        </div>

        <div class="item-sel accordion-toggle">
            <span class="icon"></span>
            <a href="<?php echo $revista['Revista']['link_aquisicao'] ?>" target="_blank">
                <span class="text-sel">
                    <?php if (isset($this->params['named']['link_aquisicao'])) { ?>
                        <strong><?php echo $idioma == 'pt' ? 'Assinatura e aquisição de exemplares' : 'Subscription information' ?></strong>
                    <?php } else { ?>
                        <?php echo $idioma == 'pt' ? 'Assinatura e aquisição de exemplares' : 'Subscription information' ?>
                    <?php } ?>
                </span>
            </a>
            <div class="clear"></div>
        </div>

        <div class="item-sel accordion-toggle">
            <span class="icon"></span>
            <a href="<?php echo $this->Html->Url('/edicoes/idioma:' . ($idioma == 'pt' ? 'en' : 'pt'), true) ?>">
                <span class="text-sel">
                    <?php echo $idioma == 'pt' ? 'English Version' : 'Versão em Português' ?>
                </span>
            </a>
            <div class="clear"></div>
        </div>

    </div>

    <a href="https://pt-br.facebook.com/RevistaEstudosCriminais/" target="_blank" class="facebook-button">
        <img src="<?php echo $this->Html->Url('/img/facebook.png', true);?>" alt="Facebook"/>
        <span class="hidden-sm">facebook.com/RevistaEstudosCriminais</span>
    </a>

    <br>
    <br>
    <h2>
        <?php echo $idioma == 'pt' ? 'Buscar por' : 'Search' ?>:
    </h2>
    <?php
    $currentIdioma = '';
    if($idioma != 'pt'){
        $currentIdioma = '/' . $idioma;
    }
    ?>
    <form action="<?php echo $this->Html->Url($currentIdioma . '/rec', true) ?>" method="GET">
        <select id="procurar-por" required class="form-control">
            <option value="">-</option>
            <option value="autor"><?php echo $idioma == 'pt' ? 'Autor' : 'Author' ?></option>
            <option value="resumo"><?php echo $idioma == 'pt' ? 'Resumo' : 'Abstract' ?></option>
            <option value="titulo"><?php echo $idioma == 'pt' ? 'Título' : 'Title' ?></option>
            <option value="palavra-chave"><?php echo $idioma == 'pt' ? 'Palavra-chave' : 'Keyword' ?></option>
        </select>
        <br>
        <input type="text" id="procurar-tipo" placeholder="<?php echo $idioma == 'pt' ? 'Selecione o tipo de busca' : 'Select the search criterion' ?>:" required />
        <br>
        <input type="submit" value="Pesquisar">
    </form>
</div>

<?php $this->start('script') ?>
<script type="text/javascript">
    var $tipo = $('#procurar-tipo');
    $('#procurar-por').on('change', function(){
        var $this = $(this);
        if ($this.val() != '') {
            $tipo.attr('name', $this.val());
            $tipo.attr('placeholder', 'Buscar por ' + $this.find(':selected').text());
        } else {
            $tipo.removeAttr('name');
            $tipo.attr('placeholder', 'Selecione o tipo de busca');
        }
    });
</script>
<?php $this->end() ?>

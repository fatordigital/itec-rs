<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-6">
				<div id="contato"></div>
				<h3>Contato</h3>
				<hr class="linha">
				<?php echo $this->Form->create('Contato', array('url' => array('controller' => 'contato', 'action' => 'index'), 'class' => 'form-horizontal form-contato', 'role' => 'form')) ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="group-form">
							<?php echo $this->Form->label('nome', 'Nome', array('class' => 'col-sm-3')); ?>
							<div class="col-sm-9">
								<?php echo $this->Form->text('nome', array('class' => 'form-control', 'data-rule-required' => 'true', 'data-msg-required' => 'Informe o seu nome')); ?>
							</div>
						</div>
						<div class="group-form">
							<?php echo $this->Form->label('email', 'E-mail', array('class' => 'col-sm-3')); ?>
							<div class="col-sm-9">
								<?php echo $this->Form->text('email', array('type' => 'email', 'class' => 'form-control', 'data-rule-required' => 'true', 'data-msg-required' => 'Informe o seu e-mail', 'data-rule-email' => 'true', 'data-msg-email' => 'Informe um e-mail válido')) ?>
							</div>
						</div>
						<div class="group-form">
							<?php echo $this->Form->label('mensagem', 'Mensagem', array('class' => 'col-sm-3')) ?>
							<div class="col-sm-9">
								<?php echo $this->Form->textarea('mensagem', array('class' => 'form-control', 'data-rule-required' => 'true', 'data-msg-required' => 'Informe a sua mensagem')) ?>
							</div>
						</div>
						<div class="retorno-contato col-sm-offset-3 col-sm-9"></div>
						<div class="group-form last">
							<div class="col-sm-offset-3 col-sm-9 text-right">
								<?php echo $this->Form->button('Limpar', array('type' => 'reset', 'class' => 'btn')) ?>
								<?php echo $this->Form->button('Enviar', array('type' => 'submit', 'class' => 'btn')) ?>
							</div>
						</div>
					</div>
				<?php echo $this->Form->end() ?>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6">
				<h3>Endereço</h3>
				<hr class="linha">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 endereco">
					<div class="row">
						<p>Rua Prof. Langendonck, n. 208</p>
						<p>Bairro Petrópolis - CEP 90630-060</p>
						<p>Porto Alegre / RS - Brasil</p>
						<p>&nbsp;</p>
						<p>Telefone: (51) 3029.5568</p>
						<p>E-mail: <?php echo $this->Html->link('contato@itecrs.com.br', 'mailto:contato@itecrs.com.br') ?></p>
						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
				<ul class="menu">
					<li><?php echo $this->Html->link('Instituto', '/o-instituto') ?></li>
					<li><?php echo $this->Html->link('Destaques', array('controller' => 'noticias', 'action' => 'index')) ?></li>
					<!--<li><?php // echo $this->Html->link('Revista', 'http://www.iobstore.com.br/ch/prod/vit/219473/216565/0/0/revista-de-estudos-criminais.aspx', array('target' => '_blank')) ?></li>-->
					<li><?php echo $this->Html->link('Revista', '/rec', array('target' => '_blank')) ?></li>
					<li><?php echo $this->Html->link('Indicações Bibliográficas', array('controller' => 'livros', 'action' => 'index')) ?></li>
					<li><?php echo $this->Html->link('Galeria', array('controller' => 'galerias', 'action' => 'index')) ?></li>
					<li><?php echo $this->Html->link('Contato', '#contato') ?></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
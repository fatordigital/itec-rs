<header>
    <div class="container  hidden-sm hidden-xs">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <?php echo $this->Html->link($this->Html->image('site/logo.jpg', array('class' => 'img-responsive', 'alt' => 'ITEC')), '/', array('escape' => false)) ?>
            </div>
            <ul class="menu list-inline col-lg-9 col-md-9">
                <li><?php echo $this->Html->activeLink('Home', '/') ?></li>
                <li>
                    <?php echo $this->Html->activeLink('O Instituto', '/o-instituto') ?>
                    <ul>
                        <li><?php echo $this->Html->activeLink('O Instituto', '/o-instituto') ?></li>
                        <?php /* <li><?php echo $this->Html->activeLink('Histórico', '/historico') ?></li> */ ?>
                        <li><?php echo $this->Html->activeLink('Membros', '/membros') ?></li>
                        <li><?php echo $this->Html->activeLink('Pensadores', '/pensadores') ?></li>
                    </ul>
                </li>
                <li><?php echo $this->Html->activeLink('Destaques', array('controller' => 'noticias', 'action' => 'index')) ?></li>
                <li><?php echo $this->Html->activeLink('Indicações Bibliográficas', array('controller' => 'livros', 'action' => 'index')) ?></li>
                <li><?php echo $this->Html->activeLink('Galerias', array('controller' => 'galerias', 'action' => 'index')) ?></li>
                <li><?php echo $this->Html->activeLink('Contato', '#contato') ?></li>
            </ul>
        </div>
    </div>
    <nav class="hidden-lg hidden-md"  role="navigation">
        <div class="cotainer">
            <div class="navbar-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="col-sm-6 col-xs-6 logo">
                                <?php echo $this->Html->link($this->Html->image('site/logo.jpg', array('class' => 'img-responsive', 'alt' => 'ITEC')), '/', array('escape' => false)) ?>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="menu-tablet-mobile nav navbar-nav">
                    <li><?php echo $this->Html->activeLink('Home', '/') ?></li>
                    <li><?php echo $this->Html->activeLink('O Instituto', '/o-instituto') ?></li>
                    <?php /* <li><?php echo $this->Html->activeLink('Histórico', '/historico') ?></li> */ ?>
                    <li><?php echo $this->Html->activeLink('Membros', '/membros') ?></li>
                    <li><?php echo $this->Html->activeLink('Pensadores', '/pensadores') ?></li>
                    <li><?php echo $this->Html->activeLink('Destaques', array('controller' => 'noticias', 'action' => 'index')) ?></li>
                    <li><?php echo $this->Html->activeLink('Indicações Bibliográficas', array('controller' => 'livros', 'action' => 'index')) ?></li>
                    <li><?php echo $this->Html->activeLink('Galerias', array('controller' => 'galerias', 'action' => 'index')) ?></li>
                    <li><?php echo $this->Html->activeLink('Contato', '#contato') ?></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
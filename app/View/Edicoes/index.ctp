<?php 
$this->start('title'); 
echo 'Revista de Estudos Criminais - ';
$this->end();
$this->start('keywords');
$this->end();
$this->start('description');
$this->end();
$this->start('author');
$this->end();
$arrayGet = array('autor', 'palavra-chave', 'resumo', 'titulo');
$arryKeysGet = array_keys($this->request->query);
$inArrayGet = array_intersect_assoc($arryKeysGet, $arrayGet) ? true : false;
$currentIdioma = ($idioma != 'pt') ? '/' . $idioma : '';

if (isset($this->params['named']['escopo'])) {
    $texto = $revista['Revista']['escopo_' . $idioma];
} elseif (isset($this->params['named']['equipe'])) {
    $texto = $revista['Revista']['equipe_' . $idioma];
} elseif (isset($this->params['named']['diretrizes'])) {
    $texto = '';
    if ($revista['Revista']['diretrizes_' . $idioma . '_file']) {
        $texto .= ''
            . '<a class="link_pdf_texto" href="'
            . $this->Html->Url('/files/revista/regras_en_file/'
                . $revista['Revista']['id'] . '/'
                . $revista['Revista']['diretrizes_' . $idioma . '_file'], true)
            . '" target="_blank">Clique aqui para download</a>'
            . '';
    }

    $texto .= $revista['Revista']['diretrizes_' . $idioma];
} elseif (isset($this->params['named']['regras'])) {
    $texto = '';
    if ($revista['Revista']['regras_' . $idioma . '_file']) {
        $texto .= ''
            . '<a class="link_pdf_texto" href="'
            . $this->Html->Url('/files/revista/regras_en_file/'
                . $revista['Revista']['id'] . '/'
                . $revista['Revista']['regras_' . $idioma . '_file'], true)
            . '" target="_blank">Clique aqui para download</a>'
            . '';
    }
    $texto .= $revista['Revista']['regras_' . $idioma];
} else {
    $texto = $revista['Revista']['apresentacao_' . $idioma];
}
?>

<section class="topo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="instituto">
                    <h1>Revista de <strong>Estudos Criminais</strong></h1>
                    <div class="migalhas-de-pao hidden-xs">
                        <?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('O Instituto', '#'); ?></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="o-instituto">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row"> 
                    <div class="hidden-xs"><?php echo $this->element('site/sidebar-artigo') ?></div>
                    <div class="col-md-8 col-sm-6 col-xs-12 class2">
                        <h2 class="tit-prin"><?php echo $titulo; ?></h2>
                        <?php
                        if (
                            (
                            isset($this->params['named']['escopo']) ||
                            isset($this->params['named']['equipe']) ||
                            isset($this->params['named']['diretrizes']) ||
                            isset($this->params['named']['regras']) ||
                            (isset($this->params['named']['ano']) && isset($this->params['named']['titulo']))
                            ) && count($this->request->query) == 0
                        ) {
                            echo $texto;
                        } else {
                            if (isset($procurar_autor)) {
                                ?>
                                <ul class="list-group">
                                    <?php foreach ($procurar_autor as $s) { ?>
                                        <li class="list-group-item">
                                            <strong>
                                                <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . $s['Edicao']['ano'] . '/titulo:' . $s['Edicao']['slug'], true) ?>">
                                                    <?php echo $s['Edicao']['titulo']; ?>
                                                </a>
                                            </strong>
                                            <p><br />
                                                <?php
                                                $arr = explode(';', $s['Edicao']['autores']);
                                                $autores = '';
                                                $ls_autors = count($arr) - 1;
                                                $ct_autors = 0;
                                                foreach ($arr as $kaut => $autor) {
                                                    $autores .= '<a class="text-danger" href="' . $this->Html->Url($currentIdioma . '/rec?autor=' . $autor, true) . ' ">';
                                                    $autores .= $autor;
                                                    $autores .= '</a>';
                                                    $ct_autors++;
                                                    if ($ct_autors == $ls_autors) {
                                                        $autores .= ' e ';
                                                    } elseif ($ct_autors < $ls_autors) {
                                                        $autores .= ', ';
                                                    } else {
                                                        $autores .= '. ';
                                                    }
                                                }
                                                echo $autores;
                                                ?>
                                            </p>
                                            <p>v.<strong><?php echo $s['Edicao']['volume'] ?></strong>, n.<strong><?php echo $s['Edicao']['numero'] ?></strong>, p.<strong><?php echo $s['Edicao']['paginacao'] ?></strong></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } elseif (isset($procurar_titulo)) { ?>
                                <ul class="list-group">
                                    <?php foreach ($procurar_titulo as $s) { ?>
                                        <li class="list-group-item">
                                            <strong>
                                                <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . $s['Edicao']['ano'] . '/titulo:' . $s['Edicao']['slug'], true) ?>">
                                                    <?php echo $s['Edicao']['titulo'] ?>
                                                </a>
                                            </strong>
                                            <p>
                                                <br>
                                                <?php
                                                $arr = explode(';', $s['Edicao']['autores']);
                                                $autores = '';
                                                $ls_autors = count($arr) - 1;
                                                $ct_autors = 0;
                                                foreach ($arr as $kaut => $autor) {
                                                    $autores .= '<a class="text-danger" href="' . $this->Html->Url($currentIdioma . '/rec?autor=' . $autor, true) . ' ">';
                                                    $autores .= $autor;
                                                    $autores .= '</a>';
                                                    $ct_autors++;
                                                    if ($ct_autors == $ls_autors) {
                                                        $autores .= ' e ';
                                                    } elseif ($ct_autors < $ls_autors) {
                                                        $autores .= ', ';
                                                    } else {
                                                        $autores .= '. ';
                                                    }
                                                }
                                                echo $autores;
                                                ?>
                                            </p>
                                            <p>
                                                v.<strong><?php echo $s['Edicao']['volume'] ?></strong>, n.<strong><?php echo $s['Edicao']['numero'] ?></strong>, p.<strong><?php echo $s['Edicao']['paginacao'] ?></strong>
                                            </p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } elseif (isset($procurar_resumo)) { ?>
                                <ul class="list-group">
                                    <?php foreach ($procurar_resumo as $s) { ?>
                                        <li class="list-group-item">
                                            <strong>
                                                <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . $s['Edicao']['ano'] . '/titulo:' . $s['Edicao']['slug'], true) ?>">
                                                    <?php echo $s['Edicao']['titulo'] ?>
                                                </a>
                                            </strong>
                                            <p>
                                                <br>
                                                <?php
                                                $arr = explode(';', $s['Edicao']['autores']);
                                                $autores = '';
                                                $ls_autors = count($arr) - 1;
                                                $ct_autors = 0;
                                                foreach ($arr as $kaut => $autor) {
                                                    $autores .= '<a class="text-danger" href="' . $this->Html->Url($currentIdioma . '/rec?autor=' . $autor, true) . ' ">';
                                                    $autores .= $autor;
                                                    $autores .= '</a>';
                                                    $ct_autors++;
                                                    if ($ct_autors == $ls_autors) {
                                                        $autores .= ' e ';
                                                    } elseif ($ct_autors < $ls_autors) {
                                                        $autores .= ', ';
                                                    } else {
                                                        $autores .= '. ';
                                                    }
                                                }
                                                echo $autores;
                                                ?>
                                            </p>
                                            <p>v.<strong><?php echo $s['Edicao']['volume'] ?></strong>, n.<strong><?php echo $s['Edicao']['numero'] ?></strong>, p.<strong><?php echo $s['Edicao']['paginacao'] ?></strong></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } elseif (isset($procurar_palavra_chave)) { ?>
                                <ul class="list-group">
                                    <?php foreach ($procurar_palavra_chave as $s) { ?>
                                        <li class="list-group-item">
                                            <strong>
                                                <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . $s['Edicao']['ano'] . '/titulo:' . $s['Edicao']['slug'], true) ?>">
                                                    <?php echo $s['Edicao']['titulo'] ?>
                                                </a>
                                            </strong>
                                            <p>
                                                <br>
                                                <?php
                                                $arr = explode(';', $s['Edicao']['autores']);
                                                $autores = '';
                                                $ls_autors = count($arr) - 1;
                                                $ct_autors = 0;
                                                foreach ($arr as $kaut => $autor) {
                                                    $autores .= '<a class="text-danger" href="' . $this->Html->Url($currentIdioma . '/rec?autor=' . $autor, true) . ' ">';
                                                    $autores .= $autor;
                                                    $autores .= '</a>';
                                                    $ct_autors++;
                                                    if ($ct_autors == $ls_autors) {
                                                        $autores .= ' e ';
                                                    } elseif($ct_autors < $ls_autors) {
                                                        $autores .= ', ';
                                                    } else {
                                                        $autores .= '. ';
                                                    }
                                                }
                                                echo $autores;
                                                ?>
                                            </p>
                                            <p>v.<strong><?php echo $s['Edicao']['volume'] ?></strong>, n.<strong><?php echo $s['Edicao']['numero'] ?></strong>, p.<strong><?php echo $s['Edicao']['paginacao'] ?></strong></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } elseif (isset($this->params['named']['edital-atual'])) { ?>
                                <?php foreach ($atributos[date('Y')] as $attr) { ?>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . (isset($this->params['named']['ano']) ? $this->params['named']['ano'] : date('Y')) . '/titulo:' . $attr['slug'], true) ?>">
                                                <?php if (isset($this->params['named']['titulo'])) { ?>
                                                    <?php if ($attr['slug'] == $this->params['named']['titulo'] || $attr['slug'] == $this->params['named']['titulo']) { ?>
                                                        <strong><?php echo $attr['titulo'] ?> <br><br> <?php echo $attr['paginacao'] ?></strong>
                                                    <?php } else { ?>
                                                            <?php echo $attr['titulo'] ?> <br><br> <?php echo $attr['paginacao'] ?>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                    <?php echo $attr['titulo'] ?> <br><br> páginas: <?php echo $attr['paginacao'] ?>
                                                <?php } ?>
                                            </a>
                                        </li>
                                    </ul>
                                <?php } ?>
                            <?php } elseif (isset($this->params['named']['ano'])) { ?>
                                <?php foreach ($atributos[$this->params['named']['ano']] as $attr) { ?>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . (isset($this->params['named']['ano']) ? $this->params['named']['ano'] : date('Y')) . '/titulo:' . $attr['slug'], true) ?>">
                                                <?php if (isset($this->params['named']['titulo'])) { ?>
                                                    <?php if ($attr['slug'] == $this->params['named']['titulo'] || $attr['slug'] == $this->params['named']['titulo']) { ?>
                                                        <strong><?php echo $attr['titulo'] ?> <br><br> <?php echo $attr['paginacao'] ?></strong>
                                                    <?php } else { ?>
                                                            <?php echo $attr['titulo'] ?> <br><br> <?php echo $attr['paginacao'] ?>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                    <?php echo $attr['titulo'] ?> <br><br> páginas: <?php echo $attr['paginacao'] ?>
                                                <?php } ?>
                                            </a>
                                        </li>
                                    </ul>
                                <?php } ?>
                            <?php } elseif (isset($this->params['named']['numero-atual'])) { ?>
                                <ul class="list-group">
                                    <?php foreach ($numero_atual as $s) { ?>
                                        <li class="list-group-item">
                                            <strong>
                                                <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . $s['Edicao']['ano'] . '/titulo:' . $s['Edicao']['slug'], true) ?>">
                                                    <?php echo $s['Edicao']['titulo_' . $s['Edicao']['idioma_original']] ?>
                                                </a>
                                            </strong>
                                            <p><br>
                                                <?php
                                                $arr = explode(';', $s['Edicao']['autores']);
                                                $autores = '';
                                                $ls_autors = count($arr) - 1;
                                                $ct_autors = 0;
                                                foreach ($arr as $kaut => $autor) {
                                                    $autores .= '<a class="text-danger" href="' . $this->Html->Url($currentIdioma . '/rec?autor=' . $autor, true) . ' ">';
                                                    $autores .= $autor;
                                                    $autores .= '</a>';
                                                    $ct_autors++;
                                                    if ($ct_autors == $ls_autors) {
                                                        $autores .= ' e ';
                                                    } elseif ($ct_autors < $ls_autors) {
                                                        $autores .= ', ';
                                                    } else {
                                                        $autores .= '. ';
                                                    }
                                                }
                                                echo $autores;
                                                ?>
                                            </p>
                                            <p>v.<strong><?php echo $s['Edicao']['volume'] ?></strong>, n.<strong><?php echo $s['Edicao']['numero'] ?></strong>, p.<strong><?php echo $s['Edicao']['paginacao'] ?></strong></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } elseif (isset($this->params['named']['numero-anterior'])) { ?>
                                <ul class="list-group">
                                    <?php foreach ($numero_anterior as $s) { ?>
                                        <li class="list-group-item">
                                            <strong>
                                                <a href="<?php echo $this->Html->Url($currentIdioma . '/edicoes/ano:' . $s['Edicao']['ano'] . '/titulo:' . $s['Edicao']['slug'], true) ?>">
                                                    <?php echo $s['Edicao']['titulo_' . $s['Edicao']['idioma_original']] ?>
                                                </a>
                                            </strong>
                                            <p>
                                                <br>
                                                <?php
                                                $arr = explode(';', $s['Edicao']['autores']);
                                                $autores = '';
                                                $ls_autors = count($arr) - 1;
                                                $ct_autors = 0;
                                                foreach ($arr as $kaut => $autor) {
                                                    $autores .= '<a class="text-danger" href="' . $this->Html->Url($currentIdioma . '/rec?autor=' . $autor, true) . ' ">';
                                                    $autores .= $autor;
                                                    $autores .= '</a>';
                                                    $ct_autors++;
                                                    if ($ct_autors == $ls_autors) {
                                                        $autores .= ' e ';
                                                    } elseif($ct_autors < $ls_autors) {
                                                        $autores .= ', ';
                                                    } else {
                                                        $autores .= '. ';
                                                    }
                                                }
                                                echo $autores;
                                                ?>
                                            </p>
                                            <p>v.<strong><?php echo $s['Edicao']['volume'] ?></strong>, n.<strong><?php echo $s['Edicao']['numero'] ?></strong>, p.<strong><?php echo $s['Edicao']['paginacao'] ?></strong></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } else { ?>
                                <?php echo $revista['Revista']['apresentacao_' . $idioma] ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="hidden-lg hidden-md hidden-sm"><?php echo $this->element('site/sidebar-artigo') ?></div>
                </div>
            </div>
        </div>
    </div>
</section>
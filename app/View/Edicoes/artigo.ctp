<?php $this->start('title') ?>O Instituto - <?php $this->end() ?>
<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>
<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="instituto">
					<h1>O <strong>Instituto</strong></h1>
					<div class="migalhas-de-pao hidden-xs"><?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('O Insitituto', '#'); ?></strong></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="o-instituto">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="hidden-xs"><?php echo $this->element('site/sidebar-artigo') ?></div>
					<div class="col-md-8 col-sm-6 col-xs-12 class2">
						    <div class="">
                                <a href="#"><div class="flag pt"></div></a>
                                <a href="#"><div class="flag en"></div></a>
								<div class="clear"></div>
							</div>
							<h2 class="tit-prin">Apresentação</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sapien augue, auctor a eros nec, vehicula laoreet leo. Vestibulum nec consequat diam. Vivamus congue augue urna, et vulputate eros congue ac. Mauris velit arcu, imperdiet condimentum elementum et, hendrerit convallis est. Etiam ut rutrum purus. Donec facilisis suscipit semper. Ut arcu urna, hendrerit sit amet diam sit amet, dictum egestas massa. Pellentesque imperdiet rutrum sagittis.</p>
							<p>Sed fermentum ex ac lacus dapibus convallis. Etiam bibendum mauris et odio lobortis malesuada. Nulla rutrum et orci in ultricies. Nunc tempus convallis felis, quis sollicitudin mi. Nunc dictum arcu et commodo posuere. Pellentesque efficitur ac lacus eu suscipit. Integer posuere, lectus nec semper posuere, orci erat rutrum risus, ac vestibulum quam libero et orci. Sed egestas ullamcorper lorem, egestas consequat lorem gravida a. Proin ultricies justo eget enim faucibus, non scelerisque sem </p>
							<p>Sed faucibus dui a ligula ultricies bibendum. Pellentesque porttitor maximus magna, eu laoreet massa auctor ut. In eget sagittis felis. Aliquam tempor a nisi et ultrices. Praesent tempus nibh vitae eleifend convallis. Suspendisse sollicitudin mattis vulputate. Proin mattis velit ut porta hendrerit. Vivamus arcu eros, faucibus a condimentum ut, varius molestie neque. Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                            <p>Curabitur euismod condimentum metus sit amet cursus. Praesent eget bibendum purus, a suscipit diam. Aliquam erat volutpat. Vivamus dictum, sem vel iaculis ultricies, urna enim vulputate augue, mattis efficitur massa ex at velit. Nulla ac eros vitae erat facilisis pharetra id vel orci. Pellentesque pretium vitae massa vitae aliquam. Vivamus vestibulum pellentesque purus et posuere. Quisque finibus quam ante, a hendrerit odio vulputate quis. Praesent massa orci, porta quis varius eu, mattis vitae nisl. Mauris congue cursus aliquam. Curabitur pretium nibh id leo commodo luctus. In ultrices dignissim ligula, eu hendrerit ipsum lobortis a. Praesent ultrices tellus sapien, id volutpat ex facilisis</p>
							<p>Vestibulum non dapibus felis. Suspendisse ac erat id massa porttitor eleifend at egestas odio. Maecenas pretium blandit sapien quis convallis. Phasellus scelerisque diam sed lobortis scelerisque. Nullam imperdiet sapien semper tempus maximus. Curabitur vitae nisl viverra, tincidunt neque quis, egestas nisi. Nulla varius lorem a egestas mollis. Sed hendrerit velit vitae neque bibendum, quis rhoncus lectus sollicitudin. Nulla vehicula ac neque sed rhoncus. Proin ante mauris, venenatis a tristique at, dignissim nec ligula.</p>
					</div>
					<div class="hidden-lg hidden-md hidden-sm">
						<?php echo $this->element('site/sidebar-artigo') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
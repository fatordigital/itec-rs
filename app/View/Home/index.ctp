<?php $this->start('title') ?>Home - <?php $this->end() ?>
<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="conceitual">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="cycle-slideshow">
					<!-- <div class="cycle-pager"></div> -->
					<?php echo $this->Html->image('site/banner-conceitual.png', array('class' => 'img-responsive', 'alt' => 'Banner Conceitual')) ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="frase">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
				<p>“Invente e morrerás perseguido como um criminoso; copie, e viverás contente como um tolo.” (Balzac)</p>
			</div>
		</div>
	</div>
</section>

<section class="destaques-capa">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h2><strong>DESTAQUES</strong> !TEC</h2>
				<hr>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<?php foreach($noticiasFotos as $noticiaFoto): ?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="destaque">
							<?php echo $this->Html->image('../files/noticia/imagem/'.$noticiaFoto['Noticia']['id'].'/'.$noticiaFoto['Noticia']['imagem'], array('class' => 'img-responsive', 'alt' => $noticiaFoto['Noticia']['titulo'])) ?>
							<div class="infos">
								<span><?php echo $noticiaFoto['NoticiaCategoria']['nome'] ?></span>
								<div class="clearfix"></div>
								<h3><?php echo $noticiaFoto['Noticia']['titulo'] ?></h3>
							</div>
							<?php echo $this->Html->link('Leia mais', '/'.$noticiaFoto['Noticia']['url_seo']) ?>
						</div>
					</div>
					<?php endforeach; ?>
					<?php foreach($noticias as $noticia): ?>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="outros-destaques">
							<span><?php echo $noticia['NoticiaCategoria']['nome'] ?></span>
							<div class="clearfix"></div>
							<h3><?php echo $noticia['Noticia']['titulo'] ?></h3>
							<p><?php echo $noticia['Noticia']['descricao_resumida'] ?></p>
							<hr>
							<?php echo $this->Html->link('Leia mais', '/'.$noticia['Noticia']['url_seo']) ?>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="sobre">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12.col-xs-12 wrapper">
				<div class="logo hidden-xs">
					<?php echo $this->Html->image('site/sobre-itec.jpg', array('class' => 'img-responsive pull-right', 'alt' => 'Sobre a ITEC')) ?>
				</div>
				<div class="content">
					<div>
						<h2><strong>O INSTITUTO</strong> TRANSDICIPLINAR DE ESTUDOS CRIMINAIS</h2>
						<p>A partir do encontro de jovens investigadores preocupados com o rumo da Política Criminal contemporânea no Programa de Pós-graduação em Ciências Criminais da Pontifícia Universidade Católica do Rio Grande do Sul (PUCRS), surgiu a necessidade de criação de um fórum de debate permanente que transcendesse a academia.</p>
						<p>Assim, o Instituto Transdisciplinar de Estudos Criminais (!TEC) foi criado no ano de 1998, objetivando divulgar os resultados da investigações de profissionais de inúmeras áreas do conhecimento preocupados com o fenômeno da violência em suas mais variadas formas de manifestação.</p>
						<?php echo $this->Html->link('Saiba mais sobre o Instituto', '/o-instituto', array('class' => 'btn')) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="revista">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h2>Conheça a revista <br><strong>de Estudos Criminais</strong></h2>
				<hr>
				<p></p>
				<ul>
					<li><a href="/rec" style="padding-left: 10px !important; padding-right: 10px !important;">Número Atual</a><br /><br /></li>
					<li><a href="/edicoes/diretrizes:ler" style="padding-left: 10px !important; padding-right: 10px !important;">Conselho Editorial</a><br /><br /></li>
                    <li><a href="/edicoes/diretrizes:regras" style="padding-left: 10px !important; padding-right: 10px !important;">Regras de publicação</a><br /><br /></li>
                    <li><a href="http://www.iobstore.com.br/ch/prod/vit/219473/216565/0/0/revista-de-estudos-criminais.aspx" style="padding-left: 10px !important; padding-right: 10px !important;" target="_blank">Adquira já sua assinatura!</a></li>
				</ul>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="row">
					<?php echo $this->Html->image('site/revista.jpg', array('class' => 'smartphone', 'alt' => 'Revista')) ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	
	
    
</section>
<section class="indicacoes">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<h2>Indicações <strong>Bibliográficas</strong></h2>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
				<p></p>
			</div>
			<div class="clearfix"></div>
			<hr>
			<?php foreach($livros as $livro): ?>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 livro">
				<div class="capa center-block">
					<?php echo $this->Html->image('site/zoom.jpg', array('class' => 'zoom', 'alt' => 'Zoom')) ?>
					<?php echo $this->Html->image('../files/livro/capa/'.$livro['Livro']['id'].'/'.$livro['Livro']['capa'], array('alt' => $livro['Livro']['titulo'], 'class' => 'img-responsive')) ?>
				</div>
				<h3><?php echo $livro['Livro']['titulo'] ?></h3>
				<p><?php echo $livro['Livro']['descricao_resumida'] ?></p>
				<hr>
				<?php echo $this->Html->link('Saiba mais', '/'.$livro['Livro']['url_seo']) ?>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
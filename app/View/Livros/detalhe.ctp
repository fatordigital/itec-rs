<?php $this->start('title') ?><?php echo $livro['Livro']['titulo'] ?> - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="indicacoes">
					<h1>INDICAÇÕES <strong>BIBLIOGRÁFICAS</strong></h1>
					<div class="migalhas-de-pao hidden-xs">
						<?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('Indicações Bibliográficas', '#'); ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="indicacoes-bibliograficas-interna">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h2><?php echo $livro['Livro']['titulo'] ?></h2>
				<hr>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<?php echo $this->Html->image('../files/livro/capa/'.$livro['Livro']['id'].'/'.$livro['Livro']['capa'], array('alt' => $livro['Livro']['titulo'], 'class' => 'img-responsive')) ?>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
					<?php echo $livro['Livro']['descricao'] ?>
				</div>
			</div>
		</div>
	</div>
</section>
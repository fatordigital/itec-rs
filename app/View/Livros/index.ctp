<?php $this->start('title') ?>Indicações Bibliográficas - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="indicacoes">
					<h1>INDICAÇÕES <strong>BIBLIOGRÁFICAS</strong></h1>
					<div class="migalhas-de-pao hidden-xs">
						<?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('Indicações Bibliográficas', '#'); ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="indicacoes-bibliograficas">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<h2>Indicações Bibliográficas</h2>
					<hr>
					<?php foreach($livros as $i => $livro): ?>
						<?php $i = $i + 1; ?>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 livro">
							<div class="capa center-block">
								<?php echo $this->Html->image('site/zoom.jpg', array('class' => 'zoom', 'alt' => 'Zoom')) ?>
								<?php echo $this->Html->image('../files/livro/capa/'.$livro['Livro']['id'].'/'.$livro['Livro']['capa'], array('class' => 'img-responsive', 'alt' => $livro['Livro']['titulo'])) ?>
							</div>
							<h3><?php echo $livro['Livro']['titulo'] ?></h3>
							<p><?php echo $livro['Livro']['descricao_resumida'] ?></p>
							<hr>
							<?php echo $this->Html->link('Saiba mais', '/'.$livro['Livro']['url_seo']) ?>
						</div>
						<?php if($i > 0 && $i%4 == 0): ?>
						<div class="clearfix"></div>
						<?php endif; ?>
					<?php endforeach; ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php echo $this->element('site/paginacao') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php echo $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->fetch('title') ?><?php echo Configure::read('Configure.nome_site') ?></title>
        <?php echo $this->Html->meta('icon') ?>
        <meta name="keywords" content="<?php echo ($this->fetch('keywords') ? $this->fetch('keywords') : Configure::read('Configure.keywords')) ?>">
        <meta name="description" content="<?php echo ($this->fetch('description') ? $this->fetch('description') : Configure::read('Configure.description')) ?>">
        <meta name="author" content="<?php echo ($this->fetch('author') ? $this->fetch('author') : Configure::read('Configure.author')) ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="<?php echo $this->Html->url('/') ?>">

        <!-- Tags OG para compartilhamento em redes sociais -->

        <meta property="og:locale" content="pt_BR">
        <meta property="og:url" content="<?php echo $this->Html->url(null, true) ?>">
        <meta property="og:title" content="<?php echo str_replace(' - ', '', $this->fetch('title')) ?>">
        <meta property="og:site_name" content="<?php echo Configure::read('Configure.nome_site') ?>">
        <meta property="og:description" content="<?php echo ($this->fetch('description') ? $this->fetch('description') : Configure::read('Configure.description')) ?>">
        <meta property="og:image" content="<?php echo $this->Html->url('/', true) ?>img/site/">
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:type" content="website">

        <?php echo $this->Html->css('site/bootstrap/bootstrap.min'); ?>
        <?php echo $this->Html->css('site/normalize'); ?>
        <?php echo $this->Html->css('//cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/example1/colorbox.min.css'); ?>
        <link rel="stylesheet/less" type="text/css" href="<?php echo $this->Html->url('/') ?>less/main.less">
        <?php echo $this->fetch('css') ?>
        <?php if (isset($current_article)){ ?>
			<meta name="citation_title" content="<?php echo $current_article['titulo']; ?>">
			<?php foreach ($authorsCitation as $authorName) { ?>
			<meta name="citation_author" content="<?php echo $authorName; ?>">
			<?php } ?>
			<meta name="citation_publication_date" content="<?php echo $current_article['ano']; ?>">
			<meta name="citation_journal_title" content="Revista de Estudos Criminais">
			<meta name="citation_volume" content="<?php echo $current_article['volume']; ?>">
			<meta name="citation_issue" content="<?php echo $current_article['numero']; ?>">
				<?php list($current_article['firstpage'], $current_article['lastpage']) = explode('-', $current_article['paginacao']); ?>
			<meta name="citation_firstpage" content="<?php echo $current_article['firstpage']; ?>">
			<meta name="citation_lastpage" content="<?php echo $current_article['lastpage']; ?>">
            <?php /*
              <?php $url = $this->Html->Url('/edicoes/edicoes/ano:' . $current_article['ano'] . '/titulo:' . $current_article['slug']); ?>
              <meta name="citation_pdf_url" content="<?php echo $url; ?>">
             */ ?>
		<?php } ?>

        <!--[if lt IE 9]>
            <script src="//raw.github.com/mylovecompany/ie9-js/master/ie9.min.js">IE7_PNG_SUFFIX=".png";</script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
        <![endif]-->

    </head>

    <body class="<?php echo $this->fetch('body') ?>">
        <?php echo $this->element('site/header') ?>
        <?php echo $this->Session->flash() ?>
        <?php echo $this->fetch('content') ?>

        <?php echo $this->element('site/footer') ?>

        <?php echo $this->Html->script('site/jquery.min') ?>
        <?php echo $this->Html->script('site/bootstrap.min') ?>
        <?php echo $this->Html->script('site/jquery.cycle2.min') ?>
        <?php echo $this->Html->script('site/jquery.cycle2.carousel.min') ?>
        <?php echo $this->Html->script('site/jquery.scrollTo.min') ?>
        <?php echo $this->Html->script('site/jquery.validate.min') ?>
        <?php echo $this->Html->script('site/additional-methods.min') ?>
        <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/jquery.colorbox-min.js') ?>
        <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.4.33/i18n/jquery.colorbox-pt-br.js') ?>
        <?php echo $this->Html->script('site/less.min') ?>
        <?php echo $this->Html->script('site/main') ?>
        <?php echo $this->fetch('script') ?>



        <?php // echo $this->element('sql_dump'); ?>
    </body>
</html>
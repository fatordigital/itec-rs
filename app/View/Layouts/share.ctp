<?php
/**
 * @var     $this       View
 *
 * @project
 * @package       app.View.Layouts
 * @since
 */

echo $this->Html->docType(); ?>
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $project_prefix_title ?> :: <?php echo $title_for_layout; ?></title>
    <!-- TAGS FACEBOOK -->
    <meta property="og:locale" content="pt_br">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>">
    <meta property="og:title" content="<?php echo $share['title']; ?>">
    <meta property="og:site_name" content="<?php echo $share['site_name']; ?>">

    <?php if(isset($share['description']) && $share['description'] != ""): ?>
        <meta property="og:description" content="<?php echo $share['description']; ?>">
    <?php endif; ?>

    <?php if (isset($share['image']) && $share['image'] != ""){ ?>
        <meta property="og:image" content="<?php echo $this->Html->Url("/", true).$share['image']; ?>">    
    <?php }else{ ?>
        <meta property="og:image" content="<?php echo $this->Html->Url("/img/site/scalzilli-fmv-logo-facebook.jpg",true); ?>">
    <?php } ?>
    <meta property="og:image:type" content="image/jpeg">
</head>
<?php echo $this->Html->tag('body'); ?>
    <?php 
        //header("HTTP/1.1 301 Moved Permanently"); 
        if(stripos($share['url'], 'http://') === false){
            $share['url'] = 'http://'.$share['url'];
        }
        //header("Location: " . $share['url']); 
    ?>
	<script type="text/javascript">
		var newLocation = "<?php echo $share['url']; ?>";
		window.location = newLocation;
	</script>
</body>
</html>

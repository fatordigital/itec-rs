<?php $this->start('title') ?>Histórico - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="instituto">
					<h1>O <strong>Instituto</strong></h1>
					<div class="migalhas-de-pao">
						<?php echo $this->Html->link('Home', '/') ?> - <?php echo $this->Html->link('O Insitituto', '/o-instituto') ?> - <strong><?php echo $this->Html->link('Histórico', '#') ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="historico">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="hidden-lg hidden-md hidden-sm">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<h2>Linha do Tempo</h2>

						<?php echo $this->Html->image('site/timeline.png', array('class' => 'img-responsive')) ?>
					</div>
					<div class="hidden-xs">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
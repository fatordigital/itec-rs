<?php $this->start('title') ?>Conselho Editorial- <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="instituto">
					<h1>A <strong> Revista</strong></h1>
					<div class="migalhas-de-pao">
						<?php echo $this->Html->link('Home', '/') ?> - A Revista - <strong><?php echo $this->Html->link('Conselho Editorial', '#') ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="membros">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="hidden-lg hidden-md hidden-sm">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<h2>CONSELHO EDITORIAL</h2>
						<hr>
						<p>Rodrigo Moraes de Oliveira</p>
<p>Alexandre Wunderlich (Pontifícia Universidade Católica/RS)</p>
<p>Álvaro Sanchez Bravo (Universidade de Sevilha)</p>
<p>Arndt Sinn (Universidade de Osnabrück, Alemanha)</p>
<p>Davi de Paiva Costa Tangerino (Universidade do Estado do Rio de Janeiro/RJ)</p>
<p>David Sanchez Rúbio (Universidade de Sevilha/Espanha)</p>
<p>Elizabeth Cancelli (Universidade de Brasília)</p>
<p>Fabio Roberto D’Avila (Pontifícia Universidade Católica/RS)</p>
<p>Fauzi Hassan Choukr (Universidade de São Paulo)</p>
<p>Felipe Augusto Forte de Negreiros Deodato (Universidade Federal da Paraíba/PB)</p>
<p>Fernando Machado Pelloni (Universidade de Buenos Aires/Argentina)</p>
<p>Geraldo Prado (Universidade Federal do Rio de Janeiro)</p>
<p>Giovani Agostini Saavedra (Pontifícia Universidade Católica/RS)</p>
<p>Helena Lobo da Costa (Universidade de São Paulo/SP)</p>
<p>Heloisa Estellita (Fundação Getúlio Vargas/SP)</p>
<p>Luiz Eduardo Soares (Universidade Federal do Rio de Janeiro)</p>
<p>Rui Cunha Martins (Universidade de Coimbra)</p>
						<p>Ruth Maria Chittó Gauer (Pontifícia Universidade Católica/RS)</p>
						<p>Vittorio Manes (Universidade de Salento, Itália)</p>
					</div>
					<div class="hidden-xs">
						<?php //echo $this->element('site/sidebar-instituto') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
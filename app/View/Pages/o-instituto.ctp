<?php $this->start('title') ?>O Instituto - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="instituto">
					<h1>O <strong>Instituto</strong></h1>
					<div class="migalhas-de-pao hidden-xs">
						<?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('O Insitituto', '#'); ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="o-instituto">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="hidden-lg hidden-md hidden-sm">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
							<h2><strong>O INSTITUTO</strong> TRANSDICIPLINAR<br> DE ESTUDOS CRIMINAIS</h2>
							<hr>
							<p>A partir do encontro de jovens investigadores preocupados com o rumo da Política Criminal contemporânea no Programa de Pós-graduação em Ciências Criminais da Pontifícia Universidade Católica do Rio Grande do Sul (PUCRS), surgiu a necessidade de criação de um fórum de debate permanente que transcendesse a academia.</p>
							<p>Assim, o Instituto Transdisciplinar de Estudos Criminais (!TEC) foi criado no ano de 1998, objetivando divulgar os resultados da investigações de profissionais de inúmeras áreas do conhecimento preocupados com o fenômeno da violência em suas mais variadas formas de manifestação.</p>
							<p>Hoje, em parceria com Institutos e Associações que realizam pesquisa sobre violência no Brasil – v.g. Instituto Brasileiro de Ciências Criminais, Instituto Carioca de Criminologia –, o !TEC contribui com ações concretas para a defesa dos direitos e garantias fundamentais, na crítica às inúmeras formas de (re)vivificação de modelos repressivos autoritários.</p>
							<p>O !TEC, portanto, tem sido um ‘local’ de encontro e um identificador do grupo de investigadores críticos no Rio Grande do Sul. Trata-se de um Instituto sem líderes, porque não há hierarquização; sem sede, porque trabalhamos em rede.</p>
							<p>Nossos veículos principais são a Revista de Estudos Criminais e o sítio www.itecrs.org. Esses instrumentos permitem a divulgação das idéias dos seus membros, o contato com demais pesquisadores e a agregação de outros que compartilham de idéias similares.</p>
							<div id="<?php echo Inflector::slug(strtolower('POR QUE TRANSDISCIPLINAR?'), '-') ?>"></div>
							<h2><strong>POR QUE</strong> TRANSDISCIPLINAR?</h2>
							<hr>
							<p>A redução dos estudos sobre violência e criminalidade em campos específicos e distantes de saber, operada pelos paradigmas tradicionais das ciências criminais (paradigma clássico-contratualista e paradigma etiológico), geraram um modo de produção científico insuficiente para responder as atuais demandas sociais.</p>
							<p>Sabemos que o grande giro na metodologia das ciências criminais ocorreu com a invasão de estudos alienígenas aos enfoques predominantemente médicos e jurídicos, culminando com a construção acadêmica do paradigma da reação social. Estes estudos, advindos fundamentalmente das áreas da antropologia, da sociologia e da psicanálise, desnudaram e desmitologizaram qualquer possibilidade de objetivação das pesquisas sobre violência e criminalidade em esferas isoladas.</p>
							<p>A complexidade do fenômeno violência (interindividual, social, institucional ou simbólica) impõe ao pensador uma superação da especialização, ou seja, somente construiremos um saber possível e uma qualificação idônea para responder os problemas que nos são apresentados cotidianamente se rompermos com as fronteiras disciplinares que compartimentalizam o saber. A reivindicalização da transdisciplinariedade aparece, portanto, no momento da consciência da crise dos paradigmas que produzem o conhecimento científico e da necessidade de sua superação, preenchendo a lacuna apresentada através da flexibilização, intercâmbio e articulação entre os pesquisadores e os saberes por eles produzidos.</p>
							<p>Desta forma, o Instituto Transdisciplinar de Estudos Criminais (ITEC) objetiva ser um instrumento para a construção de possibilidades teóricas que assegurem uma integração entre inúmeros campos de conhecimento. Procura, assim, o constante aperfeiçoamento de profissionais de diversas áreas (direito, sociologia, antropologia, psicologia, assistência social, medicina, história), buscando intensificar a pesquisa sobre o fenômeno da violência e da criminalidade em fértil relação de integração entre a academia e o labor cotidiano.</p>
							<div id="<?php echo Inflector::slug(strtolower('CARTA DE TRANSDISCIPLINARIDADE'), '-') ?>"></div>
							<h2 class="margin-top"><strong>CARTA DE</strong> TRANSDISCIPLINARIDADE</h2>
							<hr>
							<p class="italic">(adotada no Primeiro Congresso Mundial da Transdisciplinaridade, Convento de Arrábida, Portugal, 2-6 novembro 1994)</p>
							<p><strong>Lima de Freitas, Edgar Morin<br>e Basarab Nicolescu - Comitê de redação</strong></p>
							<p><strong>Preâmbulo</strong></p>
							<p>Considerando que a proliferação atual das disciplinas acadêmicas conduz a um crescimento exponencial do saber que torna impossível qualquer olhar global do ser humano;</p>
							<p>Considerando que somente uma inteligência que se dá conta da dimensão planetária dos conflitos atuais poderá fazer frente à complexidade de nosso mundo e ao desafio contemporâneo de autodestruição material e espiritual de nossa espécie;</p>
							<p>Considerando que a vida está fortemente ameaçada por uma tecnociência triunfante que obedece apenas à lógica assustadora da eficácia pela eficácia;</p>
							<p>Considerando que a ruptura contemporânea entre um saber cada vez mais acumulativo e um ser interior cada vez mais empobrecido leva à ascensão de um novo obscurantismo, cujas conseqüências sobre o plano individual e social são incalculáveis;</p>
							<p>Considerando que o crescimento do saber, sem precedentes na história, aumenta a desigualdade entre seus detentores e os que são desprovidos dele, engendrando assim desigualdades crescentes no seio dos povos e entre as nações do planeta;</p>
							<p>Considerando simultaneamente que todos os desafios enunciados possuem sua contrapartida de esperança e que o crescimento extraordinário do saber pode conduzir a uma mutação comparável àevolução dos humanóides à espécie humana;</p>
							<p>Considerando o que precede, os participantes do Primeiro Congresso Mundial de Transdisciplinaridade (Convento de Arrábida, Portugal 2 - 7 de novembro de 1994) adotaram o presente Protocolo entendido como um conjunto de princípios fundamentais da comunidade de espíritos transdisciplinares, constituindo um contrato moral que todo signatário deste Protocolo faz consigo mesmo, sem qualquer pressão juridica e institucional.</p>
							<p><strong>Artigo 2</strong></p>
							<p>O reconhecimento da existência de diferentes níveis de realidade, regidos por lógicas diferentes é inerente à atitude transdisciplínar. Qualquer tentativa de reduzir a realidade a um único nível regido por uma única lógica não se situa no campo da transdisciplinaridade</p>
							<p><strong>Artigo 3</strong></p>
							<p>A transdisciplinaridade é complementar à aproximação disciplinar: faz emergir da confrontação das disciplinas dados novos que as articulam entre si; oferece-nos uma nova visão da natureza e da realidade A transdisciplinaridade não procura o domínio sobre as várias outras disciplinas, mas a abertura de todas elas àquilo que as atravessa e as ultrapassa.</p>
							<p><strong>Artigo 4</strong></p>
							<p>O ponto de sustentação da transdisciplinaridade reside na unificação semântica e operativa das acepções através e além das disciplinas. Ela pressupõe uma racionalidade aberta por um novo olhar, sobre a relatividade definição edas noções de “deflnição”e “objetividade”. O formalismo excessivo, a rigidez das definições e o absolutismo da objetividade comportando a exclusão do sujeito levam ao empobrecimento.</p>
							<p><strong>Artigo 5</strong></p>
							<p>A visão transdisciplinar está resolutamente aberta na medida em que ela ultrapassa o domínio das ciências exatas por seu diálogo e sua reconciliação não somente com as ciências humanas mas também com a arte, a literatura, a poesia e a experiência espiritual.</p>
							<p><strong>Artigo 6</strong></p>
							<p>Com a relação à interdisciplinaridade e à multidisciplinaridade, a transdisciplinaridade é multidimensional Levando em conta as concepções do tempo e da história, a transdisciplinaridade não exclui a existência de um horizonte trans-histórico.</p>
							<p><strong>Artigo 7</strong></p>
							<p>A transdisciplinaridade não constitui uma nova religião, uma nova filosofia, uma nova metaflsica ou uma ciência das ciências.</p>
							<p><strong>Artigo 8</strong></p>
							<p>A dignidade do ser humano é também de ordem cósmica e planetária, O surgimento do ser humano sobre a Terra é uma das etapas da história do Universo, O reconhecimento da Terra como pátria é um dos imperativos da transdisciplinaridade Todo ser humano tem direito a uma nacionalidade, mas, a título de habitante da Terra, é ao mesmo tempo um ser transnacional, O reconhecimento pelo direito internacional de um pertencer duplo - a uma nação e à Terra - constitui uma das metas da pesquisa transdisciplinar.</p>
							<p><strong>Artigo 9</strong></p>
							<p>A transdisciplinaridade conduz a uma atitude aberta com respeito aos mitos, às religiões e àqueles que os respeitam em um espírito transdisciplinar.</p>
							<p><strong>Artigo 10</strong></p>
							<p>Não existe um lugar cultural privilegiado de onde se possam julgar as outras culturas. O movimento transdisciplinar é em si transcultural.</p>
							<p><strong>Artigo 11</strong></p>
							<p>Uma educação autêntica não pode privilegiar a abstração no conhecimento. Deve ensinar a contextualizar, concretizar e globalizar. A educação transdisciplinar reavalia o papel da intuição, da imaginação, da sensibilidade e do corpo na transmissão dos conhecimentos.</p>
							<p><strong>Artigo 12</strong></p>
							<p>A elaboração de uma economia transdisciplinar é fundada sobre o postulado de que a economia deve estar a serviço do ser humano e não o inverso.</p>
							<p><strong>Artigo 13</strong></p>
							<p>A ética transdisciplinar recusa toda atitude que recusa o diálogo e a discussão, seja qual for sua origem - de ordem ideológica, científica, religiosa, econômica, política ou filosófica. O saber compartilhado deverá conduzir a uma compreensão compartilhada baseada no respeito absoluto das diferenças entre os seres, unidos pela vida comum sobre uma única e mesma Terra.</p>
							<p><strong>Artigo 14</strong></p>
							<p>Rigor, abertura e tolerância são características fundamentais da atitude e da visão transdisciplinar. O rigor na argumentação, que leva em conta todos os dados, é a barreira às possíveis distorções. A abertura comporta a aceitação do desconhecido, do inesperado e do imprevisível. A tolerância é o reconhecimento do direito às idéias e verdades contrárias às nossas.</p>
							<p><strong>Artigo final</strong></p>
							<p>A presente Carta Transdisciplinar foi adotada pelos participantes do Primeiro Congresso Mundial de Transdisciplínaridade, que visam apenas à autoridade de seu trabalho e de sua atividade. Segundo os processos a serem definidos de acordo com os espíritos transdisciplinares de todos os países, o Protocolo permanecerá aberto à assinatura de todo ser humano interessado em medidas progressistas de ordem nacional, internacional para aplicação de seus artigos na vida.</p>
					</div>
					<div class="hidden-xs">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
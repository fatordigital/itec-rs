<?php $this->start('title') ?>Membros - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="instituto">
					<h1>O <strong>Instituto</strong></h1>
					<div class="migalhas-de-pao">
						<?php echo $this->Html->link('Home', '/') ?> - <?php echo $this->Html->link('O Insitituto', '/o-instituto') ?> - <strong><?php echo $this->Html->link('Membros', '#') ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="membros">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="hidden-lg hidden-md hidden-sm">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<h2><strong>COMPOSIÇÃO</strong> DO CONSELHO</h2>
						<hr>
						<p><strong>Presidente</strong></p>
						<p class="no-link">Rodrigo Moraes de Oliveira</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Alexandre Wunderlich</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Andrei Zenkner Schmidt</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Daniel Gerber</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Fabio Roberto D’ Ávila</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Felipe Cardoso Moreira de Oliveira</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Giovani Agostini Saavedra</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Jader da Silveira Marques</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Marcelo Machado Bertoluci (licenciado)</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Rafael Braude Canterji</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
						<p class="no-link">Paulo Vinícius Sporleder de Souza</p>
						<p><?php echo $this->Html->link('https://www.linkedin.com/', '#', array('target' => '_blank')) ?></p>
					</div>
					<div class="hidden-xs">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
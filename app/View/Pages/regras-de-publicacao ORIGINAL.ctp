<?php $this->start('title') ?>Conselho Editorial- <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="instituto">
					<h1>A <strong> Revista</strong></h1>
					<div class="migalhas-de-pao">
						<?php echo $this->Html->link('Home', '/') ?> - A Revista - <strong><?php echo $this->Html->link('Conselho Editorial', '#') ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="membros">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="hidden-lg hidden-md hidden-sm">
						<?php //echo $this->element('site/sidebar-instituto') ?>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<h2> <strong>REGRAS</strong> DE PUBLICAÇÃO</h2>
						<hr>
<p><strong>DIRETRIZES DE PUBLICAÇÃO E AVALIAÇÃO DE ARTIGOS</strong></p>
<p>Endereço eletrônico para submissão de artigos: <a href="mailto:rec.artigos@gmail.com">rec.artigos@gmail.com</a></p>
<p>&nbsp;</p>
<p><strong>A) DAS NORMAS PARA PUBLICAÇÃO</strong><br>
  <br>
  1-  O  envio  de  material  editorial  para  a  Revista  de  Estudos  Criminais 
  pressupõe a aceitação das diretrizes de publicação e avaliação de artigos. <br>
  Da  mesma  forma,  implica  a  cessão  dos  direitos  autorais  do  material 
  enviado  para  a  Revista  de  Estudos  Criminais.  Uma  vez  enviado  o 
  material, cabe à Revista decidir as características editoriais e gráficas, o 
  preço, os modos de distribuição e disponibilização bem como a data em 
  que  o  artigo  será  veiculado.  A  única  contraprestação  financeira  pela 
  cessão  dos  direitos  autorais  será  o  envio  ao  autor  de  um  exemplar  da 
  Revista  em  que  o  seu  trabalho  for  publicado.  Em  caso  de  artigo  em 
  coautoria,  cada  coautor  receberá  um  exemplar.  A  Revista  de  Estudos
  Criminais  fica  autorizada  a  proceder  modificações  e  correções  para  a 
adequação do texto às normas de publicação.</p>
<p>2- Os  textos  enviados para a Revista de Estudos Criminais deverão  ser 
  inéditos  no  Brasil,  levando  em  consideração  qualquer  forma  de 
  publicação  impressa e/ou digital, sendo vedado o seu encaminhamento 
simultâneo a outras revistas. </p>
<p>3-  O  envio  dos  artigos  deverá  ser  realizado  unicamente  por  correio 
  eletrônico. Os trabalhos deverão ser endereçados diretamente à Diretoria 
  da  Revista,  para  o  endereço  eletrônico:  <a href="mailto:rec.artigos@gmail.com">rec.artigos@gmail.com</a>. <br>
  Recomenda-se  que  os  textos  sejam  enviados  em  formato  word.doc. 
  Textos em  formatos que não permitem modificações, a exemplo de  .pdf, 
não serão aceitos.</p>
<p>4-  Os  artigos  deverão  ser  enviados  com  uma  folha  de  rosto  na  qual 
  conste  os  dados  pessoais  do  autor.  Os  dados  exigidos  são:  nome 
  completo;  qualificação  (incluindo  a  universidade,  instituto  ou  fundação 
ao qual o autor esta vinculado); endereço completo; endereço eletrônico.</p>
<p>5-  Os  trabalhos  deverão  ter,  preferencialmente,  de  15  a  35  páginas. 
  Casos  excepcionais  serão  analisados  pela  Diretoria  da  Revista.  Deverá 
  ser utilizada a  fonte  times new  roman,  tamanho 12, no  corpo do  texto. <br>
  Ainda,  deverá  ser  utilizado  espaçamento  entrelinhas  de  1,5,  com 
  margens  superior  e  inferior  2,0  cm  e  laterais  3,0  cm. A  formatação  do 
tamanho do papel deverá ser A4 e o texto deverá estar justificado.</p>
<p>6- Os textos poderão estar em língua portuguesa, espanhola, italiana ou 
inglesa. </p>
<p>7- No que pertine à qualificação do autor, deverá  ser  iniciada por  suas 
  titulações acadêmicas e atividade de magistério, informando a existência 
  de possível vínculo com algum órgão financiador. Em seguida, deverá ser 
complementada pelas atividades jurídicas práticas do autor. </p>
<p>8- Os  textos  deverão  ser  precedidos  de um  resumo  de  05  a  10  linhas. 
  Deverá constar uma versão do título e do resumo em língua portuguesa e uma em língua inglesa.</p>
<p>9-  Os  trabalhos  deverão  ser  precedidos,  ainda,  de  04  a  06  palavras-chaves,  as  quais  devem  constar  também  em  língua  inglesa,  e  de  um 
sumário numerado.</p>
<p>10- As referências bibliográficas deverão ser feitas de acordo com a NBR 
  6023/2002  (Norma  Brasileira  da  Associação  Brasileira  de  Normas 
  Técnicas – ABNT – Anexo  I). As  referências devem ser citadas em notas 
  de  rodapé ao  final de cada página, ou na  forma  (AUTOR, ANO). O  texto 
deverá apresentar uma forma de citação uniforme.</p>
<p>11- Caso  o  autor  queira  dar  destaque  ao  texto,  deverá utilizar  <em>itálico</em> e 
  não  <strong>negrito</strong> ou  <u>sublinhado</u>.  O  uso  de  aspas  deverá  ser  feito  para  a 
citação de outros autores. </p>
<p>12-  No  que  concerne  à  referência  legislativa,  não  há  necessidade  da 
  citação do diploma  legal, seja no rodapé, seja na bibliografia ao  final do 
texto. </p>
<p>13- A Diretoria  da Revista  de Estudos Criminais não  se  compromete  a 
  efetuar complementação dos requisitos de publicação não atendidos. Os 
  trabalhos  enviados  sem  o  atendimento  às  normas  de  publicação  da 
Revista não serão aceitos. </p>
<p>&nbsp;</p>
<p><strong>B) DA ANÁLISE E SELEÇÃO DOS TRABALHOS </strong><br>
  <br>
  1-  Os trabalhos serão analisados e avaliados, tanto em forma, como em 
  conteúdo, pelo Comitê Científico da Revista de Estudos Criminais. <br>
  <br>
  2-  Recebido  o  trabalho  pela  Diretoria  da  Revista,  o  autor  será 
  imediatamente  informado,  presumindo-se  a  cessão  de  seus  direitos 
autorais e a aceitação das diretrizes de publicação e avaliação de artigos.</p>
<p>3- A avaliação será realizada pelo sistema de pareceres <em>duplo blind</em>. Para 
  tanto, será suprimido do texto qualquer elemento que possa identificar o
  autor e, após, o  trabalho será enviado para dois pareceristas anônimos, 
  membros  do  Comitê  Científico  da  Revista  de  Estudos  Criminais.  Os
  pareceristas  poderão  aprovar  o  texto,  não  aprovar  ou  aprovar  com 
ressalvas. </p>
<p>4-  Os  pareceres  anônimos  ficarão  à  disposição  do  autor,  que  será 
  informado  do  resultado  da  avaliação  e  das  recomendações  para 
adequação do texto em caso de aprovação com ressalvas.</p>
<p>5- Em caso de haver dois pareceres discordantes sobre a publicação do 
trabalho, o texto será encaminhado para um terceiro parecerista. </p>
<p>6- Sendo o artigo aprovado sem ressalvas, ou realizada a adequação do 
  texto  pelo  autor  em  caso  de  aprovação  com  ressalvas,  a  Diretoria  da 
  Revista  avaliará  a  pertinência  e  a  oportunidade  para  a  publicação.  A 
  decisão final sobre a publicação do texto será da Diretoria da Revista de 
Estudos Criminais.</p>
<p>7- A par do sistema de pareceres <em>duplo blind</em>, em casos excepcionais, a 
  Diretoria  da  Revista  poderá  aceitar  trabalhos  de  autores  convidados 
  quando considerar sua contribuição científica de grande relevância para 
o tema em questão.</p>
<p>8- A Diretoria da Revista de Estudos Criminais  ficará  à disposição dos 
  autores para qualquer queixa e/ou esclarecimento sobre a publicação ou 
  não de seus trabalhos. O contato deverá ser feito, sempre, pelo endereço 
eletrônico: <a href="mailto:rec.artigos@gmail.com">rec.artigos@gmail.com</a>.</p>

					</div>
					<div class="hidden-xs">
						<?php //echo $this->element('site/sidebar-instituto') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
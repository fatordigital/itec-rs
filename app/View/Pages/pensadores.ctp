<?php $this->start('title') ?>Pensadores - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="instituto">
					<h1>O <strong>Instituto</strong></h1>
					<div class="migalhas-de-pao">
						<?php echo $this->Html->link('Home', '/') ?> - <?php echo $this->Html->link('O Insitituto', '/o-instituto') ?> - <strong><?php echo $this->Html->link('Pensadores', '#') ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pensadores">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="row">
					<div class="hidden-lg hidden-md hidden-sm">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-8">
						<h2><strong>PENSADORES</strong></h2>
						<hr>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/alessandro-baratta.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>ALESSANDRO BARATTA</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/anais-nin.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>ANAIS NIN</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/bartolome-de-las-casas.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>BARTOLOME DE LAS CASAS</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/beccaria.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>BECCARIA</h3>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/dostoievesky.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>DOSTOIEVESKY</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/feuerbach.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>FEUERBACH</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/foucault.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>FOUCAULT</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/francesco-carnelutti.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>FRANCESCO CARNELUTTI</h3>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/freud.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>FREUD</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/giuseppe-bettiol.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>GIUSEPPE BETTIOL</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/hannah-arendt.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>HANNAH ARENDT</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/lacan.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>LACAN</h3>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/lou-salome.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>LOU SALOMÉ</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/louk-hulsman.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>LOUK HULSMAN</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/marat.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>MARAT</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/nietzsche.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>NIETZSCHE</h3>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/pietro-verri.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>PIETRO VERRI</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/nils-christie.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>NILS CHRISTIE</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/roberto-lyra-filho.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>ROBERTO LYRA FILHO</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/saramago.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>SARAMAGO</h3>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/thoreau.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>THOREAU</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/tobias-barreto.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>TOBIAS BARRETO</h3>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<div class="wrapper center-block">
								<?php echo $this->Html->image('site/zaffaroni.jpg', array('class' => 'img-responsive', 'alt' => '')) ?>
								<h3>ZAFFARONI</h3>
							</div>
						</div>
					</div>
					<div class="hidden-xs">
						<?php echo $this->element('site/sidebar-instituto') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->start('title') ?><?php echo $noticia['Noticia']['titulo'] ?> - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="destaques">
					<h1>Destaques <strong>!TEC</strong></h1>
					<div class="migalhas-de-pao hidden-xs">
						<?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('Destaques', array('controller' => 'noticias', 'action' => 'index')); ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="destaques-interna">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<span><?php echo $noticia['NoticiaCategoria']['nome'] ?></span>
				<span class="data"><?php echo $this->Time->format($noticia['Noticia']['data'], '%d de %B de %Y') ?></span>
				<div class="clearfix"></div>
				<h2><?php echo $noticia['Noticia']['titulo'] ?></h2>
				<hr>
				<?php echo $noticia['Noticia']['conteudo'] ?>
			</div>
		</div>
	</div>
</section>
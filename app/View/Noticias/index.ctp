<?php $this->start('title') ?>Destaques !TEC - <?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<section class="topo">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="destaques">
					<h1>Destaques <strong>!TEC</strong></h1>
					<div class="migalhas-de-pao hidden-xs">
						<?php echo $this->Html->link('Home', '/'); ?> - <strong><?php echo $this->Html->link('Destaques', '#'); ?></strong>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="destaques">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h2>Destaques</h2>
				<hr>
				<?php foreach($noticias as $i => $noticia): ?>
				<?php $i = $i + 1; ?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="noticia">
						<span><?php echo $noticia['NoticiaCategoria']['nome'] ?></span>
						<h3><?php echo $this->Html->link($noticia['Noticia']['titulo'], '/' . $noticia['Noticia']['url_seo']) ?></h3>
						<p><?php echo $noticia['Noticia']['descricao_resumida'] ?></p>
						<hr>
						<?php echo $this->Html->link('Leia mais', '/' . $noticia['Noticia']['url_seo'], array('class' => 'link')) ?>
					</div>
				</div>
				<?php if($i > 0 && $i%3 == 0): ?>
				<div class="clearfix"></div>
				<?php endif; ?>
				<?php endforeach; ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php echo $this->element('site/paginacao') ?>
				</div>
			</div>
		</div>
	</div>
</section>
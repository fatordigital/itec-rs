<?php

// *** INCLUDES *******************************************************************
include '../../include2/configpadrao.inc.php';

// Parâmetros
$codigo	= anti_injection($_GET["c"]);
$admin	= $_GET["a"];

// Carregando Arquivo
$query = "SELECT file_codigo, file_arquivo, file_downloads, file_titulo, file_fileext FROM sysfile WHERE file_referencia='$codigo' AND file_status=1";
$rs = $con->execute($query) or die ($con->errormsg());
$cod_file 	= $rs->fields['file_codigo'];
$titulo		= $rs->fields['file_titulo'];
$extensao	= $rs->fields['file_fileext'];
$arquivo 	= $rs->fields['file_arquivo'];
$downloads	= $rs->fields['file_downloads'];

if (!$admin) {

	$downloads++;

	// Somando Download
	$update = "UPDATE sysfile SET file_downloads='$downloads' WHERE file_codigo='$cod_file'";
	$con->execute($update) or die($con->errormsg());

}

$NovoFileName = geraUrl($titulo);
if (!$NovoFileName) $NovoFileName = "arquivo".$cod_file;
$NovoFileName.='.'.$extensao;

if (file_exists($arquivo)) {
	header ("Content-Disposition: attachment; filename=".$NovoFileName."");
	header ("Content-Type: application/octet-stream");
	header ("Content-Length: ".filesize($arquivo));
	readfile($arquivo);
} else {
	echo('C&oacute;digo inv&aacute;lido ou arquivo n&atilde;o encontrado.');
}

?>
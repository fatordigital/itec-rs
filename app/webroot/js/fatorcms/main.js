$(document).ready(function(){

	if($('.escolha-idioma').length > 0) {
		$( ".escolha-idioma" ).tabs();
	}

	if($('.data').length > 0) {
		$('.data').datepicker({
			dateFormat: 'dd/mm/yy'
		});
	}

	if($('.editor').length > 0) {
		$('.editor').redactor({
			fileUpload: '/js/fatorcms/redactor/phps/files.php',
			imageUpload: '/js/fatorcms/redactor/phps/images.php',
			minHeight: '300',
			autoresize: false
		});
	}

});
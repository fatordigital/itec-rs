$(document).ready(function(){

	$('body').on('click','a[href*=#]:not([href=#])', function(e) {
        e.preventDefault();
        var $hash = this.hash;
        $.scrollTo( $($hash), 1600 );
    });

    $('#ContatoIndexForm').validate({
    	submitHandler: function(form) {
    		var $form = $(form),
    			$action = $form.attr('action'),
    			$method = $form.attr('method'),
    			$retorno = $('.retorno-contato');

			$.ajax({
				type: $method,
				url: $action,
				dataType: 'json',
				data: $form.serialize(),
				success: function(retorno) {
					if(retorno.tipo == 'success'){
						$form.get(0).reset();
					}
					retorno.msg = '<span class="'+retorno.tipo+'">'+retorno.msg+'</span>';
					$retorno
						.html(retorno.msg);

					setTimeout(function(){
						$retorno
							.empty();
					}, 6000);
				}
			});

    		return false;
    	}
    });

    $('.ver-mapa').on('click', function(e){
    	e.preventDefault();
    	$.colorbox({title:'Endereço da <strong>ITEC!</strong>',width:700,height:450,iframe:true,href:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.400134310175!2d-51.182944000000035!3d-30.025376400000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9519782b33b9e13f%3A0x555cafab81bd18cf!2sAv.+Carlos+Gomes%2C+403+-+Auxiliadora%2C+Porto+Alegre+-+RS!5e0!3m2!1spt-BR!2sbr!4v1422476365143'});
    });

	// Accordion //
	$('#accordion .accordion-toggle').click(function(){

         //Expand or collapse this panel
         $(this).next().slideToggle('fast');

         //Hide the other panels
         $(".accordion-content").not($(this).next()).slideUp('fast');

    });

	$('.accordion-int').find('.accordion-toggle-int').click(function(){
         //Expand or collapse this panel
         $(this).next().slideToggle('fast');
         //Hide the other panels
         $(".accordion-content-int").not($(this).next()).slideUp('fast');

    });

});
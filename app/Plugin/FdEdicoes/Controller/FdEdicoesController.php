<?php

/**
 * Controller Idiomas
 */
class FdEdicoesController extends FdEdicoesAppController {

    public $uses = array('FdEdicoes.Edicao', 'FdEdicoes.Revista');

    public function fatorcms_index($page = 1) {

        $this->FilterResults->addFilters(
            array(
                'filtro_nome' => array(
                    'OR' => array(
                        'titulo_pt' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'titulo_en' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'titulo_it' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'titulo_es' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'texto_pt' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'texto_en' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'texto_it' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'texto_es' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'palavra_chave_pt' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'palavra_chave_en' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'palavra_chave_it' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                        'palavra_chave_es' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%')),
                    )
                ),
            )
        );

        $this->FilterResults->addFilters(
            array('filtro_slug' => array('slug' => array('operator' => 'LIKE', 'value' => array('before' => '%','after' => '%'))),)
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Edicao.created DESC');
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());
        $rows = $this->paginate();
        $this->set(compact('rows'));
    }

    public function fatorcms_criar() {
        if ($this->request->is('post')) {
            $this->Edicao->create();
            if ($this->Edicao->saveAll($this->request->data)) {
                $this->Session->setFlash('Edição criada com sucesso.', 'fatorcms_success');
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash('Não foi possível cadastrar o registro, tente novamente.', 'fatorcms_danger');
                $this->redirect($this->referer());
            }
        }

        $idiomas = array(
            array(
                'Idioma' => array(
                    'titulo' => 'Português',
                    'bandeira' => 'Brazil.png',
                    'prefix' => 'pt'
                )
            ),
            array(
                'Idioma' => array(
                    'titulo' => 'Inglês',
                    'bandeira' => 'United-States.png',
                    'prefix' => 'en'
                )
            ),
            array(
                'Idioma' => array(
                    'titulo' => 'Italiano',
                    'bandeira' => 'Italy.png',
                    'prefix' => 'it'
                )
            ),
            array(
                'Idioma' => array(
                    'titulo' => 'Espanhol',
                    'bandeira' => 'Spain.png',
                    'prefix' => 'es'
                )
            )
        );

        $revistas = $this->Revista->find('list', array('fields' => array('Revista.id', 'Revista.nome')));
        $this->set(compact('revistas'));

        $this->set('idiomas', $idiomas);
    }

    public function fatorcms_editar($id = null) {
        try {

            if ($this->request->is('put')) {
                $this->Edicao->id = $id;
                $update = $this->Edicao->saveAll($this->request->data);
                if (!$update) {
                    $this->Session->setFlash('Não foi possível atualizar o registro, tente novamente.', 'fatorcms_danger');
                    $this->redirect($this->referer());
                }
                $this->Session->setFlash('Edição atualizada com sucesso.', 'fatorcms_success');
                $this->redirect($this->referer());
            }

            $editar = $this->Edicao->find('first', array('conditions' => array('Edicao.id' => $id)));
            if (!$editar) {
                throw new Exception('Registro não encontrado.');
            }
            $this->data = $editar;
            $idiomas = array(
                array(
                    'Idioma' => array(
                        'titulo' => 'Português',
                        'bandeira' => 'Brazil.png',
                        'prefix' => 'pt'
                    )
                ),
                array(
                    'Idioma' => array(
                        'titulo' => 'Inglês',
                        'bandeira' => 'United-States.png',
                        'prefix' => 'en'
                    )
                ),
                array(
                    'Idioma' => array(
                        'titulo' => 'Italiano',
                        'bandeira' => 'Italy.png',
                        'prefix' => 'it'
                    )
                ),
                array(
                    'Idioma' => array(
                        'titulo' => 'Espanhol',
                        'bandeira' => 'Spain.png',
                        'prefix' => 'es'
                    )
                )
            );
            
            $revistas = $this->Revista->find('list', array('fields' => array('Revista.id', 'Revista.nome')));
            $this->set(compact('revistas'));
            $this->set('idiomas', $idiomas);
            
        } catch (Exception $e) {
            $this->Session->setFlash('Registro não encontrado.', 'fatorcms_danger');
            $this->redirect(array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
    }

    public function fatorcms_remover($id = null) {
        $this->Edicao->id = $id;
        if (!$this->Edicao->id) {
            $this->Session->setFlash('Registro não encontrado.', 'fatorcms_danger');
            $this->redirect(array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Edicao->deleteAll(array('Edicao.id' => $id));
        $this->Session->setFlash('Registro removido com sucesso.', 'fatorcms_success');
        $this->redirect($this->referer());
    }

    public function fatorcms_status() {
        
    }

}

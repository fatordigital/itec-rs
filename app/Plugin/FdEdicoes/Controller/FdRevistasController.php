<?php

class FdRevistasController extends FdEdicoesAppController
{

    public $uses = array('FdEdicoes.Revista');

    public function fatorcms_index()
    {

        $rows = $this->paginate('Revista', 30);

        $this->set(compact('rows'));
    }

    public function fatorcms_criar()
    {

    }

    public function fatorcms_editar($id)
    {
        $edit = $this->Revista->findById($id);
        if (!$edit) {
            $this->Session->setFlash('Registro não encontrado', 'fatorcms_danger');
            $this->redirect('/fatorcms/revistas');
        }

        if ($this->request->is('PUT')) {
            if ($this->Revista->save($this->request->data)){
                $this->Session->setFlash('Registro atualizado com sucesso', 'fatorcms_success');
                $this->redirect('/fatorcms/revistas');
            } else {
                $this->Session->setFlash('Não foi possível atualizar o registro, tente novamente', 'fatorcms_danger');
            }
            $edit = $this->Revista->findById($id);
        }

        $this->data = $edit;
    }

    public function fatorcms_deletar()
    {

    }

}
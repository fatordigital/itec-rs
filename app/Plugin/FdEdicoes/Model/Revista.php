<?php

class Revista extends FdEdicoesAppModel
{

    public $actsAs = array(
        'Containable',
        'Upload.Upload' => array(
            'imagem' => array(
                'thumbnailSizes' => array(
                    'custom' => '309x380'
                ),
                'thumbnailMethod'	=> 'php',
            ),
            'diretrizes_pt_file',
            'diretrizes_en_file',
            'regras_pt_file',
            'regras_en_file'
        )
    );

    public $validate = array();

    public $hasMany = array(
        'Edicao' => array(
            'className' => 'Edicao',
            'foreignKey' => 'revista_id'
        )
    );

} 
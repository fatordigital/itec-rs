<?php
Router::connect("/fatorcms/edicoes", array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
Router::connect("/fatorcms/edicoes/:page", array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
Router::connect("/fatorcms/edicoes/:action/*", array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'prefix' => 'fatorcms', 'fatorcms' => true));


Router::connect("/fatorcms/revistas", array('plugin' => 'fd_edicoes', 'controller' => 'fd_revistas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
Router::connect("/fatorcms/revistas/:page", array('plugin' => 'fd_edicoes', 'controller' => 'fd_revistas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
Router::connect("/fatorcms/revistas/:action/*", array('plugin' => 'fd_edicoes', 'controller' => 'fd_revistas', 'prefix' => 'fatorcms', 'fatorcms' => true));
<?php $this->start('script') ?>
<script type="text/javascript">
    $('#RevistaImagem').on('change', function () {
        preview(this);
    });
    var preview = function (img) {
        if (img.files && img.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').fadeIn().attr('src', e.target.result);
            }

            reader.readAsDataURL(img.files[0]);
        }
    };
</script>
<?php $this->end() ?>
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="">Título <strong class="text-danger">*</strong></label>
                    <?php echo $this->Form->text('nome', array('maxlength' => 4, 'class' => 'form-control', 'required', 'placeholder' => 'Título')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('apresentacao_pt', array('class' => 'form-control editor', 'required', 'placeholder' => 'Apresentação *')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('apresentacao_en', array('class' => 'form-control editor', 'required', 'placeholder' => 'Apresentação *')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('escopo_pt', array('class' => 'form-control editor', 'placeholder' => 'Escopo')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('escopo_en', array('class' => 'form-control editor', 'placeholder' => 'Escopo')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('equipe_pt', array('class' => 'form-control editor', 'placeholder' => 'Equipe')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('equipe_en', array('class' => 'form-control editor', 'placeholder' => 'Equipe')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('diretrizes_pt', array('class' => 'form-control editor', 'placeholder' => 'Diretrizes')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('diretrizes_pt_file', array('label' => 'Arquivo PDF para diretrizes em Inglês', 'type' => 'file', 'div' => false)) ?>
                    <br>
                    <?php if (isset($this->data['Revista']['diretrizes_pt_file'])) { ?>
                        <?php if (is_file(WWW_ROOT . 'files' . DS . 'revista' . DS . 'diretrizes_pt_file' . DS . $this->data['Revista']['id'] . DS . $this->data['Revista']['diretrizes_pt_file'])) { ?>
                            <a href="
                            <?php
                            echo $this->Html->Url('/files/revista/diretrizes_pt_file/' .
                                $this->data['Revista']['id'] . '/' .
                                $this->data['Revista']['diretrizes_pt_file'], true);
                            ?>" target="_blank">Clique aqui para download</a>
                           <?php } else { ?>
                            <img id="preview" alt="pageview" height="100" />
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>            
            <div class="col-lg-12">
                <div class="form-group"><?php echo $this->Form->input('diretrizes_en', array('class' => 'form-control editor', 'placeholder' => 'Diretrizes')) ?></div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('diretrizes_en_file', array('label' => 'Arquivo PDF para diretrizes em Inglês', 'type' => 'file', 'div' => false)) ?>
                    <br>
                    <?php if (isset($this->data['Revista']['diretrizes_en_file'])) { ?>
                        <?php if (is_file(WWW_ROOT . 'files' . DS . 'revista' . DS . 'diretrizes_en_file' . DS . $this->data['Revista']['id'] . DS . $this->data['Revista']['diretrizes_en_file'])) { ?>
                            <a href="
                            <?php
                            echo $this->Html->Url('/files/revista/diretrizes_en_file/' .
                                $this->data['Revista']['id'] . '/' .
                                $this->data['Revista']['diretrizes_en_file'], true);
                            ?>" target="_blank">Clique aqui para download</a>
                           <?php } else { ?>
                            <img id="preview" alt="pageview" height="100" />
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('regras_pt', array('class' => 'form-control editor', 'placeholder' => 'Regras')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('regras_pt_file', array('label' => 'Arquivo PDF para regras em Português', 'type' => 'file', 'div' => false)) ?>
                    <br>
                    <?php if (isset($this->data['Revista']['regras_pt_file'])) { ?>
                        <?php if (is_file(WWW_ROOT . 'files' . DS . 'revista' . DS . 'regras_pt_file' . DS . $this->data['Revista']['id'] . DS . $this->data['Revista']['regras_pt_file'])) { ?>
                            <a href="
                            <?php
                            echo $this->Html->Url('/files/revista/regras_pt_file/' .
                                $this->data['Revista']['id'] . '/' .
                                $this->data['Revista']['regras_pt_file'], true);
                            ?>" target="_blank">Clique aqui para download</a>
                        <?php } else { ?>
                            <img id="preview" alt="pageview" height="100" />
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('regras_en', array('class' => 'form-control editor', 'placeholder' => 'Regras')) ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('regras_en_file', array('label' => 'Arquivo PDF para regras em Inglês', 'type' => 'file', 'div' => false)) ?>
                    <br>
                    <?php if (isset($this->data['Revista']['regras_en_file'])) { ?>
                        <?php if (is_file(WWW_ROOT . 'files' . DS . 'revista' . DS . 'regras_en_file' . DS . $this->data['Revista']['id'] . DS . $this->data['Revista']['regras_en_file'])) { ?>
                            <a href="
                            <?php
                            echo $this->Html->Url('/files/revista/regras_en_file/' .
                                $this->data['Revista']['id'] . '/' .
                                $this->data['Revista']['regras_en_file'], true);
                            ?>" target="_blank">Clique aqui para download</a>
                           <?php } else { ?>
                            <img id="preview" alt="pageview" height="100" />
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for=""> Link Aquisição (URl) </label>
                    <?php echo $this->Form->text("link_aquisicao", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('imagem', array('label' => 'Imagem para a revista (309x390)', 'type' => 'file', 'div' => false)) ?>
                    <br>
                    <?php if (isset($this->data['Revista']['imagem'])) { ?>
                        <?php if (is_file(WWW_ROOT . 'files' . DS . 'revista' . DS . 'imagem' . DS . $this->data['Revista']['id'] . DS . $this->data['Revista']['imagem'])) { ?>
                            <img id="preview" src="<?php echo $this->Html->Url('/files/revista/imagem/' . $this->data['Revista']['id'] . '/custom_' . $this->data['Revista']['imagem'], true) ?>" alt="pageview" height="100" />
                        <?php } else { ?>
                            <img id="preview" alt="pageview" height="100" />
                        <?php } ?>
                    <?php } else { ?>
                        <img id="preview" alt="pageview" height="100" />
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<hr />
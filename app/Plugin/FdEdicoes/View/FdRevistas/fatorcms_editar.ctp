<?php $this->Html->addCrumb('Revistas', array('plugin' => 'fd_edicoes', 'controller' => 'fd_revistas', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Editar Revista') ?>

<h3>Editar Edição</h3>

<div class="panel">
    <div class="panel-body">
        <?php echo $this->Form->create('Revista', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
        <?php echo $this->Form->input('id') ?>
        <?php include 'form.ctp' ?>

        <div class="col-lg-12">
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    Salvar Revista
                </button>
            </div>
        </div>
        <?php echo $this->Form->end() ?>
    </div>
</div>
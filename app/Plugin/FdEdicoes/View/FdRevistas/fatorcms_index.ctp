<?php $this->Html->addCrumb('Revistas'); ?>

<?php
$this->Paginator->options(array('url' => array(
    'plugin' => 'fd_edicoes', 'controller' => 'fd_revistas', 'action' => 'index', 'prefix' => 'fatorcms'
)
));
?>

<div class="row">
    <h3 class="col-lg-12 pull-left">
        Revistas
    </h3>
</div>


<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                    <tr>
                        <th>Título</th>
                        <th>Link de aquisição</th>
                        <th style="width:180px">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($rows)) { ?>
                        <?php foreach ($rows AS $row): ?>
                            <tr>
                                <td><?php echo $row['Revista']['nome'] ?></td>
                                <td>
                                    <a target="_blank" href="<?php echo $row['Revista']['link_aquisicao'] ?>">
                                        <?php echo $row['Revista']['link_aquisicao'] ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_edicoes', 'controller' => 'fd_revistas', 'action' => 'editar', $row['Revista']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_edicoes', 'controller' => 'fd_revistas', 'action' => 'remover', $row['Revista']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover este edição?") ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                    </tbody>
                </table>
                <p><?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>, mostrando <strong>{:current}</strong> registros no total de <strong>{:count}</strong> registros.') ?></p>
                <?php if ($this->Paginator->hasPage(2)): ?>
                    <ul class="pagination">
                        <?php echo $this->Paginator->prev('<<', array('class' => '', 'tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a')); ?>
                        <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a')) ?>
                        <?php echo $this->Paginator->next('>>', array('class' => '', 'tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a')) ?>
                    </ul>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>
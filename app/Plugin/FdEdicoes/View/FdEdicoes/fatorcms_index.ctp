<?php $this->Html->addCrumb('Edições'); ?>

<?php
$this->Paginator->options(array('url' => array(
    'plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'index', 'prefix' => 'fatorcms'
)
));
?>

<div class="row">
    <h3 class="col-lg-12 pull-left">
        Edições <?php echo $this->Html->link('Cadastrar novo', array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'criar', 'prefix' => 'fatorcms'), array('class' => 'btn btn-info pull-right margin-top-5-neg')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                <div class="form-group"><?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por título', 'class' => 'form-control')) ?></div>
                <div class="form-group"><?php echo $this->FilterForm->input('filtro_slug', array('placeholder' => 'Filtrar por url', 'class' => 'form-control')) ?></div>
                <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                <?php echo $this->Html->link('Limpar Filtro', array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'index', 'prefix' => 'fatorcms'), array('class' => 'btn btn-warning')) ?>
                <?php echo $this->FilterForm->end() ?>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>Título Original</th>
                            <th>Número</th>
                            <th>Volume</th>
                            <th>Autores</th>
                            <th>Paginação</th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if (isset($rows)) { ?>
                        <?php foreach ($rows AS $row): ?>
                            <tr>
                                <td><?php echo $row['Edicao']['ano'] ?></td>
                                <td title="<?php echo $row['Edicao']['titulo_' . $row['Edicao']['idioma_original']]; ?>">
                                    <?php echo substr($row['Edicao']['titulo_' . $row['Edicao']['idioma_original']], 0, 50) . '...' ?>
                                </td>
                                <td><?php echo $row['Edicao']['numero'] ?></td>
                                <td><?php echo $row['Edicao']['volume'] ?></td>
                                <td><?php echo $row['Edicao']['autores'] ?></td>
                                <td><?php echo $row['Edicao']['paginacao'] ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'editar', $row['Edicao']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'remover', $row['Edicao']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover este edição?") ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                    </tbody>
                </table>
                <p><?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>, mostrando <strong>{:current}</strong> registros no total de <strong>{:count}</strong> registros.') ?></p>
                <?php if ($this->Paginator->hasPage(2)): ?>
                    <ul class="pagination">
                        <?php echo $this->Paginator->prev('<<', array('class' => '', 'tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a')); ?>
                        <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a')) ?>
                        <?php echo $this->Paginator->next('>>', array('class' => '', 'tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a')) ?>
                    </ul>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>
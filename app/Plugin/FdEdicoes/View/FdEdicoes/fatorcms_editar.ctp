<?php $this->Html->addCrumb('Edições', array('plugin' => 'fd_edicoes', 'controller' => 'fd_edicoes', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Editar Edição') ?>
<h3>Editar Edição</h3>
<div class="panel">
    <div class="panel-body">
        <?php echo $this->Form->create('Edicao', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
        <?php echo $this->Form->input('id') ?>
        <?php include 'form.ctp' ?>
        <div class="col-lg-12"><div class="form-group"><button class="btn btn-success" type="submit">Salvar edição</button></div></div>
        <?php echo $this->Form->end() ?>
    </div>
</div>
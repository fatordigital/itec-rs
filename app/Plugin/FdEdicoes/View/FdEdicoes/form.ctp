<?php $this->start('script'); ?>
<script type="text/javascript">
	$('#EdicaoImagem').on('change', function () {
		preview(this);
	});

	var preview = function (img) {
		if (img.files && img.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#preview').fadeIn().attr('src', e.target.result);
			}
			reader.readAsDataURL(img.files[0]);
		}
	};
	$(document).ready(function () {
		if($('#EdicaoAutoresCitation').val() == ''){
			$('#EdicaoAutoresCitation').val( $('#EdicaoAutores').val() );
		}
		$('#EdicaoAutores').change(function () {
			console.log($(this).val());
			$('#EdicaoAutoresCitation').val( $(this).val() );
		});
	});

</script>
<?php $this->end(); ?>
<div class="panel">
    <div class="panel-body">
        <div class="row">

			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="">Revista <strong class="text-danger">*</strong></label>
						<?php echo $this->Form->select('revista_id', $revistas, array('class' => 'form-control', 'required')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-3">
					<div class="form-group">
						<label for="">Ano <strong class="text-danger">*</strong></label>
						<?php echo $this->Form->text('ano', array('maxlength' => 4, 'class' => 'form-control', 'required', 'placeholder' => 'Ano da edição')) ?>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<label for="">Número <strong class="text-danger">*</strong></label>
						<?php echo $this->Form->text('numero', array('class' => 'form-control', 'maxlength' => 11, 'required', 'placeholder' => 'Número da edição')) ?>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<label for="">Volume <strong class="text-danger">*</strong></label>
						<?php echo $this->Form->text('volume', array('class' => 'form-control', 'maxlength' => 11, 'required', 'placeholder' => 'Número do volume')) ?>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<label for="">Paginação <strong class="text-danger">*</strong></label>
						<?php echo $this->Form->text('paginacao', array('class' => 'form-control', 'maxlength' => 60, 'required', 'placeholder' => 'Paginação da edição')) ?>
					</div>
				</div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="">Autores <span style="font-size: 10px;">(separados por ponto e vírgula)</span><strong class="text-danger">*</strong></label>
						<?php echo $this->Form->text('autores', array('class' => 'form-control', 'required', 'placeholder' => 'Autores da edição')) ?>
					</div>
				</div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="">Autores <span style="font-size: 10px;">(para indexação, separados por ponto e vírgula)</span> <strong class="text-danger">*</strong></label>
						<?php echo $this->Form->text('autores_citation', array('class' => 'form-control', 'required', 'placeholder' => 'Autores da edição')) ?>
					</div>
				</div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="">Link (URl)</label>
						<?php echo $this->Form->text("slug", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="">Idioma Original</label>
						<div class="icheck">
							<?php
							echo $this->Form->input('idioma_original', array(
								'type' => 'radio',
								'options' => array('pt' => 'Português', 'en' => 'Inglês', 'it' => 'Italiano', 'es' => 'Espanhol'),
								'default' => 'pt',
								'div' => false,
								'legend' => false,
								'before' => '<div class="radio">',
								'after' => '</div>',
								'separator' => '</div><div class="clearfix"></div><div class="radio">'));
							?>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<hr />
<header class="panel-heading tab-bg-dark-navy-blue ">
    <ul class="nav nav-tabs">
		<?php foreach ($idiomas as $i => $idioma) { ?>
			<li class="<?php echo $idioma['Idioma']['prefix'] == 'pt' ? 'active' : ''; ?>">
				<a data-toggle="tab" href="#<?php echo $idioma['Idioma']['prefix']; ?>">
					<?php echo $idioma['Idioma']['titulo']; ?>[<img src="<?php echo $this->Html->Url('/img/flags/16/' . $idioma['Idioma']['bandeira'], true) ?>" alt="">]
				</a>
			</li>
		<?php } ?>
    </ul>
</header>
<div class="panel">
    <div class="panel-body">
        <div class="tab-content">
			<?php foreach ($idiomas as $i => $idioma) { ?>
				<div class="tab-pane fade in <?php echo $idioma['Idioma']['prefix'] == 'pt' ? 'active' : '' ?>" id="<?php echo $idioma['Idioma']['prefix'] ?>">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="">Título [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]</label>
								<?php echo $this->Form->text("titulo_" . $idioma['Idioma']['prefix'], array('div' => false, 'class' => 'form-control', 'data-slug' => $i)) ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="">Conteúdo[<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]</label>
								<?php echo $this->Form->textarea("texto_" . $idioma['Idioma']['prefix'], array('div' => false, 'class' => 'form-control editor')) ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="">Palavras Chaves[<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]</label>
								<?php echo $this->Form->textarea("palavra_chave_" . $idioma['Idioma']['prefix'], array('div' => false, 'class' => 'form-control editor')) ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
        </div>
    </div>
</div>

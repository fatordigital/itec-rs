<?php
	Router::connect("/fatorcms/rotas", array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
	Router::connect("/fatorcms/rotas/:page", array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/rotas/:action/*", array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'prefix' => 'fatorcms', 'fatorcms' => true));
<?php $this->Html->addCrumb('Rotas'); ?>

<?php
$this->Paginator->options(array('url' => array(
    'plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'index', 'prefix' => 'fatorcms'
    )
));
?>

<div class="row">
    <h3 class="col-lg-12 pull-left">Rotas <?php echo $this->Html->link('Cadastrar nova', array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'criar', 'prefix' => 'fatorcms'), array('class' => 'btn btn-info pull-right')); ?></h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_seo_url', array('placeholder' => 'Filtrar por url', 'class' => 'form-control')) ?>
                </div>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_controller', array('placeholder' => 'Filtrar por controller', 'class' => 'form-control')) ?>
                </div>
                <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                <?php echo $this->Html->link('Limpar Filtro', array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'index', 'prefix' => 'fatorcms'), array('class' => 'btn btn-warning')) ?>
                <?php echo $this->FilterForm->end() ?>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('seo_url', 'URL') ?></th>
                            <th><?php echo $this->Paginator->sort('model', 'Model') ?></th>
                            <th><?php echo $this->Paginator->sort('controller', 'Controller') ?></th>
                            <th><?php echo $this->Paginator->sort('row_id', 'ID') ?></th>
                            <th><?php echo $this->Paginator->sort('action', 'Action') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($rotas AS $rota): ?>
                        <tr>
                            <td><?php echo $rota['Rota']['id'] ?></td>
                            <td><?php echo $rota['Rota']['seo_url'] ?></td>
                            <td><?php echo $rota['Rota']['model'] ?></td>
                            <td><?php echo $rota['Rota']['controller'] ?></td>
                            <td><?php echo $rota['Rota']['action'] ?></td>
                            <td><?php echo $rota['Rota']['row_id'] ?></td>
                            <td><?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'editar', $rota['Rota']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'remover', $rota['Rota']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover esta rota?") ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <p><?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>, mostrando <strong>{:current}</strong> registros no total de <strong>{:count}</strong> registros.') ?></p>
                <?php if ($this->Paginator->hasPage(2)): ?>
                <ul class="pagination">
                        <?php echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ); ?>
                        <?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ) ?>
                        <?php echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ) ?>
                </ul>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>

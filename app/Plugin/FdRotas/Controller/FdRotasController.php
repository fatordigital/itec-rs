<?php

class FdRotasController extends FdRotasAppController {

	public $uses = array('FdRotas.Rota');

	/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Rota.seo_url'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Rota.controller' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_seo_url' => array(
					'Rota.seo_url'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_controller' => array(
					'Rota.controller'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				)
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Paginate
		$this->Rota->recursive = 0;

		$rotas = $this->paginate();

		if(count($rotas) > 0){
			foreach($rotas as $l => $rota){
				if($rota['Rota']['model'] == 'Produto' && $rota['Rota']['params_id'] == 'grade_id' && $rota['Rota']['params_value'] != ""){
					$rotas[$l]['Rota']['model'] = $rota['Rota']['model'] = 'Grade';
					$rotas[$l]['Rota']['row_id'] = $rota['Rota']['row_id'] = $rota['Rota']['params_value'];
				}

				$this->loadModel($rota['Rota']['model']);
				$row = $this->{$rota['Rota']['model']}->find('first', array('recursive' => -1, 'conditions' => array('id' => $rota['Rota']['row_id'])));
				if($row){
					$rotas[$l][$rota['Rota']['model']] = $row[$rota['Rota']['model']];
				}
			}
		}

		// debug($rotas);die;

		$this->set('rotas', $rotas);

	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_view($id = null) {
		if (!$this->Rota->exists($id)) {
			throw new NotFoundException(__('Registro Inválido.'), 'error_message');
		}
		$options = array('conditions' => array('Rota.' . $this->Rota->primaryKey => $id));
		$this->set('rota', $this->Rota->find('first', $options));

		// addBreadcrumb for view
		$this->_addBreadcrumb('Rotas', array('admin' => true, 'controller' => 'rotas', 'action' => 'index'));
		$this->_addBreadcrumb('Detalhe de Rota');
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_criar() {
		if ($this->request->is('post')) {
			$this->Rota->create();
			if ($this->Rota->saveAll($this->request->data)) {
				$this->resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_editar($id = null) {
		if (!$this->Rota->exists($id)) {
			throw new NotFoundException(__('Registro Inválido.'), 'error_message');
		}
		if(isset($this->request->data['RotaImagem']) && count($this->request->data['RotaImagem']) > 0){
			foreach($this->request->data['RotaImagem'] as $k => $rota_imagem){
				if($rota_imagem['file']['name'] == ""){
					unset($this->request->data['RotaImagem'][$k]);
				}
			}
			if(count($this->request->data['RotaImagem']) == 0){
				unset($this->request->data['RotaImagem']);
			}
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Rota->saveAll($this->request->data)) {
				if(isset($this->request->data[$this->request->data['Rota']['model']])){
					$this->loadModel($this->request->data['Rota']['model']);
					$this->request->data[$this->request->data['Rota']['model']]['seo_url'] = $this->request->data['Rota']['seo_url'];
					$this->{$this->request->data['Rota']['model']}->save($this->request->data[$this->request->data['Rota']['model']]);
				}

				$this->resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'), 'fatorcms_success');

				if($this->Session->read('referer')){
					$this->redirect($this->Session->read('referer'));
				}else{
					$this->redirect(array('action' => 'index'));
				}
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Rota.' . $this->Rota->primaryKey => $id));
			$this->request->data = $this->Rota->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		if($this->request->data['Rota']['model'] == 'Produto' && $this->request->data['Rota']['params_id'] == 'grade_id' && $this->request->data['Rota']['params_value'] != ""){
			$this->request->data['Rota']['model'] = 'Grade';
			$this->request->data['Rota']['row_id'] = $this->request->data['Rota']['params_value'];
		}

		$this->loadModel($this->request->data['Rota']['model']);
		$row = $this->{$this->request->data['Rota']['model']}->find('first', array('recursive' => -1, 'conditions' => array('id' => (int) $this->request->data['Rota']['row_id'])));
		$this->request->data[$this->request->data['Rota']['model']] = $row[$this->request->data['Rota']['model']];

		// set rota
		$options = array('conditions' => array('Rota.' . $this->Rota->primaryKey => $id));
		$rota = $this->Rota->find('first', $options);
		$this->set(compact('rota'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_remover($id = null) {
		$this->Rota->id = $id;
		if (!$this->Rota->exists()) {
			throw new NotFoundException(__('Registro Inválido.'), 'error_message');
		}
		$this->request->is('get');
		if ($this->Rota->delete()) {
			$this->resetCaches();
			$this->Session->setFlash(__('Registro deletado.'), 'fatorcms_success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		$this->redirect(array('action' => 'index'));
	}

	public function fatorcms_update_rotas(){
		$this->loadModel('Noticia');
		$noticias = $this->Noticia->find('all');
		if(count($noticias) > 0){
			foreach ($noticias as $key => $noticia) {
				$rota = $this->Rota->find('first', array('conditions' => array('Rota.row_id' => $noticia['Noticia']['id'], 'Rota.model' => 'Noticia')));
				if(!empty($rota)){
					$rot['id'] = $rota['Rota']['id'];
				}else{
					$rot['id'] = null;
				}
				$rot['seo_url'] 	= $rota['Rota']['seo_url'];
				$rot['model'] 		= $rota['Rota']['model'];
				$rot['controller'] 	= $rota['Rota']['controller'];
				$rot['action'] 		= $rota['Rota']['action'];
				$rot['row_id'] 		= $rota['Rota']['row_id'];
				$this->Rota->save($rot);
			}
		}
		die('a');
	}

	private function resetCaches(){
		Cache::write('rotas', false);
		Cache::write('vitrine_destaque', false);
		Cache::write('vitrine_normal', false);
	}

}
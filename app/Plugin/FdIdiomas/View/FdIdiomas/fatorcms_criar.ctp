<?php $this->Html->addCrumb('Idiomas', array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Criar Idioma') ?>

<h3>Criar Idioma</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Idioma', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('id') ?>
	<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Idioma Ativo</label>
				<div class="icheck">
					<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('nome', array('label' => 'Nome do idioma', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('slug', array('label' => 'Sigla do idioma', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('bandeira', array('label' => 'Nome do idioma', 'type' => 'file', 'div' => false)) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Idioma Padrão</label>
				<div class="icheck">
					<?php echo $this->Form->input('padrao', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<?php echo $this->Form->submit('Cadastrar idioma', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
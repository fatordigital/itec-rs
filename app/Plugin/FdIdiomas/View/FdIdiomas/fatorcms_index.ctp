<?php $this->Html->addCrumb('Idiomas'); ?>

<?php
$this->Paginator->options(array('url' => array(
    'plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms'
    )
));
?>

<div class="row">
    <h3 class="col-lg-12 pull-left">Idiomas <?php echo $this->Html->link('Cadastrar novo', array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'criar', 'prefix' => 'fatorcms'), array('class' => 'btn btn-info pull-right margin-top-5-neg')); ?></h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por chave', 'class' => 'form-control')) ?>
                </div>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_slug', array('placeholder' => 'Filtrar por valor', 'class' => 'form-control')) ?>
                </div>
                <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                <?php echo $this->Html->link('Limpar Filtro', array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms'), array('class' => 'btn btn-warning')) ?>
                <?php echo $this->FilterForm->end() ?>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('nome', 'Idioma') ?></th>
                            <th style="width:80px"><?php echo $this->Paginator->sort('slug', 'Sigla') ?></th>
                            <th style="width:90px">Bandeira</th>
                            <!-- <th style="width:80px">Padrão</th> -->
                            <th style="width:80px"><?php echo $this->Paginator->sort('status', 'Ativo') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($idiomas AS $idioma): ?>
                        <tr>
                            <td><?php echo $idioma['Idioma']['id'] ?></td>
                            <td><?php echo $idioma['Idioma']['nome'] ?></td>
                            <td><?php echo $idioma['Idioma']['slug'] ?></td>
                            <td><?php echo $this->Html->image('../files/idioma/bandeira/' . $idioma['Idioma']['id'] . '/thumb_' . $idioma['Idioma']['bandeira'], array('class' => 'img-thumbnail')) ?></td>
                            <!-- <td><?php echo $idioma['Idioma']['padrao'] == 1 ? '<span class="label label-success label-mini">Sim</span>' : '<span class="label label-danger label-mini">Não</span>' ?></td> -->
                            <td><input type="checkbox" class="atualiza-status" value="<?php echo $idioma['Idioma']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $idioma['Idioma']['id'] ?>" data-url="<?php echo $this->Html->url(array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'status', 'prefix' => 'fatorcms')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $idioma['Idioma']['status'] == 1 ? ' checked="checked"' : '' ?>></td>
                            <td><?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'editar', $idioma['Idioma']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'remover', $idioma['Idioma']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover este idioma?") ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <p><?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>, mostrando <strong>{:current}</strong> registros no total de <strong>{:count}</strong> registros.') ?></p>
                <?php if ($this->Paginator->hasPage(2)): ?>
                <ul class="pagination">
                        <?php echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ); ?>
                        <?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ) ?>
                        <?php echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ) ?>
                </ul>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>
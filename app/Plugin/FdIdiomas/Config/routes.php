<?php
	Router::connect("/fatorcms/idiomas", array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
	Router::connect("/fatorcms/idiomas/:page", array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/idiomas/:action/*", array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'prefix' => 'fatorcms', 'fatorcms' => true));
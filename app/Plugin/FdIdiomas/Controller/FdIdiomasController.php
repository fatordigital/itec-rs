<?php

/**
 * Controller Idiomas
 */
class FdIdiomasController extends FdIdiomasAppController {

	/**
	 * Informamos que estamos usando a tabela Idioma do plugin FdIdiomas
	 * @var array
	 */
	public $uses = array('FdIdiomas.Idioma');

	/**
	 * Lista todos os idiomas
	 * @return [type]
	 */
	public function fatorcms_index($page = 1)
	{

        $this->FilterResults->addFilters(
            array(
                'filtro_nome' => array(
                    'Idioma.nome' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_slug' => array(
                    'Idioma.slug' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        )
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Idioma.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		$idiomas = $this->paginate();
		$this->set(compact('idiomas'));
	}

	/**
	 * Cria um novo idioma
	 * @return [type]
	 */
	public function fatorcms_criar()
	{
        if($this->request->is('post'))
        {
            $this->Idioma->create();
            if($this->Idioma->save($this->request->data))
            {
                $this->Session->setFlash('Idioma cadastrada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar o novo idioma, por favor, tente novamente', 'fatorcms_danger');
            }
        }
	}

	/**
	 * Edita um idioma
	 * @param  [type] $id
	 * @return [type]
	 */
	public function fatorcms_editar($id = null)
    {
        $this->Idioma->id = $id;
        if (!$this->Idioma->exists())
        {
            throw new NotFoundException('Idioma inválido');
        }
        if ($this->request->is('post') || $this->request->is('put'))
        {
            if ($this->Idioma->save($this->request->data))
            {
                $this->Session->setFlash('Idioma alterado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível editar o edioma, por favor, tente mais tarde', 'fatorcms_danger');
            }
        } else {
            $this->request->data = $this->Idioma->read(null, $id);
        }
    }

    /**
     * Remove um idioma
     * @param  [type] $id
     * @return [type]
     */
    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->Idioma->id = $id;
        if (!$this->Idioma->exists())
        {
            throw new NotFoundException('Idioma inválido');
        }
        if ($this->Idioma->delete())
        {
            $this->Session->setFlash('Idioma removido com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('O idioma não pode ser removido', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_idiomas', 'controller' => 'fd_idiomas', 'action' => 'index', 'prefixo' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Idioma inválido');
        }
        echo $this->saveStatus('Idioma', $this->request->data['id'], $this->request->data['value']);
        die;
    }

}
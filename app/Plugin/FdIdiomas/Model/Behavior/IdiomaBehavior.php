<?php

App::uses('ModelBehavior', 'Model');

class IdiomaBehavior extends ModelBehavior
{

    private $CurrentModel;
    private $DataSource;
    private $TableSingularize;
    private $Prefixo = '_';

    private $Idioma;
    private $Traducao;
    private $TraducaoPadrao;

    public function setup(Model $model, $settings = array())
    {
        $this->DataSource = ConnectionManager::getDataSource('default');
        $this->CurrentModel = $model;
        $this->TableSingularize = Inflector::singularize($model->table);

        try {

            if ($this->tabelaExiste('idiomas')) {

                $this->Idioma = ClassRegistry::init('Idioma');

                $processo = $this->tabelaIdiomaAtributos($settings);
                if ($processo !== true) {
                    throw new Exception($processo->getMessage());
                }
                $traducao_padrao = $this->traducaoPadrao();
                if ($traducao_padrao !== true) {
                    throw new Exception($processo->getMessage());
                }

            } else {
                throw new Exception('Tabela `idiomas` não foi encontrada.');
            }

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    private function tabelaExiste($key)
    {
        return in_array($key, $this->DataSource->listSources());
    }

    private function tabelaIdiomaAtributos($settings = array())
    {
        try {
            $tabela_traducao = "{$this->TableSingularize}_atributos";
            if ($this->tabelaExiste($tabela_traducao)) {
                $this->Traducao = ClassRegistry::init(Inflector::classify($tabela_traducao));
            } else {
                if (isset($settings['alias']) && !empty($settings['alias'])) {
                    if (strstr($settings['alias'], $this->Prefixo)) {
                        $tabela_traducao = $this->TableSingularize . $settings['alias'];
                    } else {
                        $tabela_traducao = $this->TableSingularize . $this->Prefixo . $settings['alias'];
                    }
                    if ($this->tabelaExiste($tabela_traducao)) {
                        $this->Traducao = ClassRegistry::init(Inflector::classify($tabela_traducao));
                    } else {
                        throw new Exception("A tabela `{$tabela_traducao}` não foi encontrada, crie a tabela `{$tabela_traducao}` para utilizar este Behavior.");
                    }
                } else {
                    throw new Exception("Não foi possível encontrar a tabela `atributos`, auxiliar para a tradução da model `{$this->CurrentModel->table}`");
                }
            }
            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    private function traducaoPadrao()
    {
        try {
            $idioma = Router::getParam('idioma');
            if (is_null($idioma)) {
                $this->TraducaoPadrao = $this->Idioma->find('first', array('fields' => array('Idioma.id'), 'conditions' => array('Idioma.id' => 1, 'Idioma.status' => 1)));
                if (!$this->TraducaoPadrao) {
                    $this->CurrentModel->hasOne = array(
                        'TraducaoPadrao' => array(
                            'className' => $this->Traducao->alias,
                            'foreignKey' => 'traducao_id'
                        )
                    );
                } else {
                    $this->CurrentModel->hasOne = array(
                        'TraducaoPadrao' => array(
                            'className' => $this->Traducao->alias,
                            'foreignKey' => 'traducao_id',
                            'conditions' => array(
                                'TraducaoPadrao.idioma_id' => $this->TraducaoPadrao['Idioma']['id']
                            )
                        )
                    );
                }
            } else {
                $idioma = $this->Idioma->find('first', array('conditions' => array('Idioma.slug' => $idioma)));
                if (!$idioma) {
                    $this->CurrentModel->hasOne = array(
                        'TraducaoPadrao' => array(
                            'className' => $this->Traducao->alias,
                            'foreignKey' => 'traducao_id'
                        )
                    );
                } else {
                    $this->CurrentModel->hasOne = array(
                        'TraducaoPadrao' => array(
                            'className' => $this->Traducao->alias,
                            'foreignKey' => 'traducao_id',
                            'conditions' => array(
                                'TraducaoPadrao.idioma_id' => $idioma['Idioma']['id']
                            )
                        )
                    );
                }
            }

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

}
<?php

/**
 * Model Idioma
 */
class Idioma extends FdIdiomasAppModel {

	/**
	 * Salva a bandeira no banco de dados
	 * @var array
	 */
	public $actsAs = array(
		'Upload.Upload' => array(
			'bandeira' => array(
				'thumbnailSizes' => array(
                    'xvga' => '1024x768',
                    'vga' => '640x480',
                    'thumb' => '50x50'
            	),
				'thumbnailMethod'	=> 'php',
			)
		)
	);

	/**
	 * Validação do formulário
	 * @var array
	 */
	public $validate = array(
		'nome' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe o nome do idioma',
				'required' => true,
			),
		),
		'slug' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe a sigla do idioma',
				'required' => true,
			),
		)
	);


} 
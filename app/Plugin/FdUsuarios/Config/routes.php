<?php
    Router::connect("/fatorcms/usuarios", array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/usuarios/:page", array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/usuarios/:action/*", array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/grupos-acesso", array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/grupos-acesso/:page", array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/grupos-acesso/:action/*", array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'prefix' => 'fatorcms', 'fatorcms' => true));

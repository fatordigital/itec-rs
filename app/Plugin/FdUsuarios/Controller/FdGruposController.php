<?php

class FdGruposController extends FdUsuariosAppController {

    public $uses = array('FdUsuarios.Grupo');

    public function fatorcms_index($page = 1)
    {

        $this->FilterResults->addFilters(
            array(
                'filtro_nome' => array(
                    'Grupo.nome' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Grupo.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $grupos = $this->paginate();
        $this->set(compact('grupos'));
    }

    public function fatorcms_criar()
    {
        if($this->request->is('post'))
        {
            $this->Grupo->create();
            if ($this->Grupo->save($this->request->data))
            {
                $this->Session->setFlash('Grupo criado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar o grupo de usuário, por favor, tente novamente', 'fatorcms_danger');
            }
        }
    }

    public function fatorcms_editar($id = null)
    {
        $this->Grupo->id = $id;
        if (!$this->Grupo->exists())
        {
            throw new NotFoundException('Grupo inválido');
        }
        if ($this->request->is('post') || $this->request->is('put'))
        {
            if ($this->Grupo->save($this->request->data))
            {
                $this->Session->setFlash('Grupo alterado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível editar o usuário, por favor, tente mais tarde', 'fatorcms_danger');
            }
        } else {
            $this->request->data = $this->Grupo->read(null, $id);
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->Grupo->id = $id;
        if (!$this->Grupo->exists())
        {
            throw new NotFoundException('Grupo inválido');
        }
        if ($this->Grupo->delete())
        {
            $this->Session->setFlash('Grupo removido com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('O grupo não pode ser removido', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Grupo inválido');
        }
        echo $this->saveStatus('Grupo', $this->request->data['id'], $this->request->data['value']);
        die;
    }

}
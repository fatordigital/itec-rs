<?php

class FdUsuariosController extends FdUsuariosAppController {

    public $uses = array('FdUsuarios.Usuario', 'FdUsuarios.Grupo');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('sair'/*, 'fatorcms_index', 'fatorcms_editar'*/);
    }

    public function login()
    {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Usuario->id = $this->Auth->user('id');
                $this->Usuario->saveField('ultimo_acesso', date('Y-m-d H:i:s'));
                return $this->redirect($this->Auth->redirect());
            } else {
                $this->Session->setFlash('Usuário ou senha estão incorretos', 'fatorcms_danger', array(), 'auth');
            }
        }
    }

    public function sair()
    {

        return $this->redirect($this->Auth->logout());
    }

    public function fatorcms_index($page = 1)
    {

        $this->FilterResults->addFilters(
            array(
                'filtro_nome' => array(
                    'Usuario.nome' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_email' => array(
                    'Usuario.email' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_grupo' => array(
                    'Grupo.nome' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Usuario.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $usuarios = $this->paginate();
        $this->set(compact('usuarios'));
    }

    public function fatorcms_criar()
    {
        if ($this->request->is('post')) {
            $this->Usuario->create();
            if ($this->Usuario->save($this->request->data)) {
                $id = $this->Usuario->getInsertID();
                if ($this->request->data['Usuario']['enviar']) {
                    $this->sendPassword($id);
                }
                $this->Session->setFlash('Usuário criado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar o usuário, por favor, tente novamente', 'fatorcms_danger');
            }
        }
        $grupos = array('' => 'Selecione');
        if($this->Session->read('Auth.User.grupo_id') == 9) {
            $grupos += $this->Grupo->find('list', array('fields' => array('Grupo.id', 'Grupo.nome')));
        } else {
            $grupos += $this->Grupo->find('list', array('conditions' => array('Grupo.id !=' => 9), 'fields' => array('Grupo.id', 'Grupo.nome')));
        }
        
        $this->set(compact('grupos'));
    }

    public function fatorcms_editar($id = null)
    {
        $this->Usuario->id = $id;
        if (!$this->Usuario->exists()) {
            throw new NotFoundException('Usuário inválido');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Usuario->save($this->request->data)) {
                if ($this->request->data['Usuario']['enviar']) {
                    $this->sendPassword($id);
                }
                $this->Session->setFlash('Usuário alterado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível editar o usuário, por favor, tente mais tarde', 'fatorcms_danger');
            }
        } else {
            $this->request->data = $this->Usuario->read(null, $id);
            unset($this->request->data['Usuario']['senha']);
            $grupos = array('' => 'Selecione...');
            if ($this->Session->read('Auth.User.grupo_id') == 9) {
                $grupos += $this->Grupo->find('list', array('fields' => array('Grupo.id', 'Grupo.nome')));
            } else {
                $grupos += $this->Grupo->find('list', array('conditions' => array('Grupo.id !=' => 9), 'fields' => array('Grupo.id', 'Grupo.nome')));
            }
            $this->set(compact('grupos'));
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $this->Usuario->id = $id;
        if (!$this->Usuario->exists()) {
            throw new NotFoundException('Usuário inválido');
        }
        if ($this->Usuario->delete()) {
            $this->Session->setFlash('Usuário removido com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('O usuário não pode ser removido', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Usuário inválido');
        }
        echo $this->saveStatus('Usuario', $this->request->data['id'], $this->request->data['value']);
        die;
    }

    public function generatePAssword($length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    public function sendPassword($id)
    {
        App::uses('CakeEmail', 'Network/Email');
        App::uses('HtmlHelper', 'View/Helper');

        $this->Email = new CakeEmail('smtp');
        $this->Html = new HtmlHelper(new View());

        $senha = $this->generatePAssword();


        $mensagem = 'Olá ' . $this->request->data['Usuario']['nome'] . ',' . "\r\n\r\n";
        $mensagem .= 'Estamos enviando para o seu e-mail, a senha da Fator CMS.' . "\r\n";
        $mensagem .= 'A senha é: ' . $senha . "\r\n\r\n";
        $mensagem .= 'Para acessar a Fator CMS, acesse:' . $this->Html->url('/fatorcms', true) . "\r\n";
        $mensagem .= 'Qualquer dúvida, não deixe de entrar em contato conosco pelo email, fator@fatordigital.com.br' . "\r\n\r\n";
        $mensagem .= 'Atenciosamente,' . "\r\n";
        $mensagem .= 'Fator Digital';

        $this->Email->from(array('felipe.schmitz@fatordigital.com.br' => 'Fator CMS'));
        $this->Email->to($this->request->data['Usuario']['email']);
        $this->Email->replyTo('fator@fatordigital.com.br');
        $this->Email->subject('Evnvio de senha');
        $this->Email->send($mensagem);

        //$this->request->data['Usuario']['senha'] = $senha;
        $this->Usuario->id = $id;
        $this->Usuario->saveField('senha', $senha);
    }

}
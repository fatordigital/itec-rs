<?php

class Grupo extends FdUsuariosAppModel {

    public $hasMany = 'Usuario';

    public $actsAs = array('Acl' => array('type' => 'requester'));

    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'O nome do grupo é obrigatório'
            ),
        )
    );

    public function parentNode()
    {
        return null;
    }

}
<?php
App::uses('AuthComponent', 'Controller/Component');

class Usuario extends FdUsuariosAppModel {

    public $belongsTo = 'Grupo';

    public $actsAs = array('Acl' => array('type' => 'requester'));

    public $validate = array(
        
        'nome' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o nome de exibição',
                'required' => true,
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'O e-mail obrigatório',
                'required' => true,
            ),
            'email' => array(
                'rule' => array('email', true),
                'message' => 'Informe um e-mail válido',
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Este e-mail já consta em nosso banco de dados',
                'on' => 'create',
            ),
        ),
        'senha' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A senha é obrigatoria',
                'required' => true,
            ),
            'between' => array(
                'rule' => array('between', 6, 8),
                'menssage' => 'A senha deve ter entre 6 à 8 caracteres',
            ),
        ),
        'confirma' => array(
            'rule' => array('confirmPassword'),
            'message' => 'As senhas não conferem.',
        ),
        'grupo_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'O grupo de usuário é obrigatório'
            )
        )
    );

    public function  parentNode()
    {
        if (!$this->id && empty($this->data))
        {
            return null;
        }

        if (isset($this->data['Usuario']['grupo_id']))
        {
            $grupoId = $this->data['Usuario']['grupo_id'];
        } else {
            $grupoId = $this->field('grupo_id');
        }

        if ($grupoId)
        {
            return null;
        } else {
            return array('Grupo' => array('id' => 'grupo_id'));
        }
    }

    public function bindNode()
    {
        $data = AuthComponent::user();
        //var_dump($data);die;
        return array('model' => 'Grupo', 'foreign_key' => $data['Grupo']['id']);
    }

    public function confirmPassword($password = null)
    {
        if ((isset($this->data[$this->alias]['senha']) && isset($password['confirma']))
            && !empty($password['confirma'])
            && ($this->data[$this->alias]['senha'] === $password['confirma'])
        ) {
            return true;
        }
        return false;
    }

    public function beforeSave($options = array())
    {
        parent::beforeSave();
        if (isset($this->data[$this->alias]['senha']))
        {
            $this->data[$this->alias]['senha'] = AuthComponent::password($this->data[$this->alias]['senha']);
        } else {
            unset($this->data[$this->alias]['senha']);
        }
        return true;
    }

}
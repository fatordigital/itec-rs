<?php $this->Html->addCrumb('Usuários', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Editar Usuário') ?>

<h3>Editar Usuário</h3>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('Usuario', array('role' => 'form', 'class' => 'minimal')) ?>
			<?php echo $this->Form->input('id') ?>
			<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label>Usuário Ativo</label>
						<div class="icheck">
							<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('nome', array('label' => 'Nome de exibição', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('telefone', array('label' => 'Telefone do usuário', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('email', array('label' => 'E-mail para acesso', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<div class="icheck">
							<div class="checkbox single-row">
								<?php echo $this->Form->input('enviar', array('label' => 'Gerar senha automática e enviar por e-mail', 'type' => 'checkbox', 'div' => false)) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('senha', array('label' => 'Senha de acesso', 'type'=>'password', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('confirma', array('label' => 'Confirmar senha de acesso', 'type' => 'password', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('grupo_id', array('options' => $grupos, 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<?php echo $this->Form->submit('Salvar usuário', array('class' => 'btn btn-success', 'div' => false)) ?>
		<?php echo $this->Form->end() ?>
	</div>
</div>
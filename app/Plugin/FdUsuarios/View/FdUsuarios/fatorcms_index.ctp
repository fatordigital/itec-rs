<?php $this->Html->addCrumb('Usuários') ?>
<?php
$this->Paginator->options(array('url' => array(
    'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms'
    )
));
?>
<div class="row">
    <h3 class="col-lg-12 pull-left">Usuários <?php echo $this->Html->link('Cadastrar novo', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'criar', 'prefix' => 'fatorcms'), array('class' => 'btn btn-info pull-right')); ?></h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                </div>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_email', array('placeholder' => 'Filtrar por e-mail', 'class' => 'form-control')) ?>
                </div>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_grupo', array('placeholder' => 'Filtrar por grupo', 'class' => 'form-control')) ?>
                </div>
                <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                <?php echo $this->Html->link('Limpar Filtro', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'index', 'prefix' => 'fatorcms'), array('class' => 'btn btn-warning')) ?>
                <?php echo $this->FilterForm->end() ?>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('nome', 'Nome') ?></th>
                            <th><?php echo $this->Paginator->sort('email', 'E-mail') ?></th>
                            <th><?php echo $this->Paginator->sort('grupo_id', 'Grupo') ?></th>
                            <th style="width:80px"><?php echo $this->Paginator->sort('status', 'Ativo') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($usuarios AS $usuario): ?>
                        <?php if($this->Session->read('Auth.User.grupo_id') == 9): ?>
                        <tr>
                            <td><?php echo $usuario['Usuario']['id'] ?></td>
                            <td><?php echo $usuario['Usuario']['nome'] ?></td>
                            <td><?php echo $usuario['Usuario']['email'] ?></td>
                            <td><?php echo $usuario['Grupo']['nome'] ?></td>
                            <td><input type="checkbox" class="atualiza-status" value="<?php echo $usuario['Usuario']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $usuario['Usuario']['id'] ?>" data-url="<?php echo $this->Html->url(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'status', 'prefix' => 'fatorcms')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $usuario['Usuario']['status'] == 1 ? ' checked="checked"' : '' ?>></td>
                            <td><?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'editar', $usuario['Usuario']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'remover', $usuario['Usuario']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover este usuário?") ?>
                            </td>
                        </tr>
                        <?php elseif($usuario['Usuario']['grupo_id'] == $this->Session->read('Auth.User.grupo_id')): ?>
                        <tr>
                            <td><?php echo $usuario['Usuario']['id'] ?></td>
                            <td><?php echo $usuario['Usuario']['nome'] ?></td>
                            <td><?php echo $usuario['Usuario']['email'] ?></td>
                            <td><?php echo $usuario['Grupo']['nome'] ?></td>
                            <td><input type="checkbox" class="atualiza-status" value="<?php echo $usuario['Usuario']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $usuario['Usuario']['id'] ?>" data-url="<?php echo $this->Html->url(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'status', 'prefix' => 'fatorcms')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $usuario['Usuario']['status'] == 1 ? ' checked="checked"' : '' ?>></td>
                            <td><?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'editar', $usuario['Usuario']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'remover', $usuario['Usuario']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover este usuário?") ?>
                            </td>
                        </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php if ($this->Paginator->hasPage(2)): ?>
                <ul class="pagination">
                    <?php echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ); ?>
                    <?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ) ?>
                    <?php echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ) ?>
                </ul>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>
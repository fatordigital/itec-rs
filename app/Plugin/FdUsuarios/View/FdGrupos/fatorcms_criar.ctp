<?php $this->Html->addCrumb('Grupos de Usuários', array('plugin' => 'fd_usuarios', 'controller' => 'fd_grupos', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Cadastrar Grupo de Usuário') ?>

<h3>Cadastrar Grupo de Usuário</h3>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('Grupo', array('role' => 'form', 'class' => 'minimal')) ?>
		<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Grupo Ativo</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('label' => 'Nome do grupo de usuários', 'div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Cadastrar grupo', array('class' => 'btn btn-success', 'div' => false)) ?>
		<?php echo $this->Form->end() ?>
	</div>
</div>
<?php

class FdLivrosController extends FdLivrosAppController {

	public $uses = array('FdLivros.Livro');

	public function fatorcms_index($page = 1)
	{

        $this->FilterResults->addFilters(
            array(
                'filtro_titulo' => array(
                    'Livro.titulo' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_descricao' => array(
                    'Livro.descricao' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        )
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Livro.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		$livros = $this->paginate();
		$this->set(compact('livros'));
	}

	public function fatorcms_criar()
	{
        if($this->request->is('post'))
        {
            $this->Livro->create();
            if($this->Livro->save($this->request->data))
            {
                $this->Session->setFlash('Livro cadastrada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar o novo livro, por favor, tente novamente', 'fatorcms_danger');
            }
        }
	}

	public function fatorcms_editar($id = null)
    {
        $this->Livro->id = $id;
        if (!$this->Livro->exists())
        {
            throw new NotFoundException('Livro inválido');
        }
        if ($this->request->is('post') || $this->request->is('put'))
        {
            if ($this->Livro->save($this->request->data))
            {
                $this->Session->setFlash('Livro alterado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível editar o livro, por favor, tente mais tarde', 'fatorcms_danger');
            }
        } else {
            $this->request->data = $this->Livro->read(null, $id);
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->Livro->id = $id;
        if (!$this->Livro->exists())
        {
            throw new NotFoundException('Livro inválido');
        }
        if ($this->Livro->delete())
        {
            $this->Session->setFlash('Livro removido com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('O livro não pode ser removido', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefixo' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Livro inválido');
        }
        echo $this->saveStatus('Livro', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}
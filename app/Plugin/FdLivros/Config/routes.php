<?php
	Router::connect("/fatorcms/livros", array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
	Router::connect("/fatorcms/livros/:page", array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/livros/:action/*", array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'prefix' => 'fatorcms', 'fatorcms' => true));
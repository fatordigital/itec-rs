$(document).ready(function(){

	$('#LivroTitulo').stringToSlug({
		getPut: '#LivroUrlSeo'
	});

	$('#LivroCapa').on('change', function(){
		preview(this);
	});

	if($('#LivroFatorcmsEditarForm').length){
		$('#preview').show();
	}else{
		$('#preview').hide();
	}

	var preview = function(img) {
		if(img.files && img.files[0]){
			var reader = new FileReader();

			reader.onload = function(e){
				$('#preview').fadeIn().attr('src', e.target.result);
			}

			reader.readAsDataURL(img.files[0]);
		}
	};

	$('#LivroDescricaoResumida').limiter(255,$('.max-count'));

});
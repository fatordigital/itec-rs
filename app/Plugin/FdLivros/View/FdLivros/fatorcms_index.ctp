<?php $this->Html->addCrumb('Livros'); ?>

<?php
$this->Paginator->options(array('url' => array(
    'plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms'
    )
));
?>

<div class="row">
    <h3 class="col-lg-12 pull-left">Livros <?php echo $this->Html->link('Cadastrar novo', array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'criar', 'prefix' => 'fatorcms'), array('class' => 'btn btn-info pull-right margin-top-5-neg')); ?></h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_titulo', array('placeholder' => 'Filtrar por título', 'class' => 'form-control')) ?>
                </div>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_descricao', array('placeholder' => 'Filtrar por descrição', 'class' => 'form-control')) ?>
                </div>
                <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                <?php echo $this->Html->link('Limpar Filtro', array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms'), array('class' => 'btn btn-warning')) ?>
                <?php echo $this->FilterForm->end() ?>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('titulo', 'Título') ?></th>
                            <th style="width:80px"><?php echo $this->Paginator->sort('status', 'Ativo') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($livros AS $livro): ?>
                        <tr>
                            <td><?php echo $livro['Livro']['id'] ?></td>
                            <td><?php echo $livro['Livro']['titulo'] ?></td>
                            <td><input type="checkbox" class="atualiza-status" value="<?php echo $livro['Livro']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $livro['Livro']['id'] ?>" data-url="<?php echo $this->Html->url(array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'status', 'prefix' => 'fatorcms')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $livro['Livro']['status'] == 1 ? ' checked="checked"' : '' ?>></td>
                            <td><?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'editar', $livro['Livro']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'remover', $livro['Livro']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover este livro?") ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <p><?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>, mostrando <strong>{:current}</strong> registros no total de <strong>{:count}</strong> registros.') ?></p>
                <?php if ($this->Paginator->hasPage(2)): ?>
                <ul class="pagination">
                        <?php echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ); ?>
                        <?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ) ?>
                        <?php echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ) ?>
                </ul>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>
<?php $this->Html->addCrumb('Livros', array('plugin' => 'fd_livros', 'controller' => 'fd_livros', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Criar Livro') ?>

<?php $this->start('script') ?>
<?php echo $this->Html->script('/fd_livros/js/fatorcms/livros/scripts'); ?>
<?php $this->end() ?>

<h3>Criar Livro</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Livro', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Livro Ativo</label>
				<div class="icheck">
					<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('url_seo', array('label' => 'URL Seo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('titulo', array('label' => 'Título do livro', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('capa', array('label' => 'Capa do livro', 'type' => 'file', 'div' => false)) ?>
				<br>
				<img id="preview" alt="Preview" height="100">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('descricao_resumida', array('label' => 'Descrição resumda do livro', 'type' => 'textarea', 'maxlength' => 255, 'class' => 'form-control', 'div' => false)) ?>
				<span class="max-count" style="font-weight: bold"></span> caracteres restantes
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('descricao', array('label' => 'Descrição do livro', 'type' => 'textarea', 'class' => 'editor form-control', 'div' => false)) ?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->submit('Cadastrar livro', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
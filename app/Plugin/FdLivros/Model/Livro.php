<?php

class Livro extends FdLivrosAppModel {

    public $actsAs = array(
        'Upload.Upload' => array(
            'capa' => array(
                'thumbnailSizes' => array(
                    'xvga' => '1024x768',
                    'vga' => '640x480',
                    'thumb' => '50x50'
                ),
                'thumbnailMethod' => 'php',
            )
        )
    );
    public $validate = array(
        'url_seo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a url de acesso deste livro',
                'required' => true,
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'messge' => 'Esta url já está cadastrada, informe outra',
                'on' => 'create',
            ),
        ),
        'titulo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o título do livro',
                'required' => true,
            ),
        ),
        'capa' => array(
            'isFileUpload' => array(
                'rule' => array('isFileUpload'),
                'message' => 'Selecione uma capa para o livro',
                'on' => 'create',
            ),
            'isValidMimeType' => array(
                'rule' => array('isValidMimeType', array('image/jpeg', 'image/png')),
                'message' => 'Selecione um arquivo jpg ou png',
                'on' => 'create',
            ),
        ),
        'descricao_resumida' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a descrição resumida do livro',
                'required' => true,
            ),
        ),
        'descricao' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a descrição do livro',
                'required' => true,
            ),
        ),
    );

    public function afterSave($created, $options = null) {
        $controller = 'Livros';
        $model = 'Livro';
        $action = 'detalhe';
        $params_id = null;
        $params_value = null;

        if (isset($this->data[$this->alias]['url_seo'])) {
            app::import('Model', 'FdRotas.Rota');
            $this->Rota = new Rota();

            $rt = $this->Rota->find('first', array(
                'conditions' => array(
                    'AND' => array(
                        'row_id' => $this->data[$this->alias]['id'],
                        'model' => $model,
                    )
                )
            ));

            if ($rt) {
                $rota['id'] = $rt['Rota']['id'];
            } else {
                $rota['id'] = null;
            }

            $rota['controller'] = $controller;
            $rota['model'] = $model;
            $rota['action'] = $action;
            $rota['params_id'] = $params_id;
            $rota['params_value'] = $params_value;
            $rota['row_id'] = $this->data[$this->alias]['id'];
            $rota['seo_url'] = $this->data[$this->alias]['url_seo'];

            $this->Rota->save($rota);
        }
    }

}

<?php

class Configuracao extends FdConfiguracoesAppModel {

	public $validate = array(
		'key' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe a chave da configuração',
				'required' => true,
			),
		),
		'value' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe o valor da configuração',
				'required' => true,
			),
		),
	);

}
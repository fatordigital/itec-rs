<?php

class FdConfiguracoesController extends FdConfiguracoesAppController {

	public $uses = array('FdConfiguracoes.Configuracao');

	public function fatorcms_index($page = 1)
	{

        $this->FilterResults->addFilters(
            array(
                'filtro_key' => array(
                    'Configuracao.key' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                )
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_value' => array(
                    'Configuracao.value' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Configuracao.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $configuracoes = $this->paginate();
        $this->set(compact('configuracoes'));
    }

    public function fatorcms_criar()
    {
      if($this->request->is('post'))
      {
        $this->Configuracao->create();
        if($this->Configuracao->save($this->request->data))
        {
            $this->Session->setFlash('Configuração cadastrado com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_configuracoes', 'controller' => 'fd_configuracoes', 'action' => 'index', 'prefix' => 'fatorcms'));
        } else {
            $this->Session->setFlash('Não foi possível criar a nova configuração, por favor, tente novamente', 'fatorcms_danger');
        }
    }
}

public function fatorcms_editar($id = null)
{
  $this->Configuracao->id = $id;
  if (!$this->Configuracao->exists())
  {
    throw new NotFoundException('Configuração inválida');
}
if ($this->request->is('post') || $this->request->is('put'))
{
    if ($this->Configuracao->save($this->request->data))
    {
        $this->Session->setFlash('Configuração alterada com sucesso', 'fatorcms_success');
        $this->redirect(array('plugin' => 'fd_configuracoes', 'controller' => 'fd_configuracoes', 'action' => 'index', 'prefix' => 'fatorcms'));
    } else {
        $this->Session->setFlash('Não foi possível editar a configuração, por favor, tente mais tarde', 'fatorcms_danger');
    }
} else {
    $this->request->data = $this->Configuracao->read(null, $id);
}
}

public function fatorcms_remover($id = null)
{
  if (!$this->request->is('get'))
  {
    throw new MethodNotAllowedException();
}
$this->Configuracao->id = $id;
if (!$this->Configuracao->exists())
{
    throw new NotFoundException('Configuração inválida');
}
if ($this->Configuracao->delete())
{
    $this->Session->setFlash('Configuração removida com sucesso', 'fatorcms_success');
    $this->redirect(array('plugin' => 'fd_configuracoes', 'controller' => 'fd_configuracoes', 'action' => 'index', 'prefix' => 'fatorcms'));
}
$this->Session->setFlash('A configuração não pode ser removido', 'fatorcms_warning');
$this->redirect(array('plugin' => 'fd_configuracoes', 'controller' => 'fd_configuracoes', 'action' => 'index', 'prefix' => 'fatorcms'));
}

public function fatorcms_status()
{
    if (!$this->request->is('post'))
    {
        throw new NotFoundException('Configuração inválida');
    }
    echo $this->saveStatus('Configuracao', $this->request->data['id'], $this->request->data['value']);
    die;
}

}
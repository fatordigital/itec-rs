<?php $this->Html->addCrumb('Configurações Gerais', array('plugin' => 'fd_configuracoes', 'controller' => 'fd_configuracoes', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Criar Configuração') ?>

<h3>Criar Configuração</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Configuracao', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Configuração Ativa</label>
				<div class="icheck">
					<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('key', array('label' => 'Chave da configuração', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('value', array('label' => 'Valor da configuração', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->submit('Cadastrar configuração', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
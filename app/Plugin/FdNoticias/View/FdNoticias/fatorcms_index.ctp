<?php $this->Html->addCrumb('Notícias') ?>
<?php
$this->Paginator->options(array('url' => array(
    'plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms'
    )
));
?>

<div class="row">
    <h3 class="col-lg-12 pull-left">Notícias <?php echo $this->Html->link('Cadastrar nova', array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'criar', 'prefix' => 'fatorcms'), array('class' => 'btn btn-info pull-right')); ?></h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_titulo', array('placeholder' => 'Filtrar por título', 'class' => 'form-control')) ?>
                </div>
                <div class="form-group">
                    <?php echo $this->FilterForm->input('filtro_conteudo', array('placeholder' => 'Filtrar por conteúdo', 'class' => 'form-control')) ?>
                </div>
                <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                <?php echo $this->Html->link('Limpar Filtro', array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms'), array('class' => 'btn btn-warning')) ?>
                <?php echo $this->FilterForm->end() ?>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('titulo', 'Título') ?></th>
                            <th style="width:165px"><?php echo $this->Paginator->sort('data', 'Data da notícia') ?></th>
                            <th style="width:80px"><?php echo $this->Paginator->sort('status', 'Ativo') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($noticias AS $noticia): ?>
                        <tr>
                            <td><?php echo $noticia['Noticia']['id'] ?></td>
                            <td><?php echo $noticia['Noticia']['titulo'] ?></td>
                            <td><?php echo date('d/m/Y', strtotime($noticia['Noticia']['data'])) ?></td>
                            <td><input type="checkbox" class="atualiza-status" value="<?php echo $noticia['Noticia']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $noticia['Noticia']['id'] ?>" data-url="<?php echo $this->Html->url(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'status', 'prefix' => 'fatorcms')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $noticia['Noticia']['status'] == 1 ? ' checked="checked"' : '' ?>></td>
                            <td><?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'editar', $noticia['Noticia']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'remover', $noticia['Noticia']['id'], 'prefix' => 'fatorcms'), array('class' => 'btn btn-danger btn-sm', 'escape' => false), "Você deseja realmente remover esta notícia?") ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <p><?php echo $this->Paginator->counter('Página <strong>{:page}</strong> de <strong>{:pages}</strong>, mostrando <strong>{:current}</strong> registros no total de <strong>{:count}</strong> registros.') ?></p>
                <?php if ($this->Paginator->hasPage(2)): ?>
                <ul class="pagination">
                    <?php echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ); ?>
                    <?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ) ?>
                    <?php echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ) ?>
                </ul>
                <?php endif; ?>
            </div>
        </section>
    </div>
</div>
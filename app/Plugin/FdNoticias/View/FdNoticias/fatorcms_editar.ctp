<?php $this->Html->addCrumb('Notícias', array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Editar Notícia') ?>

<?php $this->start('script') ?>
<?php echo $this->Html->script('/fd_noticias/js/fatorcms/noticias/scripts'); ?>
<?php $this->end() ?>

<h3>Editar Notícia</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Noticia', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('Noticia.id') ?>
	<?php echo $this->Form->input('Noticia.usuario_id', array('type' => 'hidden')) ?>
	<?php echo $this->Form->input('Noticia.publicacao', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Notícia Ativa</label>
				<div class="icheck">
					<?php echo $this->Form->input('Noticia.status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('noticia_categoria_id', array('label' => 'Categoria da notícia', 'options' => $categorias, 'empty' => 'Selecione', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('url_seo', array('label' => 'URL Seo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('titulo', array('label' => 'Título da notícia', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('descricao_resumida', array('label' => 'Descrição resumida da notícia', 'type' => 'textarea', 'maxlength' => 255, 'div' => false, 'class' => 'form-control')) ?>
				<span class="max-count" style="font-weight: bold"></span> caracteres restantes
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('conteudo', array('label' => 'Conteúdo da notícia', 'type' => 'textarea', 'class' => 'editor form-control', 'div' => false)) ?>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('meta_description', array('label' => 'Meta descrição (para SEO)', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('meta_tags', array('label' => 'Meta tags (para SEO)', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('meta_tags', array('label' => 'Meta tags (para SEO)', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('fonte', array('label' => 'Fonte', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('data', array('label' => 'Data da notícia', 'type' => 'text', 'class' => 'data form-control', 'div' => false)) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('imagem', array('label' => 'Foto destacada da notícia', 'type' => 'file', 'div' => false)) ?>
				<br>
				<?php if(!empty($this->request->data['Noticia']['imagem'])): ?>
					<?php echo $this->Html->image('../files/noticia/imagem/'.$this->request->data['Noticia']['id'].'/thumb_'.$this->request->data['Noticia']['imagem'], array('id' => 'preview', 'height' => '100')) ?>
				<?php else: ?>
					<img id="preview" class="hide-preview" alt="Preview" height="100">
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->submit('Editar notícia', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
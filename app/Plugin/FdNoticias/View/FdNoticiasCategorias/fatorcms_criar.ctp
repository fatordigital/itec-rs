<?php $this->Html->addCrumb('Categorias', array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Criar Categoria') ?>

<?php $this->start('script') ?>
<?php echo $this->Html->script('/fd_noticias/js/fatorcms/noticia_categorias/scripts'); ?>
<?php $this->end() ?>

<h3>Criar Categoria</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('NoticiaCategoria', array('role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('NoticiaCategoria.usuario_id', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Categoria Ativa</label>
				<div class="icheck">
					<?php echo $this->Form->input('NoticiaCategoria.status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('NoticiaCategoria.url_seo', array('label' => 'URL Seo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading tab-bg-dark-navy-blue ">
					<ul class="nav nav-tabs">
						<?php foreach($idiomas as $i => $idioma): ?>
						<li class="<?php echo $i == 0 ? 'active' : '' ?>">
							<a data-toggle="tab" href="#idioma-<?php echo $idioma['Idioma']['id'] ?>">Idioma <?php echo $idioma['Idioma']['nome'] ?></a>
						</li>
						<?php endforeach; ?>
					</ul>
				</header>
				<div class="panel-body">
					<div class="tab-content">
						<?php foreach($idiomas as $i => $idioma): ?>
						<div id="idioma-<?php echo $idioma['Idioma']['id'] ?>" class="tab-pane <?php echo $i == 0 ? 'active' : '' ?>">
							<?php echo $this->Form->input('NoticiaCategoriaTraducao.'.$i.'.idioma_id', array('type' => 'hidden', 'value' => $idioma['Idioma']['id'])) ?>
							<?php echo $this->Form->input('NoticiaCategoriaTraducao.'.$i.'.padrao', array('type' => 'hidden', 'value' => $idioma['Idioma']['padrao'])) ?>
							<?php echo $this->Form->input('NoticiaCategoriaTraducao.'.$i.'.idioma_slug', array('type' => 'hidden', 'value' => $idioma['Idioma']['slug'])) ?>
							<div class="form-group">
								<?php echo $this->Form->input('NoticiaCategoriaTraducao.'.$i.'.nome', array('label' => 'Nome da categoria em '.$idioma['Idioma']['nome'], 'class' => 'form-control', 'div' => false)) ?>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</section>
		</div>
	</div>
	<?php echo $this->Form->submit('Cadastrar categoria', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
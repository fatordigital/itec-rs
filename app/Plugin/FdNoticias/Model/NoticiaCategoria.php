<?php

/**
 * Model Categoria de Notícias
 */
class NoticiaCategoria extends FdNoticiasAppModel {
	
	public $validate = array(
		'url_seo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe a url de acesso desta página',
				'required' => true,
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'messge' => 'Esta url já está cadastrada, informe outra',
				'on' => 'create',
			),
		),
	);

	public $actsAs = array(
		'Containable',
	);

	/**
	 * Relacionamento entre banco de dados
	 * @var string
	 */
	public  $hasMany = array('NoticiaCategoriaTraducao','Noticia');

	/**
	 * Salva o nome da categoria padrão
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function beforeSave($options = array())
	{
		parent::beforeSave();
		if ($this->data['NoticiaCategoriaTraducao'])
		{
			foreach ($this->data['NoticiaCategoriaTraducao'] as $NoticiaCategoriaTraducao)
			{
				if ($NoticiaCategoriaTraducao['NoticiaCategoriaTraducao']['padrao'] == true)
				{
					$this->data['NoticiaCategoria']['nome'] = $NoticiaCategoriaTraducao['NoticiaCategoriaTraducao']['nome'];
				}
			}
		}
	}

	public function afterSave($created)
	{
		$controller = 'Publicacoes';
		$model = 'NoticiaCategoria';
		$action = 'categoria';
		$params_id = null;
		$params_value = null;

		if(isset($this->data[$this->alias]['url_seo']))
		{
			app::import('Model', 'FdRotas.Rota');
			$this->Rota = new Rota();

			$rt = $this->Rota->find('first', array(
				'conditions' => array(
					'AND' => array(
						'row_id' => $this->data[$this->alias]['id'],
						'model' => $model,
					)
				)
			));

			if($rt)
			{
				$rota['id'] = $rt['Rota']['id'];
			} else {
				$rota['id'] = null;
			}

			$rota['controller'] = $controller;
			$rota['model'] = $model;
			$rota['action'] = $action;
			$rota['params_id'] = $params_id;
			$rota['params_value'] = $params_value;
			$rota['row_id'] = $this->data[$this->alias]['id'];
			$rota['seo_url'] = $this->data[$this->alias]['url_seo'];

			$this->Rota->save($rota);
		}
	}

	public function afterFind($results, $primary = false)
	{
		foreach ($results as $key => $val) {
			if (isset($val['NoticiaCategoriaTraducao'][0]['nome']) && $val['NoticiaCategoriaTraducao'][0]['nome'] == '')
			{
				$results[$key]['NoticiaCategoriaTraducao'][0]['nome'] = $val['NoticiaCategoria']['nome'];
			}
		}
		return $results;
	}

	// public function beforeFind($queryData) {
	//     if (is_null(Router::getParam('prefix')))
	//     {
	//     	if (isset($queryData['conditions']) && count($queryData['conditions']) > 0)
	//     	{
	//     		$queryData['conditions'] = array_merge($queryData['conditions'], array('NoticiaCategoriaTraducao.idioma_slug' => Configure::read('Config.language')));
	//     	} else {
	//     		$queryData['conditions'] = array('NoticiaCategoriaTraducao.idioma_slug' => Configure::read('Config.language'));
	//     	}
	//     	return $queryData;
	//     }
	// }

}
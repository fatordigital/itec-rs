<?php

/**
 * Model Notícias
 */
class Noticia extends FdNoticiasAppModel {

    /**
     * Validação de formulário
     * @var array
     */
    public $validate = array(
        'noticia_categoria_id' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Selecione uma categoria',
                'required' => true,
            ),
        ),
        'url_seo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a url de acesso desta página',
                'required' => true,
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'messge' => 'Esta url já está cadastrada, informe outra',
                'on' => 'create',
            ),
        ),
        'titulo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o título da notícia',
                'required' => true,
            ),
        ),
        'descricao_resumida' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a descrição resumida da notícia',
                'required' => true,
            ),
        ),
        'conteudo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o conteúdo da notícia',
                'required' => true,
            ),
        ),
        'imagem' => array(
        /* 'isFileUpload' => array(
          'rule' => array('isFileUpload'),
          'message' => 'Selecione uma foto',
          'on' => 'create',
          ), */
        /* 'isValidMimeType' => array(
          'rule' => array('isValidMimeType', array('image/jpeg', 'image/png')),
          'message' => 'Selecione um arquivo jpg ou png',
          'on' => 'create',
          ), */
        ),
    );

    /**
     * Salva a imagem no banco de dados
     * @var array
     */
    public $actsAs = array(
        'Upload.Upload' => array(
            'imagem' => array(
                'thumbnailSizes' => array(
                    'xvga' => '1024x768',
                    'vga' => '640x480',
                    'thumb' => '100x100'
                ),
                'thumbnailMethod' => 'php',
            )
        ),
        'Containable',
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array('NoticiaCategoria');

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array('NoticiaTraducao');

    /**
     * Trata algumas informações antes de salvar no banco
     * @param  array  $options [description]
     * @return [type]          [description]
     */
    public function beforeSave($options = array()) {
        //debug($this->data);die;
        parent::beforeSave();
        if (!empty($this->data[$this->alias]['data'])) {
            list($dia, $mes, $ano) = explode('/', $this->data[$this->alias]['data']);
            $this->data[$this->alias]['data'] = $ano . '-' . $mes . '-' . $dia . ' 00:00:00';
        } else {
            $this->data[$this->alias]['data'] = date('Y-m-d H:i:s');
        }

        if (!empty($this->data[$this->alias]['data_publicacao'])) {
            list($dia, $mes, $ano) = explode('/', $this->data[$this->alias]['data_publicacao']);
            $this->data[$this->alias]['data_publicacao'] = $ano . '-' . $mes . '-' . $dia . ' 00:00:00';
        }
    }

    public function afterSave($created, $options = null) {
        $model = 'Noticia';
        $controller = 'Noticias';
        $action = 'detalhe';
        $params_id = null;
        $params_value = null;

        if (isset($this->data[$this->alias]['url_seo'])) {
            app::import('Model', 'FdRotas.Rota');
            $this->Rota = new Rota();

            $rt = $this->Rota->find('first', array(
                'conditions' => array(
                    'AND' => array(
                        'row_id' => $this->data[$this->alias]['id'],
                        'model' => $model,
                    )
                )
            ));

            if ($rt) {
                $rota['id'] = $rt['Rota']['id'];
            } else {
                $rota['id'] = null;
            }

            $rota['controller'] = $controller;
            $rota['model'] = $model;
            $rota['action'] = $action;
            $rota['params_id'] = $params_id;
            $rota['params_value'] = $params_value;
            $rota['row_id'] = $this->data[$this->alias]['id'];
            $rota['seo_url'] = $this->data[$this->alias]['url_seo'];

            $this->Rota->save($rota);
        }
    }

    /* public function afterFind($results, $primary = false)
      {
      foreach ($results as $key => $val) {
      if(isset($val['NoticiaCategoria']['NoticiaCategoriaTraducao'][0]['nome']) && $val['NoticiaCategoria']['NoticiaCategoriaTraducao'][0]['nome'] == '')
      {
      $results[$key]['NoticiaCategoria']['NoticiaCategoriaTraducao'][0]['nome'] = $val['NoticiaCategoria']['nome'];
      }

      if (isset($val['NoticiaTraducao'][0]['titulo']) && $val['NoticiaTraducao'][0]['titulo'] == '')
      {
      $results[$key]['NoticiaTraducao'][0]['titulo'] = $val['Noticia']['titulo'];
      }

      if (isset($val['NoticiaTraducao'][0]['descricao_resumida']) && $val['NoticiaTraducao'][0]['descricao_resumida'] == '')
      {
      $results[$key]['NoticiaTraducao'][0]['descricao_resumida'] = $val['Noticia']['descricao_resumida'];
      }

      if (isset($val['NoticiaTraducao'][0]['conteudo']) && $val['NoticiaTraducao'][0]['conteudo'] == '')
      {
      $results[$key]['NoticiaTraducao'][0]['conteudo'] = $val['Noticia']['conteudo'];
      }
      }
      return $results;
      } */

    /* 	public function beforeValidate($options = array())
      {
      if(!isset($this->data['NoticiaTraducao']) || count($this->data['NoticiaTraducao']) == 0)
      return true;
      else
      return false;
      }
     */

    public function beforeFind($queryData) {
        if (is_null(Router::getParam('prefix'))) {
            //debug($queryData);
            /* if($queryData['NoticiaTraducao'][0]['titulo'] == '' || isset($queryData['NoticiaTraducao'][0]['titulo']))
              {
              $queryData['NoticiaTraducao'][0]['nome'] = $queryData['Noticia']['titulo'];
              } */
            // if (isset($queryData['conditions']) && count($queryData['conditions']) > 0)
            // {
            // 	$queryData['conditions'] = array_merge($queryData['conditions'], array('Idioma.slug' => Configure::read('Config.language')));
            // } else {
            // 	 $queryData['conditions'] = array('Idioma.slug' => Configure::read('Config.language'));
            // }
            // return $queryData;
        }
    }

}

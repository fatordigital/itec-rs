<?php

class NoticiaTraducao extends FdNoticiasAppModel {

	public $belongsTo = array('Noticia', 'Idioma');

	public $validate = array(
		'titulo' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Informe um título para a notícia',
				'required' => true,
			),
		),
	);
}
<?php
    Router::connect("/fatorcms/noticias", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/noticias/:page", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/noticias/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/noticias-categorias", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/noticias-categorias/:page", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/noticias-categorias/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'prefix' => 'fatorcms', 'fatorcms' => true));

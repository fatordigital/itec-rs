<?php

class FdNoticiasCategoriasController extends FdNoticiasAppController {

	public $uses = array('FdNoticias.NoticiaCategoria', 'FdIdiomas.Idioma');

	public function fatorcms_index($page = 1)
	{

        $this->FilterResults->addFilters(
            array(
                'filtro_titulo' => array(
                    'OR' => array(
                        'NoticiaCategoria.nome' => array(
                            'operator' => 'LIKE',
                            'value' => array(
                                'before' => '%', // opcional
                                'after'  => '%'  // opcional
                            ),
                        ),
                        'NoticiaCategoriaTraducao.nome' => array(
                            'operator' => 'LIKE',
                            'value' => array(
                                'before' => '%', // opcional
                                'after'  => '%'  // opcional
                            ),
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('joins',
            array(
                array('table' => 'noticia_categoria_traducoes',
                    'alias' => 'NoticiaCategoriaTraducao',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'NoticiaCategoriaTraducao.noticia_categoria_id = NoticiaCategoria.id',
                    ),
                ),
                array('table' => 'idiomas',
                    'alias' => 'Idioma',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Idioma.id = NoticiaCategoriaTraducao.idioma_id',
                    ),
                )
            )
        );

        $this->FilterResults->setPaginate('group', 'NoticiaCategoria.id');

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'NoticiaCategoria.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		$categorias = $this->paginate();
        $this->set(compact('categorias'));
	}

	public function fatorcms_criar()
    {
        if($this->request->is('post'))
        {
            $this->NoticiaCategoria->create();
            if($this->NoticiaCategoria->saveAll($this->request->data))
            {
                $this->Session->setFlash('Categoria cadastrada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar a nova categoria, por favor, tente novamente', 'fatorcms_danger');
            }
        }
        $idiomas = $this->Idioma->find('all', array('conditions' => array('Idioma.status' => 1)));
        $this->set(compact('idiomas'));
    }

    public function fatorcms_editar($id = null)
    {
        if (!$id)
        {
            throw new NotFoundException('Categoria inválida');
        }

        $categoria = $this->NoticiaCategoria->findById($id);

        if (!$categoria)
        {
            throw new NotFoundException('Categoria inválida');
        }

        if ($this->request->is(array('post', 'put')))
        {
            $this->NoticiaCategoria->id = $id;
            if ($this->NoticiaCategoria->saveAll($this->request->data))
            {
                $this->Session->setFlash('Categoria alterada com sucesso', 'fatorcms_success');
                return $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'action' => 'index', 'prefix' => 'fatorcms'));
            }
            $this->Session->setFlash('Não foi possível editar a categoria, por favor, tente mais tarde', 'fatorcms_danger');
        }

        if (!$this->request->data) {
            $this->request->data = $categoria;
            $idiomas = $this->Idioma->find('all', array('conditions' => array('Idioma.status' => 1)));
            $this->set(compact('idiomas'));
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->NoticiaCategoria->id = $id;
        if (!$this->NoticiaCategoria->exists())
        {
            throw new NotFoundException('Categoria inválida');
        }
        if ($this->NoticiaCategoria->delete())
        {
            $this->Session->setFlash('Categoria removida com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('A categoria não pode ser removido', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias_categorias', 'action' => 'index', 'prefix' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Categoria inválida');
        }
        echo $this->saveStatus('NoticiaCategoria', $this->request->data['id'], $this->request->data['value']);
        die;
    }

}
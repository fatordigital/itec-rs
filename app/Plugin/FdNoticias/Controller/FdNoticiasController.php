<?php

class FdNoticiasController extends FdNoticiasAppController {

    public $uses = array('FdNoticias.Noticia', 'FdIdiomas.Idioma');

    public function fatorcms_index($page = 1)
    {

        $this->FilterResults->addFilters(
            array(
                'filtro_titulo' => array(
                    'OR' => array(
                        'Noticia.titulo' => array(
                            'operator' => 'LIKE',
                            'value' => array(
                                'before' => '%',
                                'after' => '%',
                            ),
                        ),
                        'NoticiaTraducao.titulo' => array(
                            'operator' => 'LIKE',
                            'value' => array(
                                'before' => '%',
                                'after' => '%',
                            ),
                        ),
                    ),
                )
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_conteudo' => array(
                    'OR' => array(
                        'Noticia.conteudo' => array(
                            'operator' => 'LIKE',
                            'value' => array(
                                'before' => '%',
                                'after' => '%',
                            ),
                        ),
                        'NoticiaTraducao.conteudo' => array(
                            'operator' => 'LIKE',
                            'value' => array(
                                'before' => '%',
                                'after' => '%',
                            ),
                        ),
                    ),
                )
            )
        );

        $this->FilterResults->setPaginate('joins',
            array(
                array('table' => 'noticia_traducoes',
                    'alias' => 'NoticiaTraducao',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'NoticiaTraducao.noticia_id = Noticia.id',
                    ),
                ),
                array('table' => 'idiomas',
                    'alias' => 'Idioma',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Idioma.id = NoticiaTraducao.idioma_id',
                    ),
                )
            )
        );

        $this->FilterResults->setPaginate('group', 'Noticia.id');

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Noticia.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $noticias = $this->paginate();
        $this->set(compact('noticias'));
    }

    public function fatorcms_criar()
    {
        if($this->request->is('post'))
        {
            $this->Noticia->create();
            if($this->Noticia->save($this->request->data))
            {
                //$this->postarFacebook();
                Cache::write('rotas', false);
                $this->Session->setFlash('Notícia cadastrada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar a nova notícia, por favor, tente novamente', 'fatorcms_danger');
            }
        }

        //$categorias = array('' => 'Selecione...');
        $categorias = $this->Noticia->NoticiaCategoria->find('list', array(
            'fields' => array(
                'NoticiaCategoria.id',
                'NoticiaCategoria.nome',
            ),
            'order' => array(
                'NoticiaCategoria.nome',
            ),
        ));
        $idiomas = $this->Idioma->find('all', array('conditions' => array('Idioma.status' => 1)));
        $this->set(compact('categorias', 'idiomas'));
    }

    public function fatorcms_editar($id = null)
    {
        if (!$id)
        {
            throw new NotFoundException('Notícia inválida');
        }

        $noticias = $this->Noticia->findById($id);

        if (!$noticias)
        {
            throw new NotFoundException('Notícia inválida');
        }

        if ($this->request->is(array('post', 'put')))
        {
            $this->Noticia->id = $id;
            if ($this->Noticia->save($this->request->data))
            {
                //$this->postarFacebook();
                Cache::write('rotas', false);
                $this->Session->setFlash('Notícia alterada com sucesso', 'fatorcms_success');
                return $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms'));
            }
            $this->Session->setFlash('Não foi possível editar a notícia, por favor, tente mais tarde', 'fatorcms_danger');
        }

        if (!$this->request->data) {
            $this->request->data = $noticias;

            $options['fields'] = array(
                'NoticiaCategoria.id',
                'NoticiaCategoriaTraducao.nome',
            );
            $options['joins'] = array(
                array(
                    'table' => 'noticia_categoria_traducoes',
                    'alias' => 'NoticiaCategoriaTraducao',
                    'type' => 'INNER',
                    'conditions' => array(
                        'NoticiaCategoriaTraducao.noticia_categoria_id = NoticiaCategoria.id'
                    ),
                ),
            );
            $options['order'] = array('NoticiaCategoriaTraducao.nome');
            $options['group'] = array('NoticiaCategoria.id');
            $categorias = $this->Noticia->NoticiaCategoria->find('list', $options);
            $idiomas = $this->Idioma->find('all', array('conditions' => array('Idioma.status' => 1)));
            $this->set(compact('categorias', 'idiomas'));
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->Noticia->id = $id;
        if (!$this->Noticia->exists())
        {
            throw new NotFoundException('Notícia inválida');
        }
        if ($this->Noticia->delete())
        {
            Cache::write('rotas', false);
            $this->Session->setFlash('Notícia removida com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('A notícia não pode ser removido', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Notícia inválida');
        }
        echo $this->saveStatus('Noticia', $this->request->data['id'], $this->request->data['value']);
        Cache::write('rotas', false);
        die;
    }

    public function postarFacebook()
    {
        if ($this->request->data['Noticia']['facebook'] == true)
        {
            App::import('Vendor', 'facebook-php-sdk/src/facebook');

            $appId = Configure::read('Configure.facebook_app_id');
            $secret = Configure::read('Configure.facebook_secret');

            $this->Facebook = new Facebook(array(
                'appId' => $appId,
                'secret' => $secret,
                'cookie' => true,
            ));

            debug($this->Facebook);

            try {
                $this->Facebook->api('/me/feed', 'POST', array(
                    'message' => 'Teste post',
                    'link' => 'http://www.google.com.br',
                ));
            } catch(Exception $e) {
                echo $e->getMessage();
            }
            exit;
        }
    }

}
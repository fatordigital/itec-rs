<?php

class FdNoticiasAppController extends AppController {

	public function beforeRender($options = array())
	{
		if (!empty($this->request->data['Noticia']['data']))
		{
			$this->request->data['Noticia']['data'] = date('d/m/Y', strtotime($this->request->data['Noticia']['data']));
		}
		//debug($this->data);
	}

}
$(document).ready(function(){

	$('#NoticiaTitulo').stringToSlug({
		getPut: '#NoticiaUrlSeo'
	});

	$('#NoticiaImagem').on('change', function(){
		preview(this);
	});

	if($('#NoticiaFatorcmsEditarForm').length){
		if($('#preview').hasClass('hide-preview')){
			$('#preview').hide();
		}else{
			$('#preview').show();
		}
	}else{
		$('#preview').hide();
	}

	var preview = function(img) {
		if(img.files && img.files[0]){
			var reader = new FileReader();

			reader.onload = function(e){
				$('#preview').fadeIn().attr('src', e.target.result);
			}

			reader.readAsDataURL(img.files[0]);
		}
	};

	$('#NoticiaDescricaoResumida').limiter(255,$('.max-count'));

});
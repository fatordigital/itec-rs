$(document).ready(function(){

	$('#FotoTitulo').stringToSlug({
		getPut: '#FotoUrlSeo'
	});

	$('#FotoFoto').on('change', function(){
		preview(this);
	});

	if($('#FotoFatorcmsEditarForm').length){
		$('#preview').show();
	}else{
		$('#preview').hide();
	}

	var preview = function(img) {
		if(img.files && img.files[0]){
			var reader = new FileReader();

			reader.onload = function(e){
				$('#preview').fadeIn().attr('src', e.target.result);
			}

			reader.readAsDataURL(img.files[0]);
		}
	};

});
<?php

class FdFotosController extends FdGaleriasAppController {

	public $uses = array('FdGalerias.Foto');

	public function fatorcms_index($page = 1)
	{

        $this->FilterResults->addFilters(
            array(
                'filtro_titulo' => array(
                    'Foto.titulo' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Foto.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		$fotos = $this->paginate();
		$this->set(compact('fotos'));
	}

	public function fatorcms_criar()
	{
        if($this->request->is('post'))
        {
            $this->Foto->create();
            if($this->Foto->save($this->request->data))
            {
                $this->Session->setFlash('Foto cadastrada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar a nova foto, por favor, tente novamente', 'fatorcms_danger');
            }
        }
        $galerias = $this->Foto->Galeria->find('list', array(
            'conditions' => array(
                'Galeria.status' => 1,
            ),
            'fields' => array(
                'Galeria.id',
                'Galeria.titulo',
            ),
            'order' => array(
                'Galeria.titulo',
            ),
        ));
        $this->set(compact('galerias'));
	}

	public function fatorcms_editar($id = null)
    {
        $this->Foto->id = $id;
        if (!$this->Foto->exists())
        {
            throw new NotFoundException('Foto inválida');
        }
        if ($this->request->is('post') || $this->request->is('put'))
        {
            if ($this->Foto->save($this->request->data))
            {
                $this->Session->setFlash('Foto alterada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível editar a foto, por favor, tente mais tarde', 'fatorcms_danger');
            }
        } else {
            $this->request->data = $this->Foto->read(null, $id);
            $galerias = $this->Foto->Galeria->find('list', array(
            'conditions' => array(
                'Galeria.status' => 1,
            ),
            'fields' => array(
                'Galeria.id',
                'Galeria.titulo',
            ),
            'order' => array(
                'Galeria.titulo',
            ),
        ));
        $this->set(compact('galerias'));
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->Foto->id = $id;
        if (!$this->Foto->exists())
        {
            throw new NotFoundException('Foto inválida');
        }
        if ($this->Foto->delete())
        {
            $this->Session->setFlash('Foto removida com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('A foto não pode ser removida', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'action' => 'index', 'prefixo' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Foto inválida');
        }
        echo $this->saveStatus('Foto', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}
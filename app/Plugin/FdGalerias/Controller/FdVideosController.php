<?php

class FdVideosController extends FdGaleriasAppController {

	public $uses = array('FdGalerias.Video');

	public function fatorcms_index($page = 1)
	{

        $this->FilterResults->addFilters(
            array(
                'filtro_titulo' => array(
                    'Galeria.titulo' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Video.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		$videos = $this->paginate();
		$this->set(compact('videos'));
	}

	public function fatorcms_criar()
	{
        if($this->request->is('post'))
        {
            $this->Video->create();
            if($this->Video->save($this->request->data))
            {
                $this->Session->setFlash('Vídeo cadastrado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar o novo vídeo, por favor, tente novamente', 'fatorcms_danger');
            }
        }
        $galerias = $this->Video->Galeria->find('list', array(
            'conditions' => array(
                'Galeria.status' => 1,
            ),
            'fields' => array(
                'Galeria.id',
                'Galeria.titulo',
            ),
            'order' => array(
                'Galeria.titulo',
            ),
        ));
        $this->set(compact('galerias'));
	}

	public function fatorcms_editar($id = null)
    {
        $this->Video->id = $id;
        if (!$this->Video->exists())
        {
            throw new NotFoundException('Vídeo inválido');
        }
        if ($this->request->is('post') || $this->request->is('put'))
        {
            if ($this->Video->save($this->request->data))
            {
                $this->Session->setFlash('Vídeo alterado com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível editar o vídeo, por favor, tente mais tarde', 'fatorcms_danger');
            }
        } else {
            $galerias = $this->Video->Galeria->find('list', array(
                'conditions' => array(
                    'Galeria.status' => 1,
                ),
                'fields' => array(
                    'Galeria.id',
                    'Galeria.titulo',
                ),
                'order' => array(
                    'Galeria.titulo',
                ),
            ));
            $this->request->data = $this->Video->read(null, $id);
            $this->set(compact('galerias'));
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->Video->id = $id;
        if (!$this->Video->exists())
        {
            throw new NotFoundException('Vídeo inválido');
        }
        if ($this->Video->delete())
        {
            $this->Session->setFlash('Vídeo removido com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('O vídeo não pode ser removido', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'action' => 'index', 'prefixo' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Vídeo inválida');
        }
        echo $this->saveStatus('Video', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}
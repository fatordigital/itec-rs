<?php

class FdGaleriasController extends FdGaleriasAppController {

	public $uses = array('FdGalerias.Galeria');

	public function fatorcms_index($page = 1)
	{

        $this->FilterResults->addFilters(
            array(
                'filtro_titulo' => array(
                    'Galeria.titulo' => array(
                        'operator' => 'LIKE',
                        'value' => array(
                            'before' => '%',
                            'after' => '%',
                        ),
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('order', 'Galeria.created DESC');

        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		$galerias = $this->paginate();
		$this->set(compact('galerias'));
	}

	public function fatorcms_criar()
	{
        if($this->request->is('post'))
        {
            $this->Galeria->create();
            if($this->Galeria->save($this->request->data))
            {
                $this->Session->setFlash('Galeria cadastrada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível criar o nova galeria, por favor, tente novamente', 'fatorcms_danger');
            }
        }
	}

	public function fatorcms_editar($id = null)
    {
        $this->Galeria->id = $id;
        if (!$this->Galeria->exists())
        {
            throw new NotFoundException('Galeria inválida');
        }
        if ($this->request->is('post') || $this->request->is('put'))
        {
            if ($this->Galeria->save($this->request->data))
            {
                $this->Session->setFlash('Galeria alterada com sucesso', 'fatorcms_success');
                $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefix' => 'fatorcms'));
            } else {
                $this->Session->setFlash('Não foi possível editar a galeria, por favor, tente mais tarde', 'fatorcms_danger');
            }
        } else {
            $this->request->data = $this->Galeria->read(null, $id);
        }
    }

    public function fatorcms_remover($id = null)
    {
        if (!$this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }
        $this->Galeria->id = $id;
        if (!$this->Galeria->exists())
        {
            throw new NotFoundException('Galeria inválida');
        }
        if ($this->Galeria->delete())
        {
            $this->Session->setFlash('Galeria removida com sucesso', 'fatorcms_success');
            $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefix' => 'fatorcms'));
        }
        $this->Session->setFlash('A galeria não pode ser removida', 'fatorcms_warning');
        $this->redirect(array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefixo' => 'fatorcms'));
    }

    public function fatorcms_status()
    {
        if (!$this->request->is('post'))
        {
            throw new NotFoundException('Galeria inválida');
        }
        echo $this->saveStatus('Galeria', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}
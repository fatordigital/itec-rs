<?php

class Video extends FdGaleriasAppModel {

    public $belongsTo = array('Galeria');
    public $validate = array(
        'url_seo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a url de acesso deste vídeo',
                'required' => true,
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'messge' => 'Esta url já está cadastrada, informe outra',
                'on' => 'create',
            ),
        ),
        'titulo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o título do vídeo',
                'required' => true,
            ),
        ),
        'video' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a url ou id do vídeo do youtube',
                'required' => true,
            ),
        ),
        'legenda' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a legenda do vídeo',
                'required' => true,
            ),
        ),
        'galeria_id' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a galeria que será atrelada ao vídeo',
                'required' => true,
            ),
        ),
    );

    public function afterSave($created, $options = null) {
        $model = 'Video';
        $controller = 'Videos';
        $action = 'detalhe';
        $params_id = null;
        $params_value = null;

        if (isset($this->data[$this->alias]['url_seo'])) {
            app::import('Model', 'FdRotas.Rota');
            $this->Rota = new Rota();

            $rt = $this->Rota->find('first', array(
                'conditions' => array(
                    'AND' => array(
                        'row_id' => $this->data[$this->alias]['id'],
                        'model' => $model,
                    )
                )
            ));

            if ($rt) {
                $rota['id'] = $rt['Rota']['id'];
            } else {
                $rota['id'] = null;
            }

            $rota['controller'] = $controller;
            $rota['model'] = $model;
            $rota['action'] = $action;
            $rota['params_id'] = $params_id;
            $rota['params_value'] = $params_value;
            $rota['row_id'] = $this->data[$this->alias]['id'];
            $rota['seo_url'] = $this->data[$this->alias]['url_seo'];

            $this->Rota->save($rota);
        }
    }

}

<?php

class Foto extends FdGaleriasAppModel {

    public $validate = array(
        'url_seo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a url de acesso deste livro',
                'required' => true,
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'messge' => 'Esta url já está cadastrada, informe outra',
                'on' => 'create',
            ),
        ),
        'titulo' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o título do livro',
                'required' => true,
            ),
        ),
        'foto' => array(
            'isFileUpload' => array(
                'rule' => array('isFileUpload'),
                'message' => 'Selecione uma foto',
                'on' => 'create',
            ),
            'isValidMimeType' => array(
                'rule' => array('isValidMimeType', array('image/jpeg', 'image/png')),
                'message' => 'Selecione um arquivo jpg ou png',
                'on' => 'create',
            ),
        ),
        'legenda' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a descrição da foto',
                'required' => true,
            ),
        ),
        'galeria_id' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe a galeria relacionada a foto',
                'required' => true,
            ),
        ),
    );
    public $actsAs = array(
        'Upload.Upload' => array(
            'foto' => array(
                'thumbnailSizes' => array(
                    'xvga' => '1470w',
                    'vga' => '640w',
                    'thumb' => '100w'
                ),
                'thumbnailMethod' => 'php',
            )
        )
    );
    public $belongsTo = array('Galeria');

    public function afterSave($created, $options = null) {
        $model = 'Foto';
        $controller = 'Fotos';
        $action = 'detalhe';
        $params_id = null;
        $params_value = null;

        if (isset($this->data[$this->alias]['url_seo'])) {
            app::import('Model', 'FdRotas.Rota');
            $this->Rota = new Rota();

            $rt = $this->Rota->find('first', array(
                'conditions' => array(
                    'AND' => array(
                        'row_id' => $this->data[$this->alias]['id'],
                        'model' => $model,
                    )
                )
            ));

            if ($rt) {
                $rota['id'] = $rt['Rota']['id'];
            } else {
                $rota['id'] = null;
            }

            $rota['controller'] = $controller;
            $rota['model'] = $model;
            $rota['action'] = $action;
            $rota['params_id'] = $params_id;
            $rota['params_value'] = $params_value;
            $rota['row_id'] = $this->data[$this->alias]['id'];
            $rota['seo_url'] = $this->data[$this->alias]['url_seo'];

            $this->Rota->save($rota);
        }
    }

}

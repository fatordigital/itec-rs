<?php $this->Html->addCrumb('Galerias', array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Editar Galeria') ?>

<?php $this->start('script') ?>
<?php echo $this->Html->script('/fd_galerias/js/fatorcms/galerias/scripts'); ?>
<?php $this->end() ?>

<h3>Editar Livro</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Galeria', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('id') ?>
	<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Galeria Ativa</label>
				<div class="icheck">
					<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('url_seo', array('label' => 'URL Seo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('titulo', array('label' => 'Título do galeria', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->submit('Salvar livro', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
<?php $this->Html->addCrumb('Vídeo', array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Editar Galeria') ?>

<?php $this->start('script') ?>
<?php echo $this->Html->script('/fd_galerias/js/fatorcms/videos/scripts'); ?>
<?php $this->end() ?>

<h3>Editar Livro</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Video', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('id') ?>
	<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Video Ativo</label>
				<div class="icheck">
					<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('url_seo', array('label' => 'URL Seo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('titulo', array('label' => 'Título do vídeo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('video', array('label' => 'URL ou ID do vídeo no Youtube', 'div' => false, 'class' => 'form-control')) ?>
				<br>
				<?php echo $this->Youtube->thumbnail($this->request->data['Video']['video']) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('legenda', array('label' => 'Legenda do vídeo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('galeria_id', array('label' => 'Galeria do vídeo', 'div' => false, 'class' => 'form-control', 'options' => $galerias, 'empty' => 'Selecione uma galeria')); ?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->submit('Salvar vídeo', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
<?php $this->Html->addCrumb('Fotos', array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'action' => 'index', 'prefix' => 'fatorcms')) ?>
<?php $this->Html->addCrumb('Editar Foto') ?>

<?php $this->start('script') ?>
<?php echo $this->Html->script('/fd_galerias/js/fatorcms/fotos/scripts'); ?>
<?php $this->end() ?>

<h3>Editar Foto</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Foto', array('type' => 'file', 'role' => 'form', 'class' => 'minimal')) ?>
	<?php echo $this->Form->input('id') ?>
	<?php echo $this->Form->input('usuario_id', array('type' => 'hidden')) ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label>Vídeo Ativo</label>
				<div class="icheck">
					<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 1, 'div' => false, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>', 'separator' => '</div><div class="clearfix"></div><div class="radio">')) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('url_seo', array('label' => 'URL Seo', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('titulo', array('label' => 'Título da foto', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('foto', array('label' => 'Foto', 'type' => 'file', 'div' => false)) ?>
				<br>
				<?php echo $this->Html->image('../files/foto/foto/'.$this->request->data['Foto']['id'].'/thumb_'.$this->request->data['Foto']['foto'], array('id' => 'preview', 'height' => '100')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('legenda', array('label' => 'Legenda da foto', 'div' => false, 'class' => 'form-control')) ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<?php echo $this->Form->input('galeria_id', array('label' => 'Galeria da foto', 'div' => false, 'class' => 'form-control', 'options' => $galerias, 'empty' => 'Selecione uma galeria')); ?>
			</div>
		</div>
	</div>
	<?php echo $this->Form->submit('Salvar vídeo', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>
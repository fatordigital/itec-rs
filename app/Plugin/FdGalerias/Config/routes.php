<?php
	Router::connect("/fatorcms/galerias", array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
	Router::connect("/fatorcms/galerias/:page", array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/galerias/:action/*", array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/videos", array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
	Router::connect("/fatorcms/videos/:page", array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/videos/:action/*", array('plugin' => 'fd_galerias', 'controller' => 'fd_videos', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/fotos", array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
	Router::connect("/fatorcms/fotos/:page", array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true), array('pass' => array('page'), 'page' => '[0-9]+'));
    Router::connect("/fatorcms/fotos/:action/*", array('plugin' => 'fd_galerias', 'controller' => 'fd_fotos', 'prefix' => 'fatorcms', 'fatorcms' => true));
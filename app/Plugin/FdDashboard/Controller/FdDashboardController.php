<?php

class FdDashboardController extends FdDashboardAppController
{

    private function csv($file = null){
        $handle  = fopen($file, "r");
        $retorno = array();
        $fields  = array();
        $first = true;
        while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
            if($first){
                foreach ($data as $key => $value) {
                    $fields[$key] = $this->slug($value);
                }

                $first = false;
                continue;
            }else{

                $dt = array();
                foreach ($data as $key => $value) {
                    if(isset($fields[$key])){
                        $dt[$fields[$key]] = $value;
                    }
                }

                $retorno[] = $dt;
            }
        }

        return $retorno;
    }

    private function setlog($arr, $file = 'e.log'){
        // file_put_contents($file, print_r(array(date('Y-m-d H:i:s')),true), FILE_APPEND);
        file_put_contents($file, print_r($arr, true), FILE_APPEND);
    }

    private function slug($string){
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', trim($string))));
        return $slug;
    }

    private function debug($var){
        print('<pre>');
        print_r($var);
    }


    public function fatorcms_index()
    {
//        $retorno = $this->csv(WWW_ROOT . DS . 'resumos-20-08-2017.csv');
//
//
//        App::import('Model', 'FdEdicoes.Edicao');
//        $edicaoModel = new Edicao();
//
//        $traducoes = array(
//            'Resumo' => 1,
//            'Resumen' => 3,
//            'Riassunto' => 4,
//            'Abstract' => 2
//        );
//        echo '<pre>';
//        $save = array();
//        foreach ($retorno as $item) {
//            $edicao = $atributos = array();
//            if (count($item) > 11) {
//
//                print_r($item);
//
//                # Trata os valores
//                $PTraducao = isset($traducoes[$item['1-1']]) && !empty($traducoes[$item['1-1']]) ? $traducoes[$item['1-1']] : 1;
//                $PTitulo = trim($item['t_tulo']);
//                $PSlug = strtolower(Inflector::slug($PTitulo, '-'));
//                $PTexto = $item['1-2'];
//                $PalavraChave = $item['1-4'];
//
//                $STraducao = isset($traducoes[$item['2-1']]) && !empty($traducoes[$item['2-1']]) ? $traducoes[$item['2-1']] : 2;
//                $STexto = $item['2-2'];
//                $SPalavraChave = $item['2-4'];
//
//
//                $Resumo = '';
//                $Texto = '';
//                $Ano = $item['_ano'];
//                $Volume = $item['volume'];
//                $Numero = (int)$item['n_mero'];
//                $Autores = $item['autores'];
//                $Paginacao = $item['pagina_o'];
//                # Trata os valores
//
//                $edicao = array(
//                    'id' => null,
//                    'revista_id' => 1,
//                    'titulo_pt' => $PTitulo,
//                    'titulo_en' => '',
//                    'slug' => $PSlug,
//                    'ano' => $Ano,
//                    'volume' => $Volume,
//                    'numero' => $Numero,
//                    'autores' => $Autores,
//                    'slug_paginacao' => str_replace('-', '', $Paginacao),
//                    'paginacao' => $Paginacao,
//                    'texto_pt' => $PTexto,
//                    'palavra_chave_pt' => $PalavraChave,
//                    'texto_en' => $STexto,
//                    'palavra_chave_en' => $SPalavraChave,
//                    'resumo_pt' => $item['1-1'],
//                    'resumo_en' => $item['2-1'],
//                    'chave_pt' => $item['1-3'],
//                    'chave_en' => $item['2-3']
//                );
//
//                $save[] = array(
//                    'Edicao' => $edicao
//                );
//
//            }
//
//
//        }
//
////        exit;
//
//        foreach ($save as $item) {
//            $edicaoModel->save($item);
//
//        }
//
//        echo '<pre>';
//        die(print_r($save));

    }

}
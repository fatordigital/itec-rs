<?php
    Router::connect("/fatorcms/acl/acos", array('plugin' => 'acl', 'controller' => 'acos', 'action' => 'index', 'prefixo' => 'fatorcms'));
    Router::connect("/fatorcms/acl/acos/:action/*", array('plugin' => 'acl', 'controller' => 'acos', 'prefixo' => 'fatorcms'));
    Router::connect("/fatorcms/acl/aros", array('plugin' => 'acl', 'controller' => 'aros', 'action' => 'index', 'prefixo' => 'fatorcms'));
    Router::connect("/fatorcms/acl/aros/:action/*", array('plugin' => 'acl', 'controller' => 'aros', 'prefixo' => 'fatorcms'));
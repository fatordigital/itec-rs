<?php $this->Html->addCrumb('ACL', '/fatorcms/acl_manager/acl') ?>
<?php $this->Html->addCrumb(sprintf("Permissões para %s", $aroAlias)) ?>


<h3><?php echo sprintf("Permissões para %s", $aroAlias) ?></h3>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<div class="panel-body">
				<?php echo $this->Form->create('Perms', array('role' => 'form')); ?>
				<table class="table table-hover general-table">
					<thead>
						<tr>
							<th>Ação</th>
							<?php foreach ($aros as $aro): ?>
							<?php $aro = array_shift($aro); ?>
							<th><?php echo h($aro['nome']); ?></th>
						<?php endforeach; ?>
					</tr>
				</thead>
				<tbody>
					<?php
					$uglyIdent = Configure::read('AclManager.uglyIdent'); 
					$lastIdent = null;
					foreach ($acos as $id => $aco) {
						$action = $aco['Action'];
						$alias = $aco['Aco']['alias'];
						$ident = substr_count($action, '/');
						if ($ident <= $lastIdent && !is_null($lastIdent)) {
							for ($i = 0; $i <= ($lastIdent - $ident); $i++) {
								?></tr><?php
							}
						}
						if ($ident != $lastIdent) {
							?><tr class='aclmanager-ident-<?php echo $ident; ?>'><?php
						}
						?><td><?php echo ($ident == 1 ? "<strong>" : "" ) . ($uglyIdent ? str_repeat("&nbsp;&nbsp;", $ident) : "") . h($alias) . ($ident == 1 ? "</strong>" : "" ); ?></td>
						<?php foreach ($aros as $aro): 
						$inherit = $this->Form->value("Perms." . str_replace("/", ":", $action) . ".{$aroAlias}:{$aro[$aroAlias]['id']}-inherit");
						$allowed = $this->Form->value("Perms." . str_replace("/", ":", $action) . ".{$aroAlias}:{$aro[$aroAlias]['id']}"); 
						$value = $inherit ? 'inherit' : null; 
						$icon = $this->Html->image(($allowed ? 'test-pass-icon.png' : 'test-fail-icon.png')); ?>
						<td><?php echo $icon . " " . $this->Form->select("Perms." . str_replace("/", ":", $action) . ".{$aroAlias}:{$aro[$aroAlias]['id']}", array(array('inherit' => 'Herdar', 'allow' => 'Permitir', 'deny' => 'Negar')), array('empty' => 'Nenhuma mudança', 'value' => $value)); ?></td>
					<?php endforeach; ?>
					<?php 
					$lastIdent = $ident;
				}
				for ($i = 0; $i <= $lastIdent; $i++) {
					?></tr><?php
				}
				?></tbody>
			</table>
			<?php if ($this->Paginator->hasPage(2)): ?>
			<ul class="pagination">
				<?php echo $this->Paginator->prev( '<<', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ); ?>
				<?php echo $this->Paginator->numbers( array( 'tag' => 'li', 'separator' => '', 'currentClass' => 'active', 'currentTag' => 'a' ) ) ?>
				<?php echo $this->Paginator->next( '>>', array( 'class' => '', 'tag' => 'li' ), null, array( 'class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a' ) ) ?>
			</ul>
		<?php endif; ?>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<?php echo $this->Form->end() ?>
		<hr>
		<h2>Gerenciar</h2>
		<?php 
		$aroModels = Configure::read("AclManager.aros");
		if ($aroModels > 1): ?>
			<p>
			<?php foreach ($aroModels as $aroModel): ?>
				<?php echo $this->Html->link($aroModel, array('plugin' => 'acl_manager', 'controller' => 'acl', 'prefixo' => 'fatorcms', 'aro' => $aroModel), array('class' => 'btn btn-info')); ?>
			<?php endforeach; ?>
			</p>
		<?php endif; ?>
		<hr>
		<h2>Mais opções</h2>
		<p><strong>ATENÇÃO:</strong> Use a ferramenta com cuidado. O mau uso da mesma pode gerar danos ao sistema.</p>
		<p><?php echo $this->Html->link('Gerenciar permissões', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'permissions'), array('class' => 'btn btn-info')) ?> <?php echo $this->Html->link('Atualizar ACOs', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'update_acos'), array('class' => 'btn btn-warning')) ?> <?php echo $this->Html->link('Atualizar AROs', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'update_aros', 'prefixo' => 'fatorcms'), array('class' => 'btn btn-warning')) ?> <?php echo $this->Html->link('Remover ACOs/AROs', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'drop'), array('class' => 'btn btn-danger'), "Você deseja remover todos os ACOs e AROs?") ?> <?php echo $this->Html->link('Remover permissões', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'drop_perms'), array('class' => 'btn btn-danger'), "Você deseja remover todas as permissões?"); ?></p>
	</div>
</section>
</div>
</div>
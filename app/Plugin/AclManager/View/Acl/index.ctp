<?php $this->Html->addCrumb('ACL') ?>

<h3>ACL</h3>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
            <div class="panel-body">
            	<p>Selecione uma das opções abaixo.</p>
		<p><strong>ATENÇÃO:</strong> Use a ferramenta com cuidado. O mau uso da mesma pode gerar danos ao sistema.</p>
		<p><?php echo $this->Html->link('Gerenciar permissões', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'permissions'), array('class' => 'btn btn-info')) ?> <?php echo $this->Html->link('Atualizar ACOs', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'update_acos'), array('class' => 'btn btn-warning')) ?> <?php echo $this->Html->link('Atualizar AROs', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'update_aros'), array('class' => 'btn btn-warning')) ?> <?php echo $this->Html->link('Remover ACOs/AROs', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'drop', 'prefixo' => 'fatorcms'), array('class' => 'btn btn-danger'), "Você deseja remover todos os ACOs e AROs?") ?> <?php echo $this->Html->link('Remover permissões', array('plugin' => 'acl_manager', 'controller' => 'acl', 'action' => 'drop_perms'), array('class' => 'btn btn-danger'), "Você deseja remover todas as permissões?"); ?></p>
            </div>
        </section>
	</div>
</div>
<?php
App::uses('AppModel', 'Model');

class Contato extends AppModel {
	public $useTable = false;

	public $validate = array(
		'nome' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Informe o seu nome',
		),
		'email' => array(
			'rule' => 'email',
			'required' => true,
			'message' => 'Informe o seu e-mail',
		),
		'mensagem' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Informe a sua mensagem',
		),
	);
}
<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

    Router::redirect(
        '/edicoes',
        '/rec',
        array('status' => 301)
    );

	Router::connect('/', array('controller' => 'home', 'action' => 'index'));

	Router::connect('/destaques', array('controller' => 'noticias', 'action' => 'index'));
	Router::connect('/indicacoes-bibliograficas', array('controller' => 'livros', 'action' => 'index'));
	Router::connect('/galerias', array('controller' => 'galerias', 'action' => 'index'));
	Router::connect('/contato', array('controller' => 'contato', 'action' => 'index'));
    Router::connect('/edicoes', array('controller' => 'edicoes', 'action' => 'index'));
    Router::connect('/edicoes/*', array('controller' => 'edicoes', 'action' => 'index'));
    Router::connect('/:idioma/edicoes', array('controller' => 'edicoes', 'action' => 'index'), array('idioma' => 'en|pt-br'));
    Router::connect('/:idioma/edicoes/*', array('controller' => 'edicoes', 'action' => 'index'), array('idioma' => 'en|pt-br'));

    Router::connect('/rec', array('controller' => 'edicoes', 'action' => 'index'));
    Router::connect('/:idioma/rec', array('controller' => 'edicoes', 'action' => 'index'), array('idioma' => 'en|pt-br'));
    Router::connect('/:idioma/rec/*', array('controller' => 'edicoes', 'action' => 'index'), array('idioma' => 'en|pt-br'));

/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

    Router::connect('/login', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login', 'fatorcms' => false));
    Router::connect('/sair', array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'sair', 'fatorcms' => false));
    Router::connect('/fatorcms', array('plugin' => 'fd_dashboard', 'controller' => 'fd_dashboard', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));


    App::import('Model', 'FdRotas.Rota');
    $modelRota = new Rota();
    $rotas = $modelRota->getRotas();

    $request_url = $_SERVER['REQUEST_URI'];
    $explode = explode('/', $request_url);
    $url;

    while (count($explode) > 0) {
		if (stripos($_SERVER['SCRIPT_NAME'], $request_url) === false && stripos($_SERVER['SCRIPT_NAME'], $explode[1]) == true)
		{
			unset( $explode[count($explode)-1] );
			if (count($explode) > 0)
			{
				$request_url = '';
				foreach ($explode as $ex) {
					if ($ex != "")
					{
						$request_url .= '/'. $ex;
					}
				}
			}
		} else {
			$uri = str_replace($request_url.'/', '', $_SERVER['REQUEST_URI']);

			if (strlen($uri) > 1 && substr($uri,0,1) == '/')
			{
				$uri = substr($uri, 1);
			}
			$explode = array();
		}
	}

	$url = $uri;
	if ($_SERVER['QUERY_STRING'] != "")
	{
		$url = str_replace('?'.$_SERVER['QUERY_STRING'], '', $url);
	}

	//remove parametros
	$tem_paramentro = false;
	while (strpos(basename($url), ':') != "") {
		$tem_paramentro = true;
		$url = str_replace(basename($url), '', $url);
	}

	//removo a ultima barra se tiver
	while(substr($url, -1) == '/')
	{
		$url = substr($url, 0, -1);
	}

	//removo o fullscreen da url
	if(stripos($url, 'fullscreen/') !== false)
	{
		$url = str_replace('fullscreen/', '', $url);
	}

	// App::import('Model', 'FdIdiomas.Idioma');
    // $modelIdioma = new Idioma();
    // $idiomas = $modelIdioma->find('all', array('fields' => array('Idioma.slug'), 'conditions' => array('Idioma.status' => true)));
    $idiomas[0]['Idioma']['slug'] = 'pt';
    $idiomas[1]['Idioma']['slug'] = 'en';
    $idiomas[2]['Idioma']['slug'] = 'es';
    if(!empty($idiomas)){
    	foreach ($idiomas as $key => $idioma) {
    		//removo a language da url
			if(stripos($url, $idioma['Idioma']['slug'].'/') !== false){
				$url = str_replace($idioma['Idioma']['slug'].'/', '', $url);
			}
    	}
    }

	// $url = urlencode($url);
	$url = urldecode($url);

	//var_dump($url);die;

	if (array_key_exists($url, $rotas))
	{
		$rota = $uri;
		if ($tem_paramentro == true)
		{
			$rota = $url;
		}

		$params = array();
		if ($rotas[$url]['params_value'] != null)
		{
			$params = array($rotas[$url]['params_id'] => $rotas[$url]['params_value']);
			Router::connect("/{$rota}", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $params), array());
			Router::connect("/{$rota}/", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $params), array());
			Router::connect("/{$rota}/*", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $params), array());

			Router::connect("/{$url}", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $params), array());
			Router::connect("/{$url}/", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $params), array());
			Router::connect("/{$url}/*", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $params), array());
		} else {

			// var_dump($rota);
			// var_dump($url);
			// die;

			// Router::connect("/$rota", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());
			// Router::connect("/{$rota}/", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());
			// Router::connect("/{$rota}/*", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());

			Router::connect("/:language/{$url}", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array('language' => '[a-z-]{2,5}'));
			Router::connect("/:language/{$url}/", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array('language' => '[a-z-]{2,5}'));
			Router::connect("/:language/{$url}/*", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array('language' => '[a-z-]{2,5}'));

			Router::connect("/{$rota}", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());
			Router::connect("/{$rota}/", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());
			Router::connect("/{$rota}/*", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());

			Router::connect("/{$url}", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());
			Router::connect("/{$url}/", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());
			Router::connect("/{$url}/*", array('controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id']), array());
		}
	}

	Router::connect('/:language/:controller', array('action' => 'index'), array('language' => '[a-z-]{2,5}'));
	Router::connect('/:language/:controller/:action/*', array(), array('language' => '[a-z-]{2,5}'));

	Router::connect('/:pagename', array('controller' => 'pages', 'action' => 'display'), array('pagename', 'pass' => array('pagename')));



/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

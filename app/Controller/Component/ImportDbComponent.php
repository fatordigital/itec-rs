<?php

class ImportDbComponent extends Component {

	private function slug($str, $separator = '-')
	{
		setlocale(LC_ALL, 'pt_BR.ISO-8859-1');
		$str = @iconv('UTF-8', 'ISO-8859-1//IGNORE//TRANSLIT', $str);

		$str = strtolower($str);

	    // Código ASCII das vogais
	    $ascii['a'] = range(224, 230);
	    $ascii['e'] = range(232, 235);
	    $ascii['i'] = range(236, 239);
	    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
	    $ascii['u'] = range(249, 252);

	    // Código ASCII dos outros caracteres
	    $ascii['b'] = array(223);
	    $ascii['c'] = array(231);
	    $ascii['d'] = array(208);
	    $ascii['n'] = array(241);
	    $ascii['y'] = array(253, 255);

	    foreach ($ascii as $key=>$item) {
	        $acentos = '';
	        foreach ($item AS $codigo) $acentos .= chr($codigo);
	        $troca[$key] = '/['.$acentos.']/i';
	    }

	    $str = preg_replace(array_values($troca), array_keys($troca), $str);

	    // Slug?
	    if ($separator) {
	        // Troca tudo que não for letra ou número por um caractere ($slug)
	        $str = preg_replace('/[^a-z0-9]/i', $separator, $str);
	        // Tira os caracteres ($slug) repetidos
	        $str = preg_replace('/' . $separator . '{2,}/i', $separator, $str);
	        $str = trim($str, $separator);
	    }
	    return $str;
	}

	private function utf8Fix($str, $html = false)
	{
		setlocale(LC_ALL, 'pt_BR.ISO-8859-1');
		Configure::write('App.encoding', 'ISO-8859-1');
		if($html == true)
		{
			return @iconv('UTF-8', 'ISO-8859-1//IGNORE//TRANSLIT', html_entity_decode(html_entity_decode($str)));
		} else {
			return @iconv('UTF-8', 'ISO-8859-1//IGNORE//TRANSLIT', $str);
		}
	}

	private function connect()
	{
		$db = new PDO('mysql:host=server;dbname=gtadvocacia_original', 'root', 'root');
		// $db->exec('SET NAMES utf8');

		$db->query("SET character_set_results = 'utf8',
                 character_set_client = 'utf8', 
                 character_set_connection = 'utf8',
                 character_set_database = 'utf8',  
                 character_set_server = 'utf8'");
		return $db;
	}

    public function categorias()
    {
        App::import('Model','FdNoticias.NoticiaCategoria');
        App::Import('Model','FdNoticias.NoticiaCategoriaTraducao');

        $db = $this->connect();

        $categorias = $db->query('SELECT * FROM fd_categoria');

        $this->NoticiaCategoria = new NoticiaCategoria;
        $this->NoticiaCategoriaTraducao = new NoticiaCategoriaTraducao;

        $this->NoticiaCategoria->create();
        foreach($categorias as $i => $categoria) {
            $url = $categoria['categoria'];
            $url = $url;
            $url = $this->slug($url);

            $cat = $this->NoticiaCategoria->find('first', array('conditions' => array('NoticiaCategoria.id_old' => $categoria['id'])));
            if(!empty($cat))
            {
                $data[$i]['NoticiaCategoria']['id'] = $cat['NoticiaCategoria']['id'];
            } else {
                $data[$i]['NoticiaCategoria']['id'] = null;
            }
            $data[$i]['NoticiaCategoria']['id_old'] = $categoria['id'];
            $data[$i]['NoticiaCategoria']['status'] = true;
            $data[$i]['NoticiaCategoria']['url_seo'] = $url;

            $data[$i]['NoticiaCategoriaTraducao'][0]['NoticiaCategoriaTraducao']['padrao'] = true;
            $data[$i]['NoticiaCategoriaTraducao'][0]['NoticiaCategoriaTraducao']['noticia_categoria_id'] = $this->NoticiaCategoria->id;
            $data[$i]['NoticiaCategoriaTraducao'][0]['NoticiaCategoriaTraducao']['idioma_id'] = 1;
            $data[$i]['NoticiaCategoriaTraducao'][0]['NoticiaCategoriaTraducao']['idioma_slug'] = 'pt';
            $data[$i]['NoticiaCategoriaTraducao'][0]['NoticiaCategoriaTraducao']['nome'] = $categoria['categoria'];

            $this->NoticiaCategoria->saveAll($data[$i]);
        }

        debug($data);die;
    }

    public function publicacoes()
    {
        App::import('Model','FdNoticias.Noticia');
        App::import('Model','FdNoticias.NoticiaTraducao');
        App::import('Model','FdNoticias.NoticiaCategoria');

        $db = $this->connect();

        $publicacoes = $db->query('SELECT * FROM noticia WHERE noticia_tipo = "publicacao" AND noticia_status = 1');

        $this->Noticia = new Noticia;
        $this->NoticiaTraducao = new NoticiaTraducao;
        $this->NoticiaCategoria = new NoticiaCategoria;

        $this->Noticia->create();
        foreach($publicacoes as $i => $publicacao) {
            $url = $publicacao['noticia_titulo_br'];
            $url = utf8_decode($url);
            $url = $this->slug($url);

            $pub = $this->Noticia->find('first', array('conditions' => array('Noticia.id_old' => $publicacao['noticia_codigo'])));
            if(!empty($pub))
            {
                $data[$i]['Noticia']['id'] = $publicacao['noticia_codigo'];
            } else {
                $data[$i]['Noticia']['id'] = null;
            }

            $cat = $this->NoticiaCategoria->find('first', array('conditions' => array('NoticiaCategoria.id_old' => $publicacao['cod_categoria'])));
            $data[$i]['Noticia']['noticia_categoria_id'] = $cat['NoticiaCategoria']['id'];
            $data[$i]['Noticia']['status'] = true;
            $data[$i]['Noticia']['publicacao'] = true;
            $data[$i]['Noticia']['url_seo'] = $url;

            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['padrao'] = true;
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['noticia_id'] = $this->Noticia->id;
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['idioma_id'] = 1;
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['idioma_slug'] = 'pt';

            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['titulo']             = utf8_decode($publicacao['noticia_titulo_br']);
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['descricao_resumida'] = html_entity_decode(html_entity_decode(utf8_decode($publicacao['noticia_chamada_br'])));
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['conteudo']           = html_entity_decode(html_entity_decode(utf8_decode($publicacao['noticia_conteudo_br'])));

            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['meta_description'] = '';
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['meta_tags'] = '';
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['meta_title'] = '';

            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['padrao'] = '';
            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['noticia_id'] = $this->Noticia->id;
            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['idioma_id'] = 2;
            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['idioma_slug'] = 'en';

            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['titulo'] = utf8_decode($publicacao['noticia_titulo_us']);
            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['descricao_resumida'] = html_entity_decode(html_entity_decode(utf8_decode($publicacao['noticia_chamada_us'])));
            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['conteudo'] = html_entity_decode(html_entity_decode(utf8_decode($publicacao['noticia_conteudo_us'])));

            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['meta_description'] = '';
            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['meta_tags'] = '';
            $data[$i]['NoticiaTraducao'][1]['NoticiaTraducao']['meta_title'] = '';

            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['padrao'] = '';
            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['noticia_id'] = $this->Noticia->id;
            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['idioma_id'] = 3;
            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['idioma_slug'] = 'es';


            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['titulo'] = utf8_decode($publicacao['noticia_titulo_es']);
            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['descricao_resumida'] = html_entity_decode(html_entity_decode(utf8_decode($publicacao['noticia_chamada_es'])));
            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['conteudo'] = html_entity_decode(html_entity_decode(utf8_decode($publicacao['noticia_conteudo_es'])));


            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['meta_description'] = '';
            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['meta_tags'] = '';
            $data[$i]['NoticiaTraducao'][2]['NoticiaTraducao']['meta_title'] = '';

            $this->Noticia->saveAll($data[$i]);
        }

        die('ok');

    }

	public function noticias()
	{
		App::import('Model','FdNoticias.Noticia');
        App::import('Model','FdNoticias.NoticiaTraducao');
        App::import('Model','FdNoticias.NoticiaCategoria');

		$db = $this->connect();

		$noticias = $db->query('SELECT * FROM fd_noticias');

		$this->Noticia = new Noticia;
        $this->NoticiaTraducao = new NoticiaTraducao;
        $this->NoticiaCategoria = new NoticiaCategoria;

		$this->Noticia->create();
        foreach($noticias as $i => $noticia) {
            $url = $noticia['titulo'];
            $url = $this->slug($url);

            //valida se a noticia já está cadastrada ou não, para dar insert/update
            $not = $this->Noticia->find('first', array('conditions' => array('Noticia.id_old' => $noticia['id'])));
            if(!empty($not)){
            	$data[$i]['Noticia']['id'] = $not['Noticia']['id'];
            }else{
            	$data[$i]['Noticia']['id'] = null;
            }

            $data[$i]['Noticia']['id_old'] = $noticia['id'];
            $cat = $this->NoticiaCategoria->find('first', array('conditions' => array('NoticiaCategoria.id_old' => $noticia['categoria'])));
            $data[$i]['Noticia']['noticia_categoria_id'] = $cat['NoticiaCategoria']['id'];
            $data[$i]['Noticia']['status'] = $noticia['status'];
            $data[$i]['Noticia']['publicacao'] = 0;
            $data[$i]['Noticia']['url_seo'] = $url;
            $data[$i]['Noticia']['data'] = $noticia['data'] . ' 00:00:00';

            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['padrao'] = true;
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['noticia_id'] = $this->Noticia->id;
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['idioma_id'] = 1;
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['idioma_slug'] = 'pt';

            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['titulo'] 			 = $noticia['titulo'];
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['descricao_resumida'] = html_entity_decode(html_entity_decode($noticia['resumo']));
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['conteudo'] 			 = html_entity_decode(html_entity_decode($noticia['conteudo']));

            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['meta_description'] = '';
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['meta_tags'] = '';
            $data[$i]['NoticiaTraducao'][0]['NoticiaTraducao']['meta_title'] = '';

            $this->Noticia->saveAll($data[$i]);

            // debug($data);die;

            // die;
        }

        // $this->Noticia = new Noticia;
        // $this->NoticiaTraducao = new NoticiaTraducao;

        // $this->Noticia->create();
        // if(!$this->Noticia->saveAll($data['Noticia'])) {
        //     debug($this->Noticia->invalidFields());
        // } else {

        // }

		debug($data);die;
	}

    public function recuperacoes()
    {
        App::import('Model','FdRecuperacoes.Recuperacao');
        $this->Recuperacao = new Recuperacao();

        $db = $this->connect();

        $recuperacoes = $db->query('SELECT * FROM recuperacao');

        foreach($recuperacoes as $i => $recuperacao) {
            $this->Recuperacao->create();

            $rec = $this->Recuperacao->find('first', array('conditions' => array('Recuperacao.id_old' => $recuperacao['recuperacao_codigo'])));
            if(!empty($rec)){
                $data[$i]['Recuperacao']['id'] = $rec['Recuperacao']['id'];
            } else {
                $data[$i]['Recuperacao']['id'] = null;
            }

            $data[$i]['Recuperacao']['tipo']        = $recuperacao['recuperacao_tipo'];
            $data[$i]['Recuperacao']['titulo']      = utf8_decode($recuperacao['recuperacao_titulo']);
            $data[$i]['Recuperacao']['numero']      = $recuperacao['recuperacao_numero'];
            $data[$i]['Recuperacao']['responsavel'] = utf8_decode($recuperacao['recuperacao_responsavel']);
            $data[$i]['Recuperacao']['comarca']     = utf8_decode($recuperacao['recuperacao_comarca']);
            $data[$i]['Recuperacao']['vara']        = utf8_decode($recuperacao['recuperacao_vara']);
            $data[$i]['Recuperacao']['endereco']    = utf8_decode($recuperacao['recuperacao_endereco']);
            $data[$i]['Recuperacao']['telefone']    = $recuperacao['recuperacao_telefone'];
            $data[$i]['Recuperacao']['link']        = $recuperacao['recuperacao_link'];
            $data[$i]['Recuperacao']['status']      = $recuperacao['recuperacao_status'];
            $data[$i]['Recuperacao']['created']     = $recuperacao['recuperacao_cadastro'];
            $data[$i]['Recuperacao']['id_old']      = $recuperacao['recuperacao_codigo'];

            $this->Recuperacao->saveAll($data[$i]);
        }

        debug($data);die;
    }

    public function recuperacao_arquivos(){
        App::import('Model','FdRecuperacoes.Recuperacao');
        $this->Recuperacao = new Recuperacao();

        App::import('Model','FdRecuperacoes.RecuperacaoArquivo');
        $this->RecuperacaoArquivo = new RecuperacaoArquivo();

        $recuperacoes = $this->Recuperacao->find('all');

        $data = array();

        $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'importacoes' . DS . 'arquivos' . DS;

        foreach ($recuperacoes as $key => $recuperacao) {
             $db = $this->connect();
             $files = $db->query('SELECT * FROM sysfile WHERE file_vinculo = "recuperacao,'.$recuperacao['Recuperacao']['id_old'].'"');

             foreach ($files as $y => $file) {

                $filesize = null;
                $ext = null;

                if(file_exists($dir.$file['file_arquivo'])){
                    $pathinfo = pathinfo($dir.$file['file_arquivo']);
                    $filesize = filesize($dir.$file['file_arquivo']);
                    $ext = strtolower($pathinfo["extension"]);
                }

                switch ($ext) {
                    case "pdf":
                    $ctype = "application/pdf";
                    break;

                    case "txt":
                    $ctype = "text/plain";
                    break;

                    case "mp3":
                    $ctype = "audio/mpeg";
                    break;
                }

                $arquivo = array_reverse(explode('.', $file['file_filename']));
                $arquivo_nome = substr($file['file_filename'], 0, strlen($file['file_filename'])-4);
                $arquivo_nome = str_replace('.', '-', $arquivo_nome);
                $arquivo_ext = $arquivo[0];



                $data[$y]['RecuperacaoArquivo']['id']           = null;
                $data[$y]['RecuperacaoArquivo']['recuracao_id'] = $recuperacao['Recuperacao']['id'];
                $data[$y]['RecuperacaoArquivo']['titulo']       = utf8_decode($file['file_titulo']);
                $data[$y]['RecuperacaoArquivo']['descricao']    = utf8_decode($file['file_descricao']);
                $data[$y]['RecuperacaoArquivo']['referencia']   = $file['file_referencia'];
                $data[$y]['RecuperacaoArquivo']['ordem']        = $file['file_ordem'];
                // $data[$y]['RecuperacaoArquivo']['thumb']        = $file['file_titulo'];
                $data[$y]['RecuperacaoArquivo']['file']         = strtolower(Inflector::slug(utf8_decode($arquivo_nome), '-')).'.'.$arquivo_ext;
                // $data[$y]['RecuperacaoArquivo']['file_dir']     = $file['file_titulo'];
                $data[$y]['RecuperacaoArquivo']['file_type']    = $ctype;
                $data[$y]['RecuperacaoArquivo']['file_size']    = $filesize;
                $data[$y]['RecuperacaoArquivo']['downloads']    = $file['file_downloads'];
                $data[$y]['RecuperacaoArquivo']['status']       = ($file['file_status'] == 2) ? 0 : 1;
                $data[$y]['RecuperacaoArquivo']['created']      = $file['file_data'];

                if($this->RecuperacaoArquivo->save($data[$y])){
                    $dt[$y]['RecuperacaoArquivo']['id']       = $this->RecuperacaoArquivo->id;
                    $dt[$y]['RecuperacaoArquivo']['file_dir'] = $this->RecuperacaoArquivo->id;
                    $this->RecuperacaoArquivo->save($dt);

                    if(file_exists($dir.$file['file_arquivo'])){
                        $root = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'recuperacao_arquivo' . DS;
                        $novo_caminho = $root.$this->RecuperacaoArquivo->id.DS;

                        if(!is_dir($novo_caminho)){
                            mkdir($novo_caminho);
                        }

                        // $novo_caminho .= DS;

                        // die($novo_caminho);

                        if(is_dir($novo_caminho)){
                            copy($dir.$file['file_arquivo'], $novo_caminho.strtolower(Inflector::slug(utf8_decode($arquivo_nome), '-')).'.'.$arquivo_ext);
                        }
                    }
                }
             }
        }

        die;
    }

}
<?php

class ContatoController extends AppController {

	public $uses = array();

	public function index()
	{
		App::uses('CakeEmail', 'Network/Email');
		$this->Email = new CakeEmail('smtp');

		if($this->request->is('post'))
		{
			$this->layout = 'ajax';
			if($this->Contato->set($this->data))
			{
				$mensagem = 'Nome: ' . $this->request->data['Contato']['nome'] . "\r\n";
				$mensagem .= 'E-mail: ' . $this->request->data['Contato']['email'] . "\r\n";
				$mensagem .= 'Mensagem:' . "\r\n";
				$mensagem .= $this->request->data['Contato']['mensagem'];

				$this->Email->from(array('ffbertoni@tj.rs.gov.br' => 'Site ITEC!'));
	        	$this->Email->to('ffbertoni@tj.rs.gov.br');
	        	$this->Email->replyTo('ffbertoni@tj.rs.gov.br');
	        	$this->Email->subject('Contato pelo site');
	        	$enviar = $this->Email->send($mensagem);

	        	if($enviar)
	        	{
	        		$retorno = array('tipo' => 'success', 'msg' => 'Mensagem enviada com sucesso');
	        		unset($this->request->data['Contato']);
	        	} else {
	        		$retorno = array('tipo' => 'error', 'msg' => 'Não foi possível enviar a sua mensagem, por favor, tente novamente');
	        	}
	        	$this->set(compact('retorno'));
			}
		} else {
			$this->redirect('/');
		}
	}
}
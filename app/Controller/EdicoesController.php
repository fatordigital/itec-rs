<?php

App::uses('AppController', 'Controller');

class EdicoesController extends AppController {

    private $Revista;
    private $Edicao;
    private $Language;

    public function __construct(CakeRequest $cakeRequest, CakeResponse $cakeResponse) {
        parent::__construct($cakeRequest, $cakeResponse);
        $this->Revista = ClassRegistry::init('FdEdicoes.Revista');
        $this->Edicao = ClassRegistry::init('FdEdicoes.Edicao');
    }

    private function idioma() {
        if (isset($this->params->params['named']['idioma'])) {
            $this->set('idioma', $this->request->params['named']['idioma']);
            $this->Language = $this->request->params['named']['idioma'];
        } elseif (isset($this->params->params['idioma'])) {
            $this->set('idioma', $this->request->params['idioma']);
            $this->Language = $this->request->params['idioma'];
        } elseif (count($this->params->params['named']) > 1 && isset($this->params->params['named'][0]) && strlen($this->params->params['named'][0]) == 2) {
            $this->set('idioma', $this->params->params['named'][0]);
            $this->Language = $this->params->params['named'][0];
        } elseif (count($this->params->params['pass']) > 1 && isset($this->params->params['pass'][0]) && strlen($this->params->params['pass'][0]) == 2) {
            $this->set('idioma', $this->params->params['pass'][0]);
            $this->Language = $this->params->params['pass'][0];
        } else {
            $this->set('idioma', 'pt');
            $this->Language = 'pt';
        }
    }

    public function index() {
        $this->idioma();
        $ultimo_numero = $this->Edicao->find('first', array('recursive' => -1, 'order' => 'Edicao.id DESC'));
        $this->set(compact('ultimo_numero'));
        $titulo = '';

        if (isset($this->request->query['autor'])) {
            $titulo = ($this->Language == 'pt') ? 'Resultados' : 'Results';
            $procurar_autor = $this->Edicao->find('all', array(
                'conditions' => array('Edicao.autores LIKE' => '%' . $this->request->query['autor'] . '%'),
                'order' => array('Edicao.id DESC')
            ));
            foreach ($procurar_autor as $key => $value) {
                $titulos = array();
                if (!empty($value['Edicao']['titulo_pt']) && trim(strip_tags($value['Edicao']['titulo_pt'])) != '') {
                    $titulos['pt'] = trim($value['Edicao']['titulo_pt']);
                }
                if (!empty($value['Edicao']['titulo_en']) && trim(strip_tags($value['Edicao']['titulo_en'])) != '') {
                    $titulos['en'] = trim($value['Edicao']['titulo_en']);
                }
                if (!empty($value['Edicao']['titulo_es']) && trim(strip_tags($value['Edicao']['titulo_es'])) != '') {
                    $titulos['es'] = trim($value['Edicao']['titulo_es']);
                }
                if (!empty($value['Edicao']['titulo_it']) && trim(strip_tags($value['Edicao']['titulo_it'])) != '') {
                    $titulos['it'] = trim($value['Edicao']['titulo_it']);
                }

                if (!empty($value['Edicao']['titulo_' . $value['Edicao']['idioma_original']])) {
                    $procurar_autor[$key]['Edicao']['titulo'] = $value['Edicao']['titulo_' . $value['Edicao']['idioma_original']];
                } else {
                    $procurar_autor[$key]['Edicao']['titulo'] = reset($titulos);
                }
            }
            $this->request->data['autor'] = $this->request->query['autor'];
            $this->set(compact('procurar_autor'));
        }

        if (isset($this->request->query['titulo'])) {
            $titulo = ($this->Language == 'pt') ? 'Resultados' : 'Results';
            $procurar_titulo = $this->Edicao->find('all', array(
                'conditions' => array(
                    'OR' => array(
                        'Edicao.titulo_pt LIKE' => '%' . $this->request->query['titulo'] . '%',
                        'Edicao.titulo_en LIKE' => '%' . $this->request->query['titulo'] . '%',
                        'Edicao.titulo_it LIKE' => '%' . $this->request->query['titulo'] . '%',
                        'Edicao.titulo_es LIKE' => '%' . $this->request->query['titulo'] . '%')),
                'order' => array('Edicao.id DESC')
            ));
            foreach ($procurar_titulo as $key => $value) {
                $titulos = array();
                if (!empty($value['Edicao']['titulo_pt']) && trim(strip_tags($value['Edicao']['titulo_pt'])) != '') {
                    $titulos['pt'] = trim($value['Edicao']['titulo_pt']);
                }
                if (!empty($value['Edicao']['titulo_en']) && trim(strip_tags($value['Edicao']['titulo_en'])) != '') {
                    $titulos['en'] = trim($value['Edicao']['titulo_en']);
                }
                if (!empty($value['Edicao']['titulo_es']) && trim(strip_tags($value['Edicao']['titulo_es'])) != '') {
                    $titulos['es'] = trim($value['Edicao']['titulo_es']);
                }
                if (!empty($value['Edicao']['titulo_it']) && trim(strip_tags($value['Edicao']['titulo_it'])) != '') {
                    $titulos['it'] = trim($value['Edicao']['titulo_it']);
                }

                if (!empty($value['Edicao']['titulo_' . $value['Edicao']['idioma_original']])) {
                    $procurar_titulo[$key]['Edicao']['titulo'] = $value['Edicao']['titulo_' . $value['Edicao']['idioma_original']];
                } else {
                    $procurar_titulo[$key]['Edicao']['titulo'] = reset($titulos);
                }
            }
            $this->request->data['titulo'] = $this->request->query['titulo'];
            $this->set(compact('procurar_titulo'));
        }

        if (isset($this->request->query['palavra-chave'])) {
            $titulo = ($this->Language == 'pt') ? 'Resultados' : 'Results';
            $procurar_palavra_chave = $this->Edicao->find('all', array(
                'conditions' => array(
                    'OR' => array(
                        'Edicao.palavra_chave_pt LIKE' => '%' . $this->request->query['palavra-chave'] . '%',
                        'Edicao.palavra_chave_en LIKE' => '%' . $this->request->query['palavra-chave'] . '%',
                        'Edicao.palavra_chave_it LIKE' => '%' . $this->request->query['palavra-chave'] . '%',
                        'Edicao.palavra_chave_es LIKE' => '%' . $this->request->query['palavra-chave'] . '%')),
                'order' => array('Edicao.id DESC')
            ));
            foreach ($procurar_palavra_chave as $key => $value) {
                $titulos = array();
                if (!empty($value['Edicao']['titulo_pt']) && trim(strip_tags($value['Edicao']['titulo_pt'])) != '') {
                    $titulos['pt'] = trim($value['Edicao']['titulo_pt']);
                }
                if (!empty($value['Edicao']['titulo_en']) && trim(strip_tags($value['Edicao']['titulo_en'])) != '') {
                    $titulos['en'] = trim($value['Edicao']['titulo_en']);
                }
                if (!empty($value['Edicao']['titulo_es']) && trim(strip_tags($value['Edicao']['titulo_es'])) != '') {
                    $titulos['es'] = trim($value['Edicao']['titulo_es']);
                }
                if (!empty($value['Edicao']['titulo_it']) && trim(strip_tags($value['Edicao']['titulo_it'])) != '') {
                    $titulos['it'] = trim($value['Edicao']['titulo_it']);
                }

                if (!empty($value['Edicao']['titulo_' . $value['Edicao']['idioma_original']])) {
                    $procurar_palavra_chave[$key]['Edicao']['titulo'] = $value['Edicao']['titulo_' . $value['Edicao']['idioma_original']];
                } else {
                    $procurar_palavra_chave[$key]['Edicao']['titulo'] = reset($titulos);
                }
            }
            $this->request->data['palavra-chave'] = $this->request->query['palavra-chave'];
            $this->set(compact('procurar_palavra_chave'));
        }

        if (isset($this->request->query['resumo'])) {
            $titulo = ($this->Language == 'pt') ? 'Resultados' : 'Results';
            $procurar_resumo = $this->Edicao->find('all', array(
                'conditions' => array(
                    'OR' => array(
                        'Edicao.texto_pt LIKE' => '%' . $this->request->query['resumo'] . '%',
                        'Edicao.texto_en LIKE' => '%' . $this->request->query['resumo'] . '%',
                        'Edicao.texto_it LIKE' => '%' . $this->request->query['resumo'] . '%',
                        'Edicao.texto_es LIKE' => '%' . $this->request->query['resumo'] . '%'
                    )),
                'order' => array('Edicao.id DESC')
            ));

            foreach ($procurar_resumo as $key => $value) {
                $titulos = array();
                if (!empty($value['Edicao']['titulo_pt']) && trim(strip_tags($value['Edicao']['titulo_pt'])) != '') {
                    $titulos['pt'] = trim($value['Edicao']['titulo_pt']);
                }
                if (!empty($value['Edicao']['titulo_en']) && trim(strip_tags($value['Edicao']['titulo_en'])) != '') {
                    $titulos['en'] = trim($value['Edicao']['titulo_en']);
                }
                if (!empty($value['Edicao']['titulo_es']) && trim(strip_tags($value['Edicao']['titulo_es'])) != '') {
                    $titulos['es'] = trim($value['Edicao']['titulo_es']);
                }
                if (!empty($value['Edicao']['titulo_it']) && trim(strip_tags($value['Edicao']['titulo_it'])) != '') {
                    $titulos['it'] = trim($value['Edicao']['titulo_it']);
                }

                if (!empty($value['Edicao']['titulo_' . $value['Edicao']['idioma_original']])) {
                    $procurar_resumo[$key]['Edicao']['titulo'] = $value['Edicao']['titulo_' . $value['Edicao']['idioma_original']];
                } else {
                    $procurar_resumo[$key]['Edicao']['titulo'] = reset($titulos);
                }
            }

            $this->request->data['resumo'] = $this->request->query['resumo'];
            $this->set(compact('procurar_resumo'));
        }

        $revista = $this->Revista->find('first', array('conditions' => array('Revista.id' => 1)));
        $atributos = array();

        if (isset($this->params['named']['escopo'])) {
            $titulo = ($this->Language == 'pt') ? 'Escopo e periodicidade' : 'Scope';
        } elseif (isset($this->params['named']['equipe'])) {
            $titulo = ($this->Language == 'pt') ? 'Equipe' : 'Editorial Team';
        } elseif (isset($this->params['named']['diretrizes'])) {
            $titulo = ($this->Language == 'pt') ? 'Diretrizes &eacute;ticas' : 'Publication Ethics and Publication Malpractice Statement';
        } elseif (isset($this->params['named']['regras'])) {
            $titulo = ($this->Language == 'pt') ? 'Regras para publi&ccedil;&atilde;o' : 'Instructions to Authors';
        } elseif (isset($this->params['named']['numero-anterior'])) {
            $titulo = ($this->Language == 'pt') ? 'N&uacute;mero: ' . $this->params['named']['numero-anterior'] : 'Number ' . $this->params['named']['numero-anterior'];
        } elseif (isset($this->params['named']['numero-atual'])) {
            $titulo = ($this->Language == 'pt') ? 'N&uacute;mero: ' . $this->params['named']['numero-atual'] : 'Number ' . $this->params['named']['numero-atual'];
        } elseif ($titulo == '') {
            $titulo = ($this->Language == 'pt') ? 'Apresenta&ccedil;&atilde;o' : 'Presentation';
        }

        foreach ($revista['Edicao'] as $item) {
            if (isset($this->params['named']['titulo'])) {
                if ($item['slug'] == $this->params['named']['titulo'] || $item['slug'] == $this->params['named']['titulo']) {
                    $titulos = array();
                    if (!empty($item['titulo_pt']) && trim(strip_tags($item['titulo_pt'])) != '') {
                        $titulos['pt'] = trim($item['titulo_pt']);
                    }
                    if (!empty($item['titulo_en']) && trim(strip_tags($item['titulo_en'])) != '') {
                        $titulos['en'] = trim($item['titulo_en']);
                    }
                    if (!empty($item['titulo_es']) && trim(strip_tags($item['titulo_es'])) != '') {
                        $titulos['es'] = trim($item['titulo_es']);
                    }
                    if (!empty($item['titulo_it']) && trim(strip_tags($item['titulo_it'])) != '') {
                        $titulos['it'] = trim($item['titulo_it']);
                    }

                    $resumos = array();
                    $tit_resumos = array();
                    if (!empty($item['texto_pt']) && trim(strip_tags($item['texto_pt'])) != '') {
                        $tit_resumos['pt'] = 'Resumo';
                        $resumos['pt'] = trim($item['texto_pt']);
                    }
                    if (!empty($item['texto_en']) && trim(strip_tags($item['texto_en'])) != '') {
                        $tit_resumos['en'] = 'Abstract';
                        $resumos['en'] = trim($item['texto_en']);
                    }
                    if (!empty($item['texto_es']) && trim(strip_tags($item['texto_es'])) != '') {
                        $tit_resumos['es'] = 'Resumen';
                        $resumos['es'] = trim($item['texto_es']);
                    }
                    if (!empty($item['texto_it']) && trim(strip_tags($item['texto_it'])) != '') {
                        $tit_resumos['it'] = 'Riassunto';
                        $resumos['it'] = trim($item['texto_it']);
                    }

                    $tit_keywords = array();
                    $keywords = array();
                    if (!empty($item['palavra_chave_pt']) && trim(strip_tags($item['palavra_chave_pt'])) != '') {
                        $tit_keywords['pt'] = 'Palavras-chave';
                        $keywords['pt'] = explode(';', (substr($item['palavra_chave_pt'], -1) == '.') ? str_replace('.', '', $item['palavra_chave_pt']) : $item['palavra_chave_pt']);
                    }
                    if (!empty($item['palavra_chave_en']) && trim(strip_tags($item['palavra_chave_en'])) != '') {
                        $tit_keywords['en'] = 'Keywords';
                        $keywords['en'] = explode(';', (substr($item['palavra_chave_en'], -1) == '.') ? str_replace('.', '', $item['palavra_chave_en']) : $item['palavra_chave_en']);
                    }
                    if (!empty($item['palavra_chave_es']) && trim(strip_tags($item['palavra_chave_es'])) != '') {
                        $tit_keywords['es'] = 'Palabras clave';
                        $keywords['es'] = explode(';', (substr($item['palavra_chave_es'], -1) == '.') ? str_replace('.', '', $item['palavra_chave_es']) : $item['palavra_chave_es']);
                    }
                    if (!empty($item['palavra_chave_it']) && trim(strip_tags($item['palavra_chave_it'])) != '') {
                        $tit_keywords['it'] = 'Parole chiave';
                        $keywords['it'] = explode(';', (substr($item['palavra_chave_it'], -1) == '.') ? str_replace('.', '', $item['palavra_chave_it']) : $item['palavra_chave_it']);
                    }


                    //Titulo
                    $titulo = '';
                    if (!empty($item['titulo_' . $item['idioma_original']])) {
                        $titulo = $item['titulo_' . $item['idioma_original']] . '<br />';
                        $item['titulo'] = $item['titulo_' . $item['idioma_original']];
                        unset($titulos[$item['idioma_original']]);
                    } else {
                        $titulo = reset($titulos) . '<br />';
                        $idioma_titulo = key($titulos);
                        $item['titulo'] = $titulo;
                        unset($titulos[$idioma_titulo]);
                    }

                    if (count($titulos) > 0) {
                        $subtitulo = reset($titulos) . '<br /><br />';
                        $idioma_subtitulo = key($titulos);
                    } else {
                        $subtitulo = '';
                    }


                    $autores = '';
                    $authors = null;
                    if (!empty($item['autores'])) {
                        $arr = explode(';', $item['autores']);
                        $authors = $arr;
                        $ls_autors = count($arr) - 1;
                        $ct_autors = 0;
                        foreach ($arr as $kaut => $autor) {
                            $autores .= '<a class="text-danger" href="' . Router::url($this->Language . '/rec?autor=' . $autor, true) . ' ">';
                            $autores .= $autor;
                            $autores .= '</a>';
                            $ct_autors++;
                            if ($ct_autors == $ls_autors) {
                                $autores .= ' e ';
                            } elseif ($ls_autors > 0) {
                                $autores .= ', ';
                            }
                        }
                    }


                    $authorsCitation = explode(';', (!empty($item['autores_citation']) ? $item['autores_citation'] : $item['autores']));

                    $resumo_1 = '';
                    if (!empty($item['texto_' . $item['idioma_original']])) {
                        $resumo_1 = '<p style="text-align: justify"><strong>' . $tit_resumos[$item['idioma_original']] . '</strong>: ' . strip_tags($item['texto_' . $item['idioma_original']]) . '</p>';
                        unset($resumos[$item['idioma_original']]);
                    } else {
                        if (count($resumos) > 0) {
                            $idioma_resumo_1 = key($resumos);
                            $resumo_1 = '<p style="text-align: justify"><strong>' . $tit_resumos[$idioma_resumo_1] . '</strong>: ' . strip_tags(reset($resumos)) . '</p>';
                            unset($resumos[$idioma_resumo_1], $tit_resumos[$idioma_resumo_1]);
                        }
                    }
                    $resumo_2 = '';
                    if (count($resumos) > 0) {
                        $idioma_resumo_2 = key($resumos);
                        $resumo_2 = '<p style="text-align: justify"><strong>' . $tit_resumos[$idioma_resumo_2] . '</strong>: ' . strip_tags(reset($resumos)) . '</p>';
                    }

                    $keyword_1 = '';
                    $keyWords_p1 = array();
                    if (!empty($item['palavra_chave_' . $item['idioma_original']])) {
                        $keyword_1 .= '<strong>' . $tit_keywords[$item['idioma_original']] . '</strong>: ';
                        $keyWords_p1 = $keywords[$item['idioma_original']];
                        $ctk = count($keywords[$item['idioma_original']]) - 1;
                        $ct = 0;
                        foreach ($keywords[$item['idioma_original']] as $kkey => $words) {
                            $keyword_1 .= '<a class="text-danger" href="' . Router::url($this->Language . '/rec?palavra-chave=' . strip_tags($words), true) . ' ">';
                            $keyword_1 .= trim(strip_tags($words));
                            $keyword_1 .= '</a>';
                            $keyword_1 .= ($ct == $ctk) ? '. ' : '; ';
                            $ct++;
                        }
                        $keyword_1 .= '<br /><br />';
                        unset($keywords[$item['idioma_original']]);
                    } else {
                        if (count($keywords) > 0) {
                            $idioma_keywords_1 = key($keywords);
                            $keyword_1 = '<strong>' . $tit_keywords[$idioma_keywords_1] . '</strong>: ';
                            $keyWords_p1 = $keywords[$keywords[$idioma_keywords_1]];
                            $ctk = count($keywords[$idioma_keywords_1]) - 1;
                            $ct = 0;

                            foreach ($keywords[$idioma_keywords_1] as $kkey => $words) {
                                $keyword_1 .= '<a class="text-danger" href="' . Router::url($this->Language . '/rec?palavra-chave=' . strip_tags($words), true) . ' ">';
                                $keyword_1 .= trim(strip_tags($words));
                                $keyword_1 .= '</a>;';
                                $keyword_1 .= ($ct == $ctk) ? '. ' : '; ';
                                $ct++;
                            }
                            $keyword_1 .= '<br /><br />';
                            unset($keywords[$idioma_keywords_1]);
                        }
                    }
                    $keyword_2 = '';
                    $keyWords_p2 = array();
                    if (count($keywords) > 0) {
                        $idioma_keywords_2 = key($keywords);
                        $keyword_2 = '<strong>' . $tit_keywords[$idioma_keywords_2] . '</strong>: ';
                        $keyWords_p2 = $keywords[$idioma_keywords_2];
                        $ctk = count($keywords[$idioma_keywords_2]) - 1;
                        $ct = 0;

                        foreach ($keywords[$idioma_keywords_2] as $kkey => $words) {
                            $keyword_2 .= '<a class="text-danger" href="' . Router::url($this->Language . '/rec?palavra-chave=' . strip_tags($words), true) . ' ">';
                            $keyword_2 .= trim(strip_tags($words));
                            $keyword_2 .= '</a>';
                            $keyword_2 .= ($ct == $ctk) ? '. ' : '; ';
                            $ct++;
                        }
                        $keyword_2 .= '<br /><br />';
                    }

                    $apresentacao = '';
                    $apresentacao .= $subtitulo;
                    $apresentacao .= ($autores != '') ? $autores . '<br /><br />' : '';
                    $apresentacao .= $resumo_1;
                    $apresentacao .= $keyword_1;
                    $apresentacao .= $resumo_2;
                    $apresentacao .= $keyword_2;

                    $this->set(compact('authors'));
                    $this->set(compact('authorsCitation'));
                    $this->set(compact('keyWords_p1'));
                    $this->set(compact('keyWords_p2'));

                    $apresentacao .= 'v.<strong>' . $item['volume'] . '</strong>, n.<strong>' . $item['numero'] . '</strong>, p.<strong>' . $item['paginacao'] . '</strong>';
                    $revista['Revista']['apresentacao_' . $this->Language] = $apresentacao;
                    $this->set('current_article', $item);
                }
            }

            $atributos[$item['ano']][] = array(
                'titulo' => $titulo,
                'slug' => $item['slug'],
                'numero' => $item['numero'],
                'volume' => $item['volume'],
                'paginacao' => $item['paginacao'],
                'slug_paginacao' => $item['slug_paginacao']
            );
        }

        if (isset($this->params['named']['numero-atual'])) {
            $numero_atual = $this->Edicao->find('all', array(
                'recursive' => -1,
                'conditions' => array('Edicao.numero' => $this->params['named']['numero-atual'])
            ));
            $this->set(compact('numero_atual'));
        }

        if (isset($this->params['named']['numero-anterior'])) {
            $numero_anterior = $this->Edicao->find('all', array(
                'recursive' => -1,
                'conditions' => array('Edicao.numero' => $this->params['named']['numero-anterior'])
            ));
            $this->set(compact('numero_anterior'));
        }

        krsort($atributos);

        if (!isset($revista['Revista']['apresentacao_' . $this->Language])) {
            $idioma = $this->Language;
            $revista['Revista']['apresentacao_' . $this->Language] = $revista['Revista']['apresentacao_' . $idioma];
        }

        $this->set(compact('revista', 'atributos', 'titulo'));
    }

}

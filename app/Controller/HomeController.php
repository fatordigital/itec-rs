<?php

class HomeController extends AppController {

	public $uses = array('FdNoticias.Noticia', 'FdLivros.Livro', 'FdGalerias.Video', 'FdGalerias.Foto');

	public function index()
	{
		$noticiasFotos = $this->Noticia->find('all', array('conditions' => array('Noticia.status' => true, 'Noticia.imagem IS NOT NULL'), 'order' => array('Noticia.data' => 'DESC', 'Noticia.id' => 'DESC'), 'limit' => 2));
		$noticias = $this->Noticia->find('all', array('conditions' => array('Noticia.status' => true, 'Noticia.imagem IS NULL'), 'order' => array('Noticia.data' => 'DESC', 'Noticia.id' => 'DESC'), 'limit' => 4));

		$video = $this->Video->find('first', array('conditions' => array('Video.status' => true), 'order' => array('Video.created' => 'DESC')));
		$ultimaFoto = $this->Foto->find('first', array('conditions' => array('Foto.status' => true), 'order' => array('Foto.created' => 'DESC')));
		$fotos = $this->Foto->find('all', array('conditions' => array('Foto.status' => true, 'Foto.id !=' => $ultimaFoto['Foto']['id']), 'order' => array('Foto.created' => 'DESC'), 'limit' => 2));

		$livros = $this->Livro->find('all', array('conditions' => array('Livro.status' => true), 'order' => array('Livro.created' => 'DESC'), 'limit' => 4));

		$this->set(compact('noticiasFotos','noticias','video','ultimaFoto','fotos','livros'));

	}

}
<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Acl',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'FdUsuarios.Usuario',
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'senha',
                    ),
                    'scope' => array('Usuario.status' => 1),
                ),
            ),
            'authorize' => array(
                'Actions' => array(
                    'actionPath' => 'controllers',
                    'userModel' => 'FdUsuarios.Usuario',
                ),
            ),
            'loginAction' => array(
                'plugin' => 'fd_usuarios',
                'controller' => 'fd_usuarios',
                'action' => 'login',
                'fatorcms' => false,
            ),
            'logoutRedirect' => array(
                'plugin' => 'fd_usuarios',
                'controller' => 'fd_usuarios',
                'action' => 'login',
                'fatorcms' => false,
            ),
            'loginRedirect' => array(
                'plugin' => 'fd_dashboard',
                'controller' => 'fd_dashboard',
                'action' => 'index',
                'prefix' => 'fatorcms',
                'fatorcms' => true,
            ),
        ),
        'Session',
        'Paginator',
        'RequestHandler',
        'ImportDb',
        'FilterResults.FilterResults' => array(
            'auto' => array(
                'paginate' => false,
                'explode' => true,
            ),
            'explode' => array(
                'character' => ' ',
                'concatenate' => 'AND',
            ),
        ),
    );

    public $helpers = array(
        'Session',
        'Time',
        'Texto',
        'FilterResults.FilterForm',
        'Youtube',
        'Html' => array('className' => 'MyHtml')
    );

    public function beforeFilter()
    {

        //$this->ImportDb->noticias();
        //die;

        $this->setConfiguracoes();

        $this->padrao();

        //$this->connectFacebook();

        if ((isset($this->params['prefix']) && ($this->params['prefix'] == 'fatorcms'))) {
            $this->layout = 'fatorcms';
            $this->Auth->deny();
        } else {
            $this->setIdioma();

//            $idiomas = $this->Idioma->find('all', array('recursive' => -1, 'fields' => array('Idioma.slug', 'Idioma.nome'), 'conditions' => array('Idioma.status' => true)));
//            $this->set(compact('idiomas'));

            $this->Auth->allow();
        }
    }

    public function padrao()
    {
        $socias = array(
            array(
                'nome'      => 'Telma Cecília Torrano',
                'oab'       => 'OAB/RS nº 49.030 e OAB/SP sob nº 284.888',
                'sinopse'   => 'Graduada em Ciências Jurídicas e Sociais pela Pontifícia Universidade Católica do RS em 1999. Especialista em Direito Civil pela Uniritter/RS, em Gestão Estratégica de Departamentos Jurídicos e Escritórios de Advocacia pela Unisinos/RS, em Administração Legal pela FGV São Paulo',
                'mais'      => ' MBA em Gestão Econômica e Estratégica de Negócios pela FGV/SP.<br>Experiência de mais de 11 anos em Gestão de escritórios de advocacia, especialmente na gestão de pessoas e processos organizacionais e no gerenciamento estratégico de carteiras de grande volume, atendendo nacionalmente  empresas de médio e grande porte  de diversos segmentos. Vice Presidente Jurídica da SOBRATT ( Sociedade Brasileira de teletrabalho e teleatividades). Indicada por 3  anos consecutivos pela revista Análise Editorial como uma das mais admiradas advogadas na área de Direito do Consumidor. Palestrante em diversos eventos ligados a Gestão Estratégica de Contencioso de Volume.<br>Atuação em diversos projetos , conduzidos em empresas dos mais variados segmentos, sempre ligados ao gerenciamento estratégico e a redução da base de processos ativos, desenvolvendo em conjunto com os Gestores de Departamentos Jurídicos , análises gerenciais personalizadas e customizadas.<br>Atua ativamente também em projetos ligados a campanhas de redução da provisão, treinamentos in company, dente outros. Parecerista  nas questões alusivas ao direito civil, contratual e consumeirista.',
                'foto'      => 'telma.png',
                'linkedin'  => 'https://www.linkedin.com/profile/view?id=49222306',
            ),
            array(
                'nome'      => 'Vanessa Guazzelli Braga',
                'oab'       => 'OAB/RS nº 46.853 e OAB/SP nº 284.889',
                'sinopse'   => 'Graduada em Ciências Jurídicas e Sociais pela Pontifícia Universidade Católica do Rio Grande do Sul em 1998. Pós graduada em Gestão de Serviços, com experiência na área Financeira-Estratégica, MBA Gestão de Serviços – IBGEN – 2008.  Curso de Educação Continuada',
                'mais'      => ' na Fundação Getúlio Vargas de São Paulo/SP - Administração Legal para Escritórios e Departamentos Juridicos  - 2010. MBA em andamento na Fundação Getúlio Vargas, em Gestão Econômica e Estratégica de Negócios.<br>Sólida experiência em gestão jurídica, com foco no desenvolvimento de ações conjuntas com Departamentos Jurídicos e Financeiros em diversos projetos ligados a questões de readequação orçamentária. Atua ativamente na elaboração, proposição e condução de todos os projetos, tendo como experiência diversos cases desenvolvidos de forma personalizada na área de gestão de finanças jurídicas, provisão, auditoria, dentre outros.',
                'foto'      => 'vanessa.png',
                'linkedin'  => 'https://www.linkedin.com/pub/vanessa-guazzelli-braga/22/bb0/649',
            )
        );

        $categorias = array(
            array('id' => 2, 'nome' => 'Advogados'),
            array('id' => 3, 'nome' => 'Consultores Externos'),
            array('id' => 4, 'nome' => 'Bacharéis'),
            array('id' => 5, 'nome' => 'Estagiários'),
            array('id' => 6, 'nome' => 'Administrativo'),
        );

        $sliderSegmentos = array(
            array(
                'titulo' => 'Bancário',
                'frase' => '',
                'foto' => 'segmento-bancario-sepia.png',
            ),
            array(
                'titulo' => 'Cartões de Crédito',
                'frase' => '',
                'foto' => 'segmento-cartao-de-credito-sepia.png',
            ),
            array(
                'titulo' => 'Energia Elétrica',
                'frase' => '',
                'foto' => 'segmento-energia-sepia.png'
            ),
            array(
                'titulo' => 'Construção Civil Pesada',
                'frase' => '',
                'foto' => 'segmento-construcao-civil-pesada-sepia.png'
            ),
            array(
                'titulo' => 'Construção Civil',
                'frase' => 'Empreendimentos Incorporações',
                'foto' => 'segmento-construcao-civil-sepia.png'
            ),
            array(
                'titulo' => 'Entretenimento',
                'frase' => '',
                'foto' => 'segmento-entretenimento-sepia.png'
            ),
            array(
                'titulo' => 'Indústria Moveleira',
                'frase' => '',
                'foto' => 'segmento-industria-moveleira-sepia.png'
            ),
            /*array(
                'titulo' => 'Comunicação',
                'frase' => '',
                'foto' => 'foto-segmento.png'
            ),
            array(
                'titulo' => 'Securitizadoras',
                'frase' => '',
                'foto' => 'foto-segmento.png'
            ),
            array(
                'titulo' => 'Indústria de Produtos de Limpeza',
                'frase' => '',
                'foto' => 'foto-segmento.png'
            ),*/
            array(
                'titulo' => 'Varejo',
                'frase' => '',
                'foto' => 'segmento-varejo-sepia.png'
            ),
            array(
                'titulo' => 'Serviços',
                'frase' => '',
                'foto' => 'segmento-servicos-sepia.png'
            ),
        );

        $expertises = array(
            array(
                'icone' => 'consumidor',
                'titulo' => 'Consumidor',
                'chamada' => 'Gestão de Contencioso de Volume',
                'conteudo' => 'Representa grandes empresas nacionais em processos administrativos e judiciais, em processos judiciais de alta complexidade.',
            ),
            array(
                'icone' => 'contratos',
                'titulo' => 'Contratos',
                'chamada' => '',
                'conteudo' => 'Nossa equipe de advogados e consultores acompanha o ritmo e os atuais contextos atinentes aos contratos empresariais, atualizando-se com relação às melhores práticas para cada setor e segmento, e com os temas mais importantes e complexos relativos às suas operações e contratos, oferecendo total apoio jurídico e alternativas criativas e eficazes de redução de risco.',
            ),
            array(
                'icone' => 'contecioso',
                'titulo' => 'Contecioso',
                'chamada' => '',
                'conteudo' => 'O escritório Guazzelli & Torrano representa grandes empresas nacionais em processos administrativos e judiciais, em processos judiciais de alta complexidade, movidos tanto por consumidores quanto por entes coletivos, em ações civis públicas ou em demandas individuais.',
            ),
            array(
                'icone' => 'ambiental',
                'titulo' => 'Ambiental',
                'chamada' => '',
                'conteudo' => 'Atuamos na esfera administrativa e judicial, especialmente com o planejamento preventivo, o estudo para captação de financiamento para projetos ambientais e com a elaboração de um plano de gestão ambiental.',
            ),
            array(
                'icone' => 'consumidor-2',
                'titulo' => 'Consumidor',
                'chamada' => 'Preventivo e Estrutural',
                'conteudo' => 'Na esfera preventiva, avaliamos produtos, serviços, fluxo de informações internas e externas e estudamos a viabilidade da implementação de mudanças que previnam o ingresso de novas demandas originadas a partir da identificação e solução do problema.',
            ),
            array(
                'icone' => 'societario',
                'titulo' => 'Societário',
                'chamada' => '',
                'conteudo' => 'Sedimentamos nossa filosofia de trabalho buscando a identificação de necessidades específicas de cada cliente, de maneira rápida e precisa, e na proposta de soluções eficientes em todosos estágios de uma operação.',
            ),
            array(
                'icone' => 'licitacoes',
                'titulo' => 'Licitações',
                'chamada' => '',
                'conteudo' => 'Nossa equipe de advogados e consultores disponibiliza ampla assessoria em processos de Licitações e Contratações Administrativas.<br>Atuamos também junto ao Tribunal de Contas e em audiências e consultas públicas.',
            ),
            array(
                'icone' => 'tributario',
                'titulo' => 'Tributário',
                'chamada' => '',
                'conteudo' => '<span style="font-size: 11px">Focamos o nosso trabalho na operação da empresa, conciliando as necessidades do mundo empresarial com segurança, através do desenvolvimento de um Planejamento Tributário moderno e eficaz, alinhado com as mais recentes e confirmadas teses e práticas que possam impactar positivamente na significativa redução da carga tributária da empresa, considerando suas características e particularidades, bem como o seu segmento e seus produtos e serviços.</span>',
            ),
            array(
                'icone' => 'teletrabalho',
                'titulo' => 'Teletrabalho',
                'chamada' => '',
                'conteudo' => 'Atuamos em parceria com a Sociedade Brasileira de Teletrabalho e Teleatividades (SOBRATT). Elaboramos contratos específicos para esta modalidade de trabalho.<br>Elaboramos um estudo detalhado sobre a viabilidade de implementação do teletrabalho nas empresas.',
            ),
            array(
                'icone' => 'imobiliario',
                'titulo' => 'Imobiliário',
                'chamada' => '',
                'conteudo' => 'Fornecemos todo o suporte jurídico necessário nas transações e incorporações imobiliárias; na elaboração de contratos, escrituras; no acompanhamento de processos nos serviços registrais e notariais; na assessoria preventiva, no contencioso; e em todos os demais temas pertinentes ao direito imobiliário e registral.',
            ),
            array(
                'icone' => 'recuperacao-de-credito',
                'titulo' => 'Recuperação de Crédito',
                'chamada' => '',
                'conteudo' => 'Utilizamos o que há de mais moderno em tecnologia. Nossa intenção é obter o maior índice de recuperação e de gestão dos créditos problemáticos, além de selecionar, dentre várias, a melhor prática para o recebimento do crédito e para a adoção de políticas que minimizem as inadimplências.',
            ),
            array(
                'icone' => 'trabalhista',
                'titulo' => 'Trabalhista',
                'chamada' => '',
                'conteudo' => 'Atendimento pessoal e personalizado em matéria preventiva e contenciosa administrativa e judicial para todas as questões alusivas aos direitos individuais e coletivos patronais. ',
            ),
        );

        $cases = array(
            array(
                'titulo' => 'Empreendimentos Imobiliários',
                'conteudo' => 'Face a atuação multidisciplinar, coordenada pelas áreas cível, trabalhista, tributária  e ambiental, nosso escritório tem atuado para diversos grupos de Construtoras e Incorporadoras de médio e grande porte, desde a aquisição do terreno, a entrega do empreendimento e o pós venda, atuando preventivamente e na esfera contenciosa, extrajudicial e judicial.',
                'foto' => 'case-1.png',
            ),
            array(
                'titulo' => 'Indústria Moveleira',
                'conteudo' => 'Obtivemos resultados surpreendentes com a redução total do custo do Contencioso na esfera consumeirista,  com a  redução de 60% dos processos da base inicialmente recebida, mediante  mapeamento de pontos críticos e definição de ações pontuais e mutirões de acordo em casos com grande probabilidade de perda pela empresa.',
                'foto' => 'case-2.png',
            ),
            array(
                'titulo' => 'Construção Civil Pesada',
                'conteudo' => 'Nossas esquipes acompanharam Projetos de grandes obras como Pontes, Estradas, Estaleiros e grandes empreendimentos da Construção Civil Pesada, gerenciando e monitorando in locu todas as questões atinentes ao funcionamento da obra e prevento possíveis implicações jurídicas de forma preventiva. Na esfera contenciosa, nossa equipe obteve diversos precedentes inovadores ligados a estes segmentos, além da redução considerável do passivo trabalhista, através do gerenciamento estratégico da base de ações.',
                'foto' => 'case-3.png',
            ),
            array(
                'titulo' => 'Soluções de Pagamentos',
                'conteudo' => 'Desenvolvimento de um  Business Intelligence direcionado ao aumento dos Ïndices de êxito em Primeira e Segunda Instância e correta compreensão das particularidades do negócio do segmento do cliente pelos órgãos julgadores. Os resultados foram atingidos com o aumento de 20% do Ïndice de Êxito em Primeira Instância e 40% em Segunda Instância, com a diminuição total da base em 25% de processos ativos.',
                'foto' => 'case-4.png',
            ),
            array(
                'titulo' => 'Auditoria/Saneamento e Mapeamento de Informações “Due Diligence” Para Diversos Segmentos',
                'conteudo' => 'O escritório desenvolveu importantes projetos alusivos ao saneamento, atualização  e mapeamento de Carteiras de grande volume, construindo Indicadores para avaliação do cenário macro e desenvolvimento de diversas estratégias direcionadas ao segmento e aos objetivos de cada cliente.',
                'foto' => 'case-5.png',
            ),
        );

        $this->set(compact('socias','categorias','sliderSegmentos','expertises','cases'));
    }

    private function setIdioma()
    {
        if ($this->Session->read('linguagem_default') == "") {
            App::import('Model', 'FdIdiomas.Idioma');
            $this->Idioma = new Idioma();
            $idioma = $this->Idioma->find("first", array('recursive' => -1, 'fields' => 'Idioma.slug', 'conditions' => array('Idioma.padrao' => true)));
            $this->Session->write('linguagem_default', $idioma['Idioma']['slug']);
        }

        App::import('Model', 'FdIdiomas.Idioma');
        $this->Idioma = new Idioma();

        //seto a linguagem quaqndo carrega o site
        if(isset($this->params['language']) && $this->params['language'] != $this->Session->read('LANGUAGE')){

            $idiomas = $this->Idioma->find("all", array('recursive' => -1, 'fields' => 'Idioma.slug', 'conditions' => array('Idioma.status' => true)));
            $linguas = Set::extract('/Idioma/slug', $idiomas);

            if(substr($this->referer(), 0, strlen($this->referer())) == '/'){
                $refer = substr($this->referer(), 1, strlen($this->referer()) -1);
            }else{
                $refer = $this->referer();
            }

            $refer = '/'.$this->params->url;

            if(in_array($this->params['language'], $linguas) && !in_array(substr($refer,1), $linguas)){
                Configure::write('Config.language', $this->params['language']);
                $this->Session->write('LANGUAGE', $this->params['language']);

               //debug($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);die;

                /*if(stripos($refer, $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) == false){
                    $this->redirect('/');
                }else{
                    $this->redirect($refer);
                }*/

                $this->redirect($refer);

            }
            
        }else{
            if($this->Session->read('LANGUAGE') != null){
                Configure::write('Config.language', $this->Session->read('LANGUAGE'));
            }else{
                Configure::write('Config.language', $this->Session->read('linguagem_default'));
            }
        }

        if(Configure::read('Config.language') != null){
            if($this->Session->read('LINGUAGEM_ATIVA') == null){
                $linguagem_ativa = $this->Idioma->find("first", array('recursive' => -1, 'fields' => array('Idioma.slug'), 'conditions' => array('Idioma.slug' => Configure::read('Config.language'))));
                $this->Session->write('LINGUAGEM_ATIVA', $linguagem_ativa);
            }else{
                $linguagem_ativa = $this->Session->read('LINGUAGEM_ATIVA');
            }

            if($linguagem_ativa['Idioma']['slug'] != Configure::read('Config.language')){
                $linguagem_ativa = $this->Idioma->find("first", array('recursive' => -1, 'fields' => array('Idioma.slug'), 'conditions' => array('Idioma.slug' => Configure::read('Config.language'))));
                $this->Session->write('LINGUAGEM_ATIVA', $linguagem_ativa);
            }

            $this->set(compact('linguagem_ativa'));
        }
    }

    public function saveStatus($model, $id, $status)
    {
        $this->{$model}->id = $id;
        $this->{$model}->saveField('status', $status);
        if ($status == 1)
        {
            return 0;
        } else {
            return 1;
        }
    }

    public function setConfiguracoes()
    {
        App::import('Model', 'FdConfiguracoes.Configuracao');

        $this->Configuracao = new Configuracao();

        $configuracoes = $this->Configuracao->find('all', array('fields' => array('Configuracao.key', 'Configuracao.value'), 'conditions' => array('Configuracao.status' => true)));

        foreach($configuracoes as $configuracao) {
            Configure::write('Configure.'.$configuracao['Configuracao']['key'], $configuracao['Configuracao']['value']);
        }
    }

    public function getPagina($id = null, $type = 'first', $order = false, $limit = false)
    {
        $options['conditions'] = array(
                                    'Pagina.status' => 1, 'Pagina.id' => $id
                                );
        $options['contain'] = array(
                                'PaginaTraducao' => array(
                                    'conditions' => array('PaginaTraducao.idioma_slug' => Configure::read('Config.language'))
                                )
                            );

        if($order)
            $options['order'] = 'RAND()';
        else
            $options['order'] = array('Pagina.titulo' => 'ASC');

        if($limit) $options['limit'] = $limit;

        $pagina = $this->Pagina->find($type, $options);

        // debug($pagina);die;

        /*$pagina = $this->Pagina->find($type, array(
            'conditions' => array(
                'Pagina.status' => 1,
                'Pagina.id' => $id,
            ),
            'contain' => array(
                'PaginaTraducao' => array(
                    'conditions' => array('PaginaTraducao.idioma_slug' => Configure::read('Config.language'))
                ),
            ),
            'order' => array('Pagina.titulo' => 'ASC')
        ));*/

        return $pagina;
    }


}

<?php

class NoticiasController extends AppController {

	public $uses = array('FdNoticias.Noticia');

	public function index()
	{
		$this->Paginator->settings = array(
			'conditions' => array('Noticia.status' => true),
			'order' => array('Noticia.data' => 'DESC', 'Noticia.id' => 'DESC'),
		);

		$noticias = $this->Paginator->Paginate('Noticia');

		$this->set(compact('noticias'));
	}

	public function detalhe($id = null)
	{
		$noticia = $this->Noticia->find('first', array('conditions' => array('Noticia.id' => $id, 'Noticia.status' => true)));

		$this->set(compact('noticia'));
	}
}
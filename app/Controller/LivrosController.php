<?php

class LivrosController extends AppController {

	public $uses = array('FdLivros.Livro');

	public function index()
	{
		$this->Paginator->settings = array(
			'conditions' => array('Livro.status' => true),
			'order' => array('Livro.created' => 'DESC', 'Livro.id' => 'DESC'),
		);

		$livros = $this->Paginator->Paginate('Livro');

		$this->set(compact('livros'));
	}

	public function detalhe($id =null)
	{
		$livro = $this->Livro->find('first', array('conditions' => array('Livro.status' => true)));

		$this->set(compact('livro'));
	}
}
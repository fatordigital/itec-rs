<?php

class GaleriasController extends AppController {

	public $uses = array('FdGalerias.Galeria');

	public function index()
	{
		$this->Paginator->settings = array(
			'conditions' => array('Galeria.status' => true),
			'order' => array('Galeria.data' => 'DESC', 'Galeria.id' => 'DESC'),
		);

		$galerias = $this->Paginator->Paginate('Galeria');

		$this->set(compact('galerias'));
	}

	public function detalhe($id = null)
	{
		$galeria = $this->Galeria->find('first', array('conditions' => array('Galeria.id' => $id, 'Galeria.status' => true)));

		$this->set(compact('galeria'));
	}
}
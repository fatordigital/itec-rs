<?php
/**
 * EnqueteResultadoFixture
 *
 */
class EnqueteResultadoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'enquete_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'usuario_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'opcao' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'votos' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'enquete_id' => 1,
			'usuario_id' => 1,
			'opcao' => 'Lorem ipsum dolor sit amet',
			'votos' => 1,
			'created' => '2014-09-18 21:50:53',
			'modified' => '2014-09-18 21:50:53'
		),
	);

}

<?php
/**
 * EnqueteResultadoTraducaoFixture
 *
 */
class EnqueteResultadoTraducaoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'enquete_resultado_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'idioma_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'opcao' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'idioma_slug' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'enquete_resultado_id' => 1,
			'idioma_id' => 1,
			'opcao' => 'Lorem ipsum dolor sit amet',
			'idioma_slug' => 'Lorem ip'
		),
	);

}

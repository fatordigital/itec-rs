<?php
App::uses('EnqueteResultado', 'Model');

/**
 * EnqueteResultado Test Case
 *
 */
class EnqueteResultadoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.enquete_resultado',
		'app.enquete',
		'app.usuario',
		'app.enquete_resultado_traducao'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EnqueteResultado = ClassRegistry::init('EnqueteResultado');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EnqueteResultado);

		parent::tearDown();
	}

}

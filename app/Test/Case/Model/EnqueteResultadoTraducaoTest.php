<?php
App::uses('EnqueteResultadoTraducao', 'Model');

/**
 * EnqueteResultadoTraducao Test Case
 *
 */
class EnqueteResultadoTraducaoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.enquete_resultado_traducao',
		'app.enquete_resultado',
		'app.enquete',
		'app.usuario',
		'app.idioma'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EnqueteResultadoTraducao = ClassRegistry::init('EnqueteResultadoTraducao');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EnqueteResultadoTraducao);

		parent::tearDown();
	}

}
